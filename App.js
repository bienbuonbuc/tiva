import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MainStack from './src/routes';
import {Root} from 'native-base';
import { AppLoading } from "expo";
import * as Font from "expo-font";
import AddJobOvertime from './src/screens/job_overtime/add_job_overtime/AddJobOvertime';
import JobOvertime from './src/screens/job_overtime/JobOvertime';
import CandidatesManage from './src/screens/candidates _manage/CandidatesManage';
import Push from './Push';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
    // console.disableYellowBox = true; 
  }
  async UNSAFE_componentWillMount() {
    await Font.loadAsync({                
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Calibri: require('./assets/CALIBRI.ttf')
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return (            
        <Root>
          <AppLoading />
        </Root>     
      );
    }
    return (
      <MainStack />
    );    
  }
}
