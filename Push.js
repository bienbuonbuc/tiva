import React from 'react';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import {
  Text,
  View,
  TouchableOpacity
} from 'react-native';
// import Constants from 'expo-constants';


// This refers to the function defined earlier in this guide

const PUSH_ENDPOINT = 'https://your-server.com/users/push-token';

export default class Push extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      notification: '',
      pre:''
    }
    ,console.disableYellowBox = true;

  }


  registerForPushNotificationsAsync = async() => {
    // if (Constants.isDevice) {
      // nếu chạy thiết bị ảo thì Constants.isDevice có giá trị false, máy thật thì true
      // console.log(!Constants.isDevice)
      
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (status !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      alert('No notification permissions!');
      return;
    }
  
    // // Stop here if the user did not grant permissions
    // if (finalStatus !== 'granted') {
    //   return;
    // }
  
    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
    console.log(token)
    // } else {
    //   alert('phai su dung may that de chay');
    // }

    // POST the token to your backend server from where you can retrieve it to send push notifications.
    return fetch(PUSH_ENDPOINT, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"  ,
      },
      body: JSON.stringify({
        token: {
          value: token,
        },
        user: {
          username: 'Brent',
        },
      }),
    });
  }


  componentDidMount() {
    this.registerForPushNotificationsAsync();

    // Handle notifications that are received or selected while the app
    // is open. If the app was closed and then opened by tapping the
    // notification (rather than just tapping the app icon to open it),
    // this function will fire on the next tick after the app starts
    // with the notification data.
    this.notificationSubscription = Notifications.addListener(this.handleNotification);
  }

  handleNotification = (notification) => {
    this.setState({notification: notification});
    console.log(this.state.notification)
    if(this.state.notification.origin == 'selected') {
      alert('aaa')
    }
  };

  a = async() =>{
    let res = await fetch('https://exp.host/--/api/v2/push/send',{
      method:'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        to:'ExponentPushToken[RQOicNCtWhtOeCVtszgTAO]' ,
        sound: 'default',
        title:'Ứng Viên',
        body:'Có Ứng Viên Vừa Ứng Tuyển'
      })
    })
    let pre = await res.json();
    this.setState({pre:pre.data})
    return pre;
  }

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity onPress = {() => this.a()}>
          <Text>pushhhhhhhhhhhhh</Text>
          <Text>{this.state.pre.id}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}