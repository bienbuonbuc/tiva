//parse lib
const qs = require('qs');
//base url
const url ='https://demo.tiva.vn/api';
//defind hedeaders
const header = {
    Accept: 'application/json',
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
};
export  default class  Http {
    //tôi lười nghĩ quá :))
    // đây là cách dùng async
    //ông export class cho dễ dùng
    static async get(patch) {
        try {
            // gọi lên server
            let pre = await fetch(url + patch, {
                //init headers
                headers: header
            });
            //đợi parse
            let res = await pre.json();
            //trả về kết quả cuối
            return res;
        } catch (error) {
            console.log (error);
        }
    }

    //HTTP POST METHOD
    static async post(patch, data) {
        try {
           // y như trên ^^
            let res = await fetch(url + patch, {
                //gán method post
                //gán headers
                //parse json -> form data
                method: "POST",
                headers: header,
                body: qs.stringify(data)
            });
            let pre = await res.json();
            return pre;
        } catch (e) {
            console.log(e);
        }
    }}