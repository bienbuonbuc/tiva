const Colors  = {
  primary: '#226B74',
  secondary: '#254B5A',
  tertiary: '#5DA6A7',
  statusBar: '#802390',
  active: '#802390',
  divider: '#00000050',
  bookmark: '#F25F5C',
  setting: '#FA7921',
  help: '#9BC53D',
  background: '#F4F4F4',
  star: '#F1C40E',  
  note: '#B9BABF',
  comboBox: '#CBCBCB',
  money: '#FF1C1C'
}
export default Colors;