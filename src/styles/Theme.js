import {StyleSheet, Platform} from 'react-native';
import Colors from './Color';
import {dimensions} from './Dimension';
const Themes = StyleSheet.create({
    header: {
        paddingHorizontal: 15,
        backgroundColor:Colors.active,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        height:Platform.OS === 'ios' ? 70 : 50,
        paddingTop: Platform.OS==='ios'?20:0,
        width:'100%'
    },
    headerBottomSheet: {
        width:'100%',
        paddingHorizontal:15,
        height:50,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    divider: {
        width:'100%',
        height:1,
        backgroundColor: '#B9BABF'
    },
    dividerLarge: {
        width:'100%',
        height:4,
        backgroundColor: '#B9BABF'
    },
    rbSheetContainer: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        justifyContent: "center",
        alignItems: "center"
    },
    rbSheetTitleHeader: {
        fontWeight:'bold'
    },
    rbSheetItemContainer: {
        paddingHorizontal:10
    },
    rbSheetBtReCheck: {
        color: '#3E7CE5',
        fontWeight:'bold'
    },
    tabsUnderlineColor: {
        backgroundColor: Colors.active,
    },
    tabActiveTextStyle: {
        color: Colors.active,
    },
    tabActiveStyle: {
        backgroundColor: 'white'
    },
    tabStyle: {
        backgroundColor: 'white',        
    },
    tabContent: {
        //backgroundColor: Colors.background,
        //height: 100
    },
    searchItem: {
        borderRadius:100, 
        marginLeft: 10, 
        padding:10, 
        borderWidth:1, 
        borderColor: '#525252'
    },
    searchContainer: {
        flexDirection:'row', 
        paddingVertical: 10,
        paddingRight:10,
        paddingBottom:10,
    }, 
    statusContainer: {
        borderRadius:100,
        padding:5,
        marginTop:5,
        backgroundColor:'#707070',
        width:120,
        alignItems:'center'
    },
    activityIndicatorView: {
        alignSelf: 'center', 
        position: 'absolute', 
        top: dimensions.fullHeight*0.5
    },
});
export default Themes;