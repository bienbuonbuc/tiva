import {Dimensions} from 'react-native'

export const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
export const padding = {
  mn: 5,
  sm: 10,
  qt: 15,
  md: 20,
  lg: 30,
  xl: 40
}
export const radius = {
  sm: 5,
  md: 10,
  lg: 15,
}
export const fontsSize = {
  sm: 12,
  md: 18,
  lg: 28,
  primary: 'Cochin'
}
export const iconSize = {
  sm: 15,
  md: 25,
  qt: 30,
  lg: 35
}
export const dividers = {
  sm: 0.5,
  md: 1,
  lg: 1.5, 
}
