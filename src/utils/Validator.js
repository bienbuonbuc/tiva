const Validator = {
    Required(strArray) {
        let result = true;
        for(let i = 0; i < strArray.length; i++) {
            if (strArray[i] == '') return false;
            else result=true;
        }      
        return result;
    },
    IsEmail(email) {
        const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i;
        if (email.match(regex) == null)        
            return false;
        else
            return true;
    },
    MinLength(val,len) {    
        if ((parseInt(val.toString().length) - parseInt(len)) < 0) return false;
        else return true; 
    },
    IsPhoneNumberVN(phone) {
        // const regex = /(09[1|2|3|4|5|6|7|8|9]|03[2|3|4|5|6|7|8|9])+([0-9]{7})\b/g;
        // if (phone.match(regex) == null)        
        //     return false;
        // else
            return true;
    }
}
export default Validator;