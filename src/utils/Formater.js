const Formater = {
    DateUTCFormat(dateValue,type,character) {
        const dateEls = dateValue.split(' ');              
        const y = dateEls[3].toString();
        const mText = dateEls[2].toString();
        const d = dateEls[1].toString();
        let m = '';
        if (mText == 'Jan') m='01'
        else if (mText == 'Feb') m='02'
        else if (mText == 'Mar') m='03'
        else if (mText == 'Apr') m='04'
        else if (mText == 'May') m='05'
        else if (mText == 'Jun') m='06'
        else if (mText == 'Jul') m='07'
        else if (mText == 'Aug') m='08'
        else if (mText == 'Sep') m='09'
        else if (mText == 'Oct') m='10'
        else if (mText == 'Nov') m='11'
        else if (mText == 'Dec') m='12'
       
        let result = '';     
        if (type == 'dd-mm-yyyy') result = d + character + m + character + y;
        else if (type == 'yyyy-mm-dd') result = y + character + m + character + d; 
        else if (type == 'dd-mm') result = d + character + m;
        else if (type == 'yyyy') result = y;
        else if (type == 'dd') result = d;
        else if (type == 'm') result = m;        
        return result;
    }
}
export default Formater;