import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const ProfileRecruitmentAPI = {
    GetAllRecruitmentApplied(filterCondition) {
        var url = Url.ApiURL + 'danh-sach-ung-vien-da-ung-tuyen-cong-viec';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(filterCondition)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetAllPosted(token) {
        //console.log('token',token);
        var url = Url.ApiURL + 'cong-viec-da-dang-cua-ntd';
        return fetch(url,{
            method:'POST',
            headers: header,
            body:JSON.stringify(token)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetAllExpiredPost(token) {
        //console.log(token);
        var url = Url.ApiURL + 'cong-viec-het-han-cua-ntd';
        return fetch(url,{
            method:'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    IncrementDateline(condition) {
        var url = Url.ApiURL + 'gia-han-30-ngay';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(condition)
        })
        .then(res => Promise.all([res.status,res.json()]));
    }
};
export default ProfileRecruitmentAPI;