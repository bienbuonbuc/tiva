import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const JobDetailAPI = {
    GetJobDetail(jobId) {
        var url = Url.ApiURL + 'chi-tiet-cong-viec';
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(jobId)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetEmployerDetail(jobId) {
        var url = Url.ApiURL + 'chi-tiet-nha-tuyen-dung-theo-cong-viec';
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(jobId)
        })  
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetJobGroupWithEmployer(jobId) {
        var url = Url.ApiURL + 'viec-lam-cung-nha-tuyen-dung';
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(jobId)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    ApplyNow(condition){
        var url = Url.ApiURL + 'ung-vien/ung-tuyen-cong-viec/' + condition.jobId + "?token=" + condition.token;
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    }
};
export default JobDetailAPI;