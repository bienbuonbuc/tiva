import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const EditAccountAPI = {
    EditAccountInfo(account) {
        var url = Url.ApiURL + 'chinh-sua-thong-tin-tai-khoan';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(account)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
};
export default EditAccountAPI;