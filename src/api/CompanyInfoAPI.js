import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const CompanyInfoAPI = {
    UpdateEnterpriseInfo(enterprise) {
        //console.log(enterprise);
        var url = Url.ApiURL + 'cap-nhat-thong-tin-cong-ty';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(enterprise)
        })
        .then(res => Promise.all([res.status,res.json()]));
    }, 
};
export default CompanyInfoAPI;
