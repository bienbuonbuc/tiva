import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const HomeAPI = {
    GetCareers() {
        var url = Url.ApiURL + 'danh-sach-nganh-nghe';
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status, res.json()]));
    },
    GetJobTypes() {
        var url = Url.ApiURL + 'nhom-viec-lam';
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetProvinces() {
        var url = Url.ApiURL + 'tat-ca-tinh-thanh';
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetAllJob(filter,page) {
        var url = Url.ApiURL + 'cong-viec?page=' + page;
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(filter)
        })
        .then(res => Promise.all([res.status,res.json()]));
    }
};
export default HomeAPI;