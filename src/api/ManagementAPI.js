import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const ManagementAPI = {
    GetAllPostRecruitment(token) {
        var url = Url.ApiURL + 'tin-tuyen-dung-da-dang';       
        return fetch(url,{
            method:'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + JSON.parse(token.token),
            },
            body: null
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
};
export default ManagementAPI;