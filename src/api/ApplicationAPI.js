import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const ApplicationAPI = {    
    ChangeStatusApplication(status) {
        var url = Url.ApiURL + 'cap-nhat-trang-thai-ung-vien';
        return fetch(url, {
            method:'POST',
            headers: header,
            body: JSON.stringify(status)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetEmployeeInfo(condition) {
        var url = Url.ApiURL + 'thong-tin-ung-vien-da-ung-tuyen';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(condition)
        })
        .then(res => Promise.all([res.status,res.json()]));
    }
}
export default ApplicationAPI;