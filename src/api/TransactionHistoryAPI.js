import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const HistoryTransactionAPI = {
    GetAllHistoryTransaction(token) {
        var url = Url.ApiURL + 'lich-su-giao-dich';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(token)
        })
        .then(res => Promise.all([res.status,res.json()]));
    }
};
export default HistoryTransactionAPI