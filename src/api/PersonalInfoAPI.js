import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const PersonalInfoAPI = {
    EditPersonalInfo(personal) {
        var url = Url.ApiURL + 'cap-nhat-thong-tin-ca-nhan';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(personal)
        })
        .then(res => Promise.all([res.status,res.json()]));
    }
};
export default PersonalInfoAPI;