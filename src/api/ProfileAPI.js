import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const ProfileAPI = {   
    NumberRecruitApplied(token) {
        var url = Url.ApiURL + 'so-ung-vien-da-ung-tuyen';
        //console.log(token);
        return fetch(url,{
            method:'POST',
            headers: header,
            body:JSON.stringify(token)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetAllRecruitment(token) {
        var url = Url.ApiURL + 'thong-ke-ung-vien';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(token)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetAllPosted(token) {
        var url = Url.ApiURL + 'thong-ke-tin-da-dang';
        return fetch(url,{
            method:'POST',
            headers:header,
            body:JSON.stringify(token)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetAccountInfo(token) {
        var url = Url.ApiURL + 'thong-tin-ntd?token=' + token;
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    }
};
export default ProfileAPI;
