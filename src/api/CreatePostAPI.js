import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const CreatePostAPI = {
    GetCareers() {
        var url = Url.ApiURL + 'danh-sach-nganh-nghe';
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetJobGroup() {
        var url = Url.ApiURL + 'nhom-viec-lam';
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    Create(post) {
        var url = Url.ApiURL + 'tao-tin-tuyen-dung';
        return fetch(url,{
            method:'POST',
            headers: header, 
            body: JSON.stringify(post)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetSalaries() {
        var url = Url.ApiURL + 'luong';
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    }
};
export default CreatePostAPI;