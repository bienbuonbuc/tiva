import * as Url from '../constants/Url';

const header={
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};
const AccountAPI = {
    Login(account) {
        var url = Url.ApiURL + 'dang-nhap';
        return fetch(url, {
            method: 'POST',
            headers: header,
            body: JSON.stringify(account)
        })
        .then(res => Promise.all([res.status, res.json()]));
    },
    LoginWithPhoneNumber(account) {
        var url = Url.ApiURL + 'dang-nhap-bang-so-dien-thoai';
        return fetch(url, {
            method: 'POST',
            headers: header,
            body: JSON.stringify(account)
        })
        .then(res => Promise.all([res.status, res.json()]));
    },
    ForgotPassword(email) {
        var url = Url.ApiURL + 'quên mật khẩu';
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(email)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    Register(account) {
        console.log(account);
        var url = Url.ApiURL + 'dang-ky';
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(account)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    RegisterGoogle(account) {
        console.log(account);
        var url = Url.ApiURL + 'register-with-google';
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(account)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    RegisterFB(account) {
        console.log(account);
        var url = Url.ApiURL + 'register-social';
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(account)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    RegisterNTD(account) {
        console.log(account);
        var url = Url.ApiURL + 'tro-thanh-ntd';
        return fetch(url,{
            method:'POST',
            headers: header,
            body: JSON.stringify(account)
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetBussinessType() {
        var url = Url.ApiURL + 'danh-sach-nganh-nghe';
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
    GetBussiness() {
        var url = Url.ApiURL + 'danh-sach-business';
        return fetch(url,{
            method:'GET',
            body:null
        })
        .then(res => Promise.all([res.status,res.json()]));
    },
};
export default AccountAPI;