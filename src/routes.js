import React from 'react';
import {createStackNavigator, createBottomTabNavigator, createSwitchNavigator} from 'react-navigation';
import Login from './screens/login/Login';
import RegisterStep1 from './screens/register/step1/Register_step1';
import RegisterStep2 from './screens/register/step2/Register_step2';
import RegisterStep3 from './screens/register/step3/Register_step3';
import ForgotPassword from './screens/forgot_password/ForgotPassword';
import Home from './screens/home/Home';
import { Ionicons } from '@expo/vector-icons';
import Management from './screens/management/Management';
import Profile from './screens/profile/Profile';
import Colors from './styles/Color';
import JobDetail from './screens/job_detail/JobDetail';
import ProfileRecruitment from './screens/profile_recruitment_manager/ProfileRecruitment';
import Application from './screens/application/Application';
import CreatePost from './screens/create_post_recruitment/CreatePost';
import PersonalInfo from './screens/add_personal_info/PersonalInfo';
import EditAccount from './screens/edit_account_info/EditAccountInfo';
import CompanyInfo from './screens/add_company_info/CompanyInfo';
import OnlinePayment from './screens/online_payment/OnlinePayment';
import TransferPayment from './screens/transfer_payments/TransferPayment';
import HistoryTransaction from './screens/history_transaction/HistoryTransaction';
import JobOvertime from './screens/job_overtime/JobOvertime';
import AddJobOvertime from './screens/job_overtime/add_job_overtime/AddJobOvertime';
import CandidatesManage from './screens/candidates _manage/CandidatesManage';
import DetailCandidates from './screens/detail_candidates/DetailCandidates';
import SimpleLineIcons from '@expo/vector-icons/SimpleLineIcons';
import FormLogin from './screens/login/form_login/FormLogin';
import RegisterStep from './screens/job_overtime/register_step/RegisterStep';

const HomeStack = createStackNavigator(
    {
       Home: Home,
       JobDetail: JobDetail
    },
    {
        navigationOptions: {
            header: null,
        }
    }
);

const ManagementStack = createStackNavigator(
    {
        Management: Management,
        ProfileRecruitment: ProfileRecruitment,
        Application: Application,
        CreatePost: CreatePost
    },
    {
        navigationOptions: {
            header: null,
        }
    }
);
const CandidatesManageStack = createStackNavigator(
    {
        CandidatesManage: CandidatesManage,
        DetailCandidates: DetailCandidates
    },
    {
        navigationOptions: {
            header: null,
        }
    }
);

const ProfileStack = createStackNavigator(
    {
        Profile:Profile,
        PersonalInfo:PersonalInfo,
        EditAccount:EditAccount,
        CompanyInfo:CompanyInfo,
        OnlinePayment:OnlinePayment,
        TransferPayment:TransferPayment,
        HistoryTransaction:HistoryTransaction
    },
    {
        navigationOptions: {
            header: null,
        }
    }
);
const JobOverStack = createStackNavigator(
    {
        JobOvertime:JobOvertime,
        AddJobOvertime: AddJobOvertime
    },
    {
        navigationOptions: {
            header: null,
        }
    }
)

const BottomTab = createBottomTabNavigator(
    {
        JobOverStack: {
            screen: JobOverStack,
            navigationOptions:{
                title: 'Việc làm thêm',
                tabBarIcon: ({tintColor}) => (
                    <Ionicons name={'md-home'} size={30} color={tintColor}/>
                )
            }
        },
        Home: {
            screen: HomeStack,
            navigationOptions:{
                title: 'Việc làm',
                tabBarIcon: ({tintColor}) => (
                    <Ionicons name={'ios-business'} size={30} color={tintColor}/>
                )
            }
        },
        Management: {
            screen: ManagementStack,
            navigationOptions:{
                title: 'Nhà tuyển dụng',
                tabBarIcon: ({tintColor}) => (
                    <Ionicons name={'ios-briefcase'} size={30} color={tintColor}/>
                )
            }
        },
        CandidatesManage: {
            screen: CandidatesManageStack,
            navigationOptions:{
                title: 'Ứng viên',
                tabBarIcon: ({tintColor}) => (
                    <SimpleLineIcons name={'people'} size={30} color={tintColor}/>
                )
            }
        },
        Profile: {
            screen: ProfileStack,
            navigationOptions:{
                title: 'Hồ sơ',
                tabBarIcon: ({tintColor}) => (
                    <Ionicons name={'ios-contact'} size={30} color={tintColor}/>
                )
            }
        }
    },{       
        animationEnabled: true,
        tabBarOptions: {
            activeTintColor: Colors.active,
        }
    }, 
);

const RegisterStack = createStackNavigator(
    {
        RegisterStep1: RegisterStep1,
        RegisterStep2: RegisterStep2,
        RegisterStep3: RegisterStep3
    },
    {
        navigationOptions: {
            header: null,
        }
    }
);
const MainStack = createStackNavigator
(
    {
        Login: Login,        
        Register: RegisterStack,
        ForgotPassword: ForgotPassword,
        BTab: BottomTab,
        FormLogin: FormLogin,
        AddJobOvertime: AddJobOvertime,
        RegisterStep: RegisterStep,
        CandidatesManage: CandidatesManage,
        JobOvertime: JobOvertime
    },     
    {
        initialRouteName: 'Login',
        navigationOptions: {
            header: null,
        }
    }
);

// const MainStack = createSwitchNavigator
// (
//     {
//         Login: Login,        
//         Register: RegisterStack,
//         ForgotPassword: ForgotPassword,
//         BTab: BottomTab,
//         FormLogin: FormLogin
//     },     
//     {
//         initialRouteName: 'Login',
//         navigationOptions: {
//             header: null,
//         }
//     }
// );
// createSwitchNavigator chỉ tải 1 màn hình tại 1 thời điểm, không có chức năng quay lại
export default MainStack;