import React from 'react';
import {View,Text} from 'native-base';
import {TouchableOpacity,Platform, StatusBar} from 'react-native';
import Colors from '../styles/Color';
import {Ionicons} from '@expo/vector-icons';
import Themes from '../styles/Theme';

export default class BasicHeader extends React.Component {
    render() {
        return (
            <View style={Themes.header}>
                <StatusBar hidden={false}></StatusBar>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name={'ios-arrow-back'} size={30} color={'white'}/>
                </TouchableOpacity>
                <Text style={{color:'white',fontWeight:'bold'}}>{this.props.title}</Text>
                <View></View>
            </View>
        )
    }
}