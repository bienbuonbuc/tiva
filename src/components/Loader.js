import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import Themes from '../styles/Theme';
import Colors from '../styles/Color';

export default class Loader extends React.Component {
    render() {
        return(
            <View style={Themes.activityIndicatorView}>        
                <ActivityIndicator animating={this.props.loading} size={'large'} color={Colors.active}/>        
            </View>
        )
    }
}