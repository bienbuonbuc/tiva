import React from 'react';
import {View} from 'native-base';
import {Text} from 'react-native';
import Colors from '../styles/Color';

export default class NotifyEmptyData extends React.Component {
    render() {
        return(
            <View style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'center',position:'absolute',zIndex:900000}}>
                <Text style={{color:Colors.active,fontWeight:'bold',fontSize:17}}>Không có dữ liệu</Text>
            </View>
        )
    }
}