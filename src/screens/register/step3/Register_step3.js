import React from 'react';
import {View, Text, Input, Button, Textarea} from 'native-base';
import {ImageBackground, Image, TouchableOpacity, ScrollView, ActivityIndicator, Alert, AsyncStorage} from 'react-native';
import style from '../style';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../../styles/Color';
import {iconSize} from '../../../styles/Dimension';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RBSheet from "react-native-raw-bottom-sheet";
import Themes from '../../../styles/Theme';
import AccountAPI from '../../../api/AccountAPI';
import Validator from '../../../utils/Validator';
import { StackActions, NavigationActions } from 'react-navigation';

const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'BTab' })],
    key:null
  });
export default class RegisterStep3 extends React.Component {

    constructor(props) {
        super(props);
        const userStep2 = this.props.navigation.getParam('userStep2');
        this.state= {
            userStep2: userStep2,
            studyProcess: '',
            bussiness: [],
            bussinessTypeIdSelected: 0,
            loading: true,
            career_name: 'Chọn ngành nghề',
            tokenFB: '',
            registrationFace: '',
            google_id: '',
            registrationGoogle: ''
        }
    }

    async componentDidMount() {
        const google_id = await AsyncStorage.getItem('google_id');
        const registrationFace = await AsyncStorage.getItem('registrationFace');
        const tokenFB = await AsyncStorage.getItem('tokenFB');
        const registrationGoogle = await AsyncStorage.getItem('registrationGoogle');
        await this.setState({tokenFB: tokenFB, registrationFace: registrationFace,google_id:google_id,registrationGoogle:registrationGoogle })
        AccountAPI.GetBussinessType()
        .then(async(res) => {
            this.setState({loading:false,bussiness: res[1]});
        })
        .catch((err) => {
            this.setState({loading:false});
            Alert.alert('Lỗi: ' + err);
        })        
        .done();
    }

    renderSubject = (subjects) => {
        return subjects.map((el,i) => {
            return (               
                <TouchableOpacity key={i} onPress={() => {
                        this.setState({bussinessTypeIdSelected: el.career_category_id,career_name:el.career_category_name});
                        this.RBSheet.close();
                    }}>
                    <View style={Themes.rbSheetItemContainer}>                                                  
                        <View style={{marginBottom:15}}></View>                        
                        <Text>{el.career_category_name}</Text>
                        <View style={[Themes.divider,{marginTop:15}]}></View>
                    </View>
                </TouchableOpacity>               
            );
        });
    }

    Register = () => {
        this.setState({loading:true});

        if (!Validator.Required([this.state.studyProcess])) {
            this.setState({loading:false});
            Alert.alert('Quá trình học tập và kinh nghiệm không được để trống!');
            return;
        }

        if (this.state.bussinessTypeIdSelected == 0) {
            this.setState({loading:false});
            Alert.alert('Bạn phải chọn ngành nghề!');
            return;
        }

        const account = {};
        let careers = [];
        careers.push(this.state.bussinessTypeIdSelected);
        
        account.experience = this.state.studyProcess;
        account.careers = careers,
        account.name = this.state.userStep2.fullName;
        account.email = this.state.userStep2.email;
        account.password = this.state.userStep2.password;
        account.phone = this.state.userStep2.phoneNumber;
        account.address = this.state.userStep2.address;
        if(this.state.registrationFace == 'Đăng kí Facebook') {
            account.tokenFB = this.state.tokenFB
        }
        if(this.state.registrationGoogle == 'Đăng kí Google') {
            account.google_id = this.state.google_id
        }

        //console.log(account);            
        //return;
    if(this.state.registrationGoogle == 'Đăng kí Google') {
        AccountAPI.RegisterGoogle(account)
            .then(async (res) => {
                this.setState({loading:false});
                if (res[0] == 200) {
                    await AsyncStorage.multiSet([
                        ['token',JSON.stringify(res[1].token)],
                        ['role',JSON.stringify(res[1].user.role)],
                        ['email',JSON.stringify(account.email)],
                        ['password',JSON.stringify(account.password)]  
                    ]);
                    this.props.navigation.dispatch(resetAction);
                }
                else Alert.alert(res[1].message + '');                                  
            })
            .catch((err) => {
                this.setState({loading:false});
                Alert.alert("Lỗi: " + err);
            })
            .done();
    }
    if(this.state.registrationFace == 'Đăng kí Facebook') {
        AccountAPI.RegisterFB(account)
            .then(async (res) => {         
                this.setState({loading:false});
                if (res[0] == 200) {
                    await AsyncStorage.multiSet([
                        ['token',JSON.stringify(res[1].token)],
                        ['role',JSON.stringify(res[1].user.role)],
                        ['email',JSON.stringify(account.email)],
                        ['password',JSON.stringify(account.password)]  
                    ]);
                    this.props.navigation.dispatch(resetAction);
                }
                else Alert.alert(res[1].message + '');                                  
            })
            .catch((err) => {
                this.setState({loading:false});
                Alert.alert("Lỗi: " + err);
            })
            .done();
    }
    else{
        AccountAPI.Register(account)
        .then(async (res) => {         
            this.setState({loading:false});
            if (res[0] == 200) {
                await AsyncStorage.multiSet([
                    ['token',JSON.stringify(res[1].data)],
                    ['role',JSON.stringify(res[1].user.role)],
                    ['email',JSON.stringify(account.email)],
                    ['password',JSON.stringify(account.password)]  
                ]);
                this.props.navigation.dispatch(resetAction);
            }
            else Alert.alert(res[1].message + '');                                  
        })
        .catch((err) => {
            this.setState({loading:false});
            Alert.alert("Lỗi: " + err);
        })
        .done();
    }
    }

    render() {
        return (           
            <ImageBackground source={require('../../../../assets/images/bg.jpg')} style={style.ImageBG}>
                <View style={[Themes.activityIndicatorView,{zIndex:100000,position:'absolute'}]}>        
                    <ActivityIndicator animating={this.state.loading} size={'large'} color={Colors.active}/>        
                </View>  
                <KeyboardAwareScrollView>
                    <View style={style.root}>
                        <Image source={require('../../../../assets/images/logo.png')} style={style.Logo}/>
                        <Text style={style.wellcome}>Chào mừng bạn đến với TIVA !</Text>
                        <View style={[style.formGroup,{marginTop:20}]}>
                            <Text style={style.titleStep3}>Chọn ngành nghề của bạn</Text>
                            <TouchableOpacity onPress={() => {this.RBSheet.open();}}>
                                <View style={style.comboBox}>
                                    <Text>{this.state.career_name}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={style.formGroup,{margin: 10}}>
                            <Text style={style.titleStep3}>Quá trình học tập hoặc kinh nghiệm</Text>
                            <Textarea style={style.textArea}
                                onChangeText={(text) => this.setState({studyProcess:text})}>                                
                            </Textarea>
                        </View>                    
                        <Button block style={style.btBlock}
                            onPress={() => this.Register()}>
                            <Text>HOÀN THÀNH ĐĂNG KÝ</Text> 
                        </Button>
                        <Button block style={style.btBlock}
                            onPress={() => this.props.navigation.goBack()}>
                            <Text>Quay lại</Text> 
                        </Button>
                        <RBSheet
                            ref={ref => {
                                this.RBSheet = ref;
                            }}
                            height={300}
                            duration={250}                            
                            customStyles={{ 
                                container: Themes.rbSheetContainer,                               
                                }}>
                            <View style={{width:'100%'}}>
                                <View style={Themes.headerBottomSheet}>
                                    <TouchableOpacity onPress={() => {this.RBSheet.close()}}>
                                        <Ionicons name='ios-close' size={40} />
                                    </TouchableOpacity>
                                    <Text style={Themes.rbSheetTitleHeader}>Chọn ngành nghề</Text>
                                    <View></View>
                                </View>
                                <ScrollView style={{height:250}}>
                                    <View>
                                        <View style={Themes.divider}></View>                                   
                                        {this.renderSubject(this.state.bussiness)}
                                    </View>
                                </ScrollView>
                            </View>
                        </RBSheet>
                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>           
        )
    }
} 