import React from 'react';
import {View, Text, Input, Button} from 'native-base';
import {ImageBackground, Image, Alert, AsyncStorage} from 'react-native';
import style from '../style';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../../styles/Color';
import {iconSize} from '../../../styles/Dimension';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Validator from '../../../utils/Validator';

export default class RegisterStep2 extends React.Component {

    constructor(props){
        super(props);
        const userStep1 = this.props.navigation.getParam('userStep1');
        this.state={
            userStep1: userStep1,
            fullName: userStep1.fullName ? userStep1.fullName : '',
            phoneNumber: '',
            address: ''
        }
    }

    async componentDidMount() {
        // const name = JSON.parse(await AsyncStorage.getItem('userName'));
        // if (name && name != '') {
        //     this.setState({fullName:name});
        // }        
    }

    RegisterStep2Now = () => {
        if (!Validator.Required([this.state.fullName,this.state.phoneNumber,this.state.address])) {
            Alert.alert('Email, password và xác thực mật khẩu không được để trống');
            return;
        }

        if (!Validator.IsPhoneNumberVN(this.state.phoneNumber)) {
            Alert.alert('Số điện thoại không đúng định dạng!');
            return;
        }

        const userStep2 = {
            email: this.state.userStep1.email,
            password: this.state.userStep1.password,
            fullName: this.state.fullName,
            phoneNumber: this.state.phoneNumber,
            address: this.state.address
        };                
        console.log(userStep2);
        this.props.navigation.navigate('RegisterStep3',{userStep2:userStep2});
    }

    render() {
        return (
            <ImageBackground source={require('../../../../assets/images/bg.jpg')} style={style.ImageBG}>
                <KeyboardAwareScrollView>
                    <View style={style.root}>
                        <Image source={require('../../../../assets/images/logo.png')} style={style.Logo}/>
                        <Text style={style.wellcome}>Chào mứng bạn đến với TIVA !</Text>
                        <View style={style.formGroup}>
                            <View style={style.form}>
                                <Ionicons name='ios-person' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Nhập họ và tên' style={{marginLeft: 5}} value={this.state.fullName}
                                    onChangeText={(text) => this.setState({fullName:text})}
                                />
                            </View>
                            <View style={style.divider}></View>
                            <View style={style.form}>
                                <Ionicons name='ios-call' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Nhập số điện thoại' style={{marginLeft: 5}} value={this.state.phoneNumber}
                                    onChangeText={(text) => this.setState({phoneNumber:text})}
                                />
                            </View>
                            <View style={style.divider}></View>
                            <View style={style.form}>
                                <Ionicons name='ios-pin' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Nhập địa chỉ của bạn' style={{marginLeft: 5}} value={this.state.address}
                                    onChangeText={(text) => this.setState({address:text})}
                                />
                            </View>
                            <View style={style.divider}></View>
                        </View>
                        <Button block style={style.btBlock}
                            onPress={() => this.RegisterStep2Now()}>
                            <Text>{`Bước tiếp theo (2/3)`}</Text> 
                        </Button>
                        <Button block style={style.btBlock}
                            onPress={() => this.props.navigation.goBack()}>
                            <Text>Quay lại</Text> 
                        </Button>
                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>
        )
    }
} 