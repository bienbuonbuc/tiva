import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
const style = StyleSheet.create({
    ImageBG: {
        width: '100%',
        height: '100%',        
    },
    Logo: {
        width: dimensions.fullWidth-80,        
        resizeMode: 'contain',
        marginTop: 60
    },
    root: {               
        alignItems: 'center'
    },
    wellcome: {
        color: Colors.active,
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontSize: 15
    },
    formGroup: {
        marginTop: 40,
        flexDirection: 'column',
        alignItems: 'center'
    },
    form: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    divider: {
        backgroundColor: Colors.active,
        height: 1,
        width: dimensions.fullWidth-30,
    },
    btBlock: {
        backgroundColor: Colors.active,
        borderRadius: 100,
        marginTop: 15,
        marginHorizontal: 15
    },
    options: {
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop: 5
    },
    textOption: {
        fontWeight:'bold',
        color:Colors.active,        
    },
    titleStep3: {
        color: Colors.active,
        width: dimensions.fullWidth,
        paddingHorizontal: 15
    },
    comboBox: {
        backgroundColor: Colors.comboBox,
        borderRadius: 10,
        width: dimensions.fullWidth-30,
        padding: 10,
        marginTop: 10
    },
    textArea: {
        backgroundColor: 'white',
        borderRadius: 10,
        borderColor: Colors.comboBox,
        borderWidth: 1,
        marginTop:10,
        width: dimensions.fullWidth-30,
        alignSelf:'center',
        height:40
    },       
});

export default style;