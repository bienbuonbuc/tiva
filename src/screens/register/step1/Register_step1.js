import React from 'react';
import {View, Text, Input, Button} from 'native-base';
import {ImageBackground, Image, Alert} from 'react-native';
import style from '../style';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../../styles/Color';
import {iconSize} from '../../../styles/Dimension';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {NavigationActions} from 'react-navigation';
import Validator from '../../../utils/Validator';

export default class RegisterStep1 extends React.Component {

    constructor(props){
        super(props);
        const email = this.props.navigation.getParam('email');
        this.state={
            email: email ? email : '',
            password:'',
            confirmPassword:''
        }
    }

    RegisterStep1Now = () => {
        if (!Validator.Required([this.state.email,this.state.password,this.state.confirmPassword])) {
            Alert.alert('Email, password và xác thực mật khẩu không được để trống');
            return;
        }

        if (!Validator.IsEmail(this.state.email)) {
            Alert.alert('Email không đúng định dạng');
            return;
        }
      
        if (!Validator.MinLength(this.state.password,8) || !Validator.MinLength(this.state.confirmPassword,8)) {
            Alert.alert('Password không được nhỏ hơn 8 kí tự');
            return;
        }

        if (this.state.password === this.state.confirmPassword) {
            const userStep1 = {
                email:this.state.email,
                password:this.state.password
            };

            const name = this.props.navigation.getParam('fullName');                        
            //console.log(name);
            if (name) { 
                userStep1.fullName = name;
            }
            //console.log('userStep1',userStep1);
            this.props.navigation.navigate('RegisterStep2',{userStep1:userStep1});
        }
        else Alert.alert('Mật khẩu không trùng khớp!');
    }

    render() {
        return (
            <ImageBackground source={require('../../../../assets/images/bg.jpg')} style={style.ImageBG}>
                <KeyboardAwareScrollView>
                    <View style={style.root}>
                        <Image source={require('../../../../assets/images/logo.png')} style={style.Logo}/>
                        <Text style={style.wellcome}>Chào mừng bạn đến với TIVA !</Text>
                        <View style={style.formGroup}>
                            <View style={style.form}>
                                <Ionicons name='ios-send' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Nhập email của bạn' style={{marginLeft: 5}}
                                    onChangeText={(text) => this.setState({email:text})}
                                />
                            </View>
                            <View style={style.divider}></View>
                            <View style={style.form}>
                                <Ionicons name='ios-lock' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Nhập mật khẩu của bạn' style={{marginLeft: 5}}
                                    secureTextEntry={true}
                                    onChangeText={(text) => this.setState({password:text})}
                                />
                            </View>
                            <View style={style.divider}></View>
                            <View style={style.form}>
                                <Ionicons name='ios-lock' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Xác nhận lại mật khẩu' style={{marginLeft: 5}}
                                    secureTextEntry={true}
                                    onChangeText={(text) => this.setState({confirmPassword:text})}
                                />
                            </View>
                            <View style={style.divider}></View>
                        </View>
                        <Button block style={style.btBlock}
                            onPress={() => this.RegisterStep1Now()}>
                            <Text>ĐĂNG KÝ</Text> 
                        </Button>
                        <Button block style={style.btBlock}
                            onPress={() => this.props.navigation.navigate('Login')}>
                            <Text>Hủy</Text> 
                        </Button>
                    </View> 
                </KeyboardAwareScrollView>
            </ImageBackground>
        )
    }
} 