import React from 'react';
import {View, Text, Input, Button} from 'native-base';
import {ImageBackground, Image, Alert, AsyncStorage,ActivityIndicator, Platform, TouchableOpacity} from 'react-native';
import style from '../style';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../../styles/Color';
import {iconSize} from '../../../styles/Dimension';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AccountAPI from '../../../api/AccountAPI';
import Themes from '../../../styles/Theme';
import Loader from '../../../components/Loader';
import Validator from '../../../utils/Validator';
import * as Facebook from 'expo-facebook';
import { CheckBox } from 'react-native-elements'
import * as firebase from 'firebase';
import * as GoogleSignIn from 'expo-google-sign-in';
import { StackActions, NavigationActions } from 'react-navigation';
//import { AppAuth } from 'expo-app-auth';

// This value should contain your REVERSE_CLIENT_ID
//const { URLSchemes } = AppAuth;

//console.log(JSON.stringify(AppAuth.URLSchemes,null,2));

// const firebaseConfig = {
//     apiKey: "AIzaSyD3bdbluOBr_kGu5CVxhfs82QGx-ymQ9Fc",
//     //apiKey: "AIzaSyBgUxTIf66k0dbpMgqHjQsm67smbI23HWw",
//     authDomain: "tiva-89883.firebaseapp.com",
//     databaseURL: "https://tiva-89883.firebaseio.com/",
//     storageBucket: "gs://tiva-89883.appspot.com"
// };
  
// firebase.initializeApp(firebaseConfig);
const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'BTab' })],
    key:null
  });

export default class FormLogin extends React.Component {

    constructor(props){
        super(props);
        const account=this.props.navigation.getParam('account');
        this.state={
            email: account ? account.email : '',
            password: account ? account.password : '',
            // email: 'vn3ctran@gmail.com',
            // password: '123456a@',
            loading:false,
            saveAccount: true,
            user: ''
        }        
    }    

    componentDidMount() {
        //const email = JSON.parse(await AsyncStorage.getItem('email'));
        //const password = JSON.parse(await AsyncStorage.getItem('password'));
        // this.initAsync()

        // const token = JSON.parse(await AsyncStorage.getItem('token'));
        // if (token) {
        //     //this.setState({email:email,password:password,saveAccount:true});
        //     this.props.navigation.navigate('Home')
        // }
        this.checkToken();
        // else {
        //     this.setState({email:'',password:'',saveAccount:false});
        // }       

        firebase.auth().onAuthStateChanged((user) => {
            if (user != null) {
              console.log("We are authenticated now!");
            }
          
            // Do other things
        });        
    }

    async SignInGoogle() {
        // await Platform.OS==='ios' ?await this.initAsync()
        await this.signInAsync();
    }

    initAsync = async () => {
        try {
            await GoogleSignIn.initAsync({ clientId: '139840216373-l89glnh7oqo35i3eekdioccpck8jnib7.apps.googleusercontent.com'});
            this._syncUserWithStateAsync();
        } catch ({ message }) {
            alert('GoogleSignIn.initAsync(): ' + message);
        }        
    };
    
    _syncUserWithStateAsync = async () => {
        const user = await GoogleSignIn.signInSilentlyAsync();
        // alert('fullname:', user.displayName);
        this.setState({ user });
        const authenticationInfo = {
            token: "",
            role: 2
        };
        
        await AsyncStorage.multiSet([
            ['token',JSON.stringify(authenticationInfo.token)],
            ['role',JSON.stringify(authenticationInfo.role)],
            //['userName',JSON.stringify(name)]
        ]);            
        //this.props.navigation.navigate('BTab');
        this.props.navigation.navigate('RegisterStep1',{fullName:user.displayName,email:user.email});
    };

    signInAsync = async () => {
        try {
          await GoogleSignIn.askForPlayServicesAsync();
          const { type, user } = await GoogleSignIn.signInAsync();
          if (type === 'success') {
            alert('ID:',user.uid)
            this._syncUserWithStateAsync();
          }
        } catch ({ message }) {
          alert('login: Error:' + message);
        }
    };

    async loginWithFacebook() {
        await Facebook.initializeAsync(
           '553584265133449',
        );
      
        const { type, token } = await Facebook.logInWithReadPermissionsAsync(
          { permissions: ['public_profile'] }
        );
      
        if (type === 'success') {
            
          // Build Firebase credential with the Facebook access token.
          const credential = firebase.auth.FacebookAuthProvider.credential(token);
            console.log('jjjjjjjjjjjjjj',credential)
          const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);

            const authenticationInfo = {
                token: token,
                role: 2
            };
            const name = (await response.json());
            
            console.log('kkkkkkkkkkkkkkk',name)
            await AsyncStorage.multiSet([
                ['token',JSON.stringify(authenticationInfo.token)],
                ['role',JSON.stringify(authenticationInfo.role)],
                //['userName',JSON.stringify(name)]
            ]);
            //this.props.navigation.navigate('BTab');
            this.props.navigation.navigate('RegisterStep1',{fullName:name});
                        
            // Sign in with credential from the Facebook user.
            firebase.auth().signInWithCredential(credential).catch((error) => {
                // Handle Errors here.
            });
        }
    }

    login = async () => {
        this.setState({loading:true});                
        const account = {
            email:this.state.email,
            password:this.state.password
        };
          
        if (!Validator.Required([account.email,account.password])) {
            this.setState({loading:false});  
            Alert.alert('Email và password không được để trống');
            return;
        }

        if (!Validator.IsEmail(account.email)) {
            this.setState({loading:false});  
            Alert.alert('Email không đúng định dạng');
            return;
        }
      
        if (!Validator.MinLength(account.password,8)) {
            this.setState({loading:false});  
            Alert.alert('Password không được nhỏ hơn 8 kí tự');
            return;
        }

        await AccountAPI.Login(account)       
        .then( async (res) => {       
            //console.log(res);
            this.setState({loading:false});     
            if (res[0] == 200) {                                        
                await AsyncStorage.multiSet([
                    ['token',JSON.stringify(res[1].token)],
                    ['role',JSON.stringify(res[1].role)]
                ]);            

                if (this.state.saveAccount == true) {
                    await AsyncStorage.multiSet([
                        ['email',JSON.stringify(this.state.email)],
                        ['password',JSON.stringify(this.state.password)]                        
                    ]);  
                }
                else {
                    await AsyncStorage.removeItem('email');
                    await AsyncStorage.removeItem('password');
                }

                    this.props.navigation.dispatch(resetAction);
            } else {               
                Alert.alert("Sai thông tin đăng nhập");
            }
        })
        .catch(err => {
            this.setState({loading:false});     
            Alert.alert('Lỗi: ' + err);
        })
        .done();
    }

    loginWithPhoneNumber = async () => {
        this.setState({loading:true});                
        const account = {
            phone:this.state.email,
            password:this.state.password
        };

        if (!Validator.Required([account.email,account.password])) {
            this.setState({loading:false});  
            Alert.alert('Email và password không được để trống');
            return;
        }
      
        if (!Validator.MinLength(account.password,8)) {
            this.setState({loading:false});  
            Alert.alert('Password không được nhỏ hơn 8 kí tự');
            return;
        }

        await AccountAPI.LoginWithPhoneNumber(account)       
        .then( async (res) => {       
            //console.log(res);
            this.setState({loading:false});     
            if (res[0] == 200) {                                        
                await AsyncStorage.multiSet([
                    ['token',JSON.stringify(res[1].token)],
                    ['role',JSON.stringify(res[1].role)]
                ]);            

                if (this.state.saveAccount == true) {
                    await AsyncStorage.multiSet([
                        ['email',JSON.stringify(this.state.email)],
                        ['password',JSON.stringify(this.state.password)]                        
                    ]);  
                }
                else {
                    await AsyncStorage.removeItem('email');
                    await AsyncStorage.removeItem('password');
                }

                    this.props.navigation.dispatch(resetAction);
            } else {               
                Alert.alert("Sai thông tin đăng nhập");
            }
        })
        .catch(err => {
            this.setState({loading:false});     
            Alert.alert('Lỗi: ' + err);
        })
        .done();
    }

    checkToken = async () => {
        const token = JSON.parse(await AsyncStorage.getItem('token'));
        return fetch('https://tiva.vn/api/check-user?token=' + token)
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.message == 'Token vẫn có thể sử dụng.') {
                console.log(responseJson.message)
                this.props.navigation.navigate('Home')
            }
            return responseJson
        })
        .catch((e) => {
            console.log('loi lấy công việc')
        })
    }

    // async FacebookLogIn() {
    //     try {
    //       await Facebook.initializeAsync("1875296572729968");
    //       const {
    //         type,
    //         token,
    //         expires,
    //         permissions,
    //         declinedPermissions,
    //       } = await Facebook.logInWithReadPermissionsAsync({
    //         permissions: ['public_profile'],
    //       });
    //       if (type === 'success') {
    //         // Get the user's name using Facebook's Graph API
    //         const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
    //         //console.log('token',token);
    //         const authenticationInfo = {
    //             token: token,
    //             role: 2
    //         };
    //         const name = (await response.json()).name;
            
    //         await AsyncStorage.multiSet([
    //             ['token',JSON.stringify(authenticationInfo.token)],
    //             ['role',JSON.stringify(authenticationInfo.role)],
    //             //['userName',JSON.stringify(name)]
    //         ]);            
    //         //this.props.navigation.navigate('BTab');
    //         this.props.navigation.navigate('RegisterStep1',{fullName:name});

    //         //Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
    //       } else {
    //         // type === 'cancel'
    //       }
    //     } catch ({ message }) {
    //       alert(`Facebook Login Error: ${message}`);
    //     }
    //   }

    render() {
        return (
            <ImageBackground source={require('../../../../assets/images/bg.jpg')} style={style.ImageBG}>
                <Loader loading={this.state.loading}/>
                <KeyboardAwareScrollView>
                    <View style={style.root}>
                        <Image source={require('../../../../assets/images/logo.png')} style={[style.Logo, {marginTop:100}]}/>
                        <Text style={style.wellcome}>Chào mứng bạn đến với TIVA !</Text>
                        <View style={style.formGroup}>
                            <View style={style.form}>
                                <Ionicons name='ios-send' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Nhập email hoặc SĐT' style={{marginLeft: 5}}
                                    value={this.state.email}
                                    onChangeText={(text) => this.setState({email:text})}
                                />
                            </View>
                            <View style={style.divider}></View>
                            <View style={style.form}>
                                <Ionicons name='ios-lock' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Nhập mật khẩu của bạn' style={{marginLeft: 5}}
                                    secureTextEntry={true}
                                    value={this.state.password}
                                    onChangeText={(text) => this.setState({password:text})}
                                />
                            </View>                            
                            <View style={style.divider}></View>
                            {/* <View style={style.form}>
                                <CheckBox 
                                    checked={this.state.saveAccount}
                                    onPress={() => this.setState({saveAccount: !this.state.saveAccount})}
                                />
                                <Text>Lưu đăng nhập</Text>
                            </View> */}
                        </View>
                        <TouchableOpacity style={style.btBlock}
                            onPress={() => {this.login()}}>
                            <Text style={{color:'white'}}>ĐĂNG NHẬP</Text>
                        </TouchableOpacity> 
                        {/* <Button small block style={style.btBlock}
                            onPress={() => {this.loginWithPhoneNumber()}}>
                            <Text>ĐĂNG NHẬP BẰNG SĐT</Text>
                        </Button>                                 */}
                        {/* <Button block small style={[style.btBlock,{backgroundColor:'#1976D2'}]}
                            onPress={() => this.loginWithFacebook()}>
                            <Text>ĐĂNG VỚI FACEBOOK</Text>
                        </Button>
                        <Button block small style={[style.btBlock,{backgroundColor:'#F34336'}]}
                            onPress={() => this.SignInGoogle()}>
                            <Text>ĐĂNG VỚI GOOGLE</Text>
                        </Button>       */}
                        <View style={style.options}>                    
                            <Button transparent onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                <Text style={style.textOption}>Quên mật khẩu</Text>
                            </Button>
                            <View></View>
                            <Button transparent onPress={() => this.props.navigation.navigate('Register')}>
                                <Text style={style.textOption}>Đăng ký tài khoản</Text>
                            </Button>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <View style={{flex: 1, justifyContent: 'flex-end'}}>
                        <Text style={style.titleHelp} >
                                Hotline hỗ trợ: 0988 940 068
                        </Text>
                </View>
            </ImageBackground>
        )
    }
} 