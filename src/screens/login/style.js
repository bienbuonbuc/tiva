import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
const style = StyleSheet.create({
    ImageBG: {
        width: '100%',
        height: '100%',        
    },
    Logo: {
        width: dimensions.fullWidth-80,        
        resizeMode: 'contain',
        marginTop: 40
    },
    root: {      

        alignItems: 'center'
    },
    wellcome: {
        color: Colors.active,
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontSize: 15
    },
    formGroup: {
        marginTop: 25,
        flexDirection: 'column',
        alignItems: 'center'
    },
    form: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    divider: {
        backgroundColor: Colors.active,
        height: 1,
        width: dimensions.fullWidth-30,
    },
    btBlock: {
        backgroundColor: Colors.active,
        borderRadius: 100,
        marginTop: 20,
        marginHorizontal: 15,
        width: dimensions.fullWidth-30,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        marginBottom: 5
    },
    options: {
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop: 5
    },
    textOption: {
        fontWeight:'bold',
        color:Colors.active,        
    },
    titleHelp: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        color: Colors.active,
        marginBottom: 20
    },
    actionLogin: {
        borderRadius: 5, 
        flexDirection: 'row', 
        marginTop: 20,
        width: dimensions.fullWidth
      },
      buttonFace: {
        padding: 10,
        width: dimensions.fullWidth*0.425, 
        backgroundColor: '#C48AD5', 
        justifyContent: 'center', 
        alignItems: 'center',
        borderRadius: 40,
        marginLeft: dimensions.fullWidth*0.05,
        flexDirection: 'row'
      },
      buttonGoogle: {
        width: dimensions.fullWidth*0.425,
        backgroundColor: '#C48AD5', 
        padding: 10, 
        justifyContent: "center", 
        alignItems: 'center',
        borderRadius: 40,
        marginLeft: dimensions.fullWidth*0.05,
        flexDirection: 'row'
      },
});

export default style;