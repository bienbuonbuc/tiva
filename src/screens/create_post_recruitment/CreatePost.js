import React from "react";
import { View, Title, Label, Input, Item, Textarea } from "native-base";
import {
  Text,
  TouchableOpacity,
  Alert,
  Image,
  ImageBackground,
  ScrollView,
  Platform,
  AsyncStorage
} from "react-native";
import style from "./style";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Themes from "../../styles/Theme";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../../styles/Color";
import { dimensions } from "../../styles/Dimension";
import RBSheet from "react-native-raw-bottom-sheet";
import Loader from "../../components/Loader";
import CreatePostAPI from "../../api/CreatePostAPI";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import * as FileSystem from "expo-file-system";
import Validator from "../../utils/Validator";
import { StackActions, NavigationActions } from "react-navigation";

export default class CreatePost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchType: 1,
      loading: false,
      careers: [],
      careerIdSelected: 0,
      careerNameIsSelected: "Loại ngành nghề",
      salaries: [],
      salarySelected: 0,
      salaryNameSelected: "Mức lương",
      jobGroupIdSelected: 0,
      jobGroupNameIsSelected: "Loại công việc",
      jobName: "",
      positionName: "",
      description: "",
      imageUri: "",
      jobGroups: [],
      isPickImage: false
    };
  }

  async componentDidMount() {
    this.setState({ loading: true });

    await CreatePostAPI.GetCareers()
      .then(res => {
        this.setState({ careers: res[1] });
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();

    await CreatePostAPI.GetSalaries()
      .then(res => {
        this.setState({ salaries: res[1].salaries.data });
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();

    await CreatePostAPI.GetJobGroup()
      .then(res => {
        this.setState({ loading: false, jobGroups: res[1].job_group });
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();
  }

  renderJob = careers => {
    return careers.map((el, i) => {
      return (
        <TouchableOpacity
          key={i}
          onPress={() => {
            this.setState({
              careerIdSelected: el.career_category_id,
              careerNameIsSelected: el.career_category_name
            });
            this.RBSheet.close();
          }}
        >
          <View style={Themes.rbSheetItemContainer}>
            <View style={{ marginBottom: 15 }}></View>
            <Text>{el.career_category_name}</Text>
            <View style={[Themes.divider, { marginTop: 15 }]}></View>
          </View>
        </TouchableOpacity>
      );
    });
  };

  renderJobGroup = times => {
    return times.map((el, i) => {
      return (
        <TouchableOpacity
          key={i}
          onPress={() => {
            this.setState({
              jobGroupIdSelected: el.job_group_id,
              jobGroupNameIsSelected: el.job_group_name
            });
            this.RBSheet.close();
          }}
        >
          <View style={Themes.rbSheetItemContainer}>
            <View style={{ marginBottom: 15 }}></View>
            <Text>{el.job_group_name}</Text>
            <View style={[Themes.divider, { marginTop: 15 }]}></View>
          </View>
        </TouchableOpacity>
      );
    });
  };

  renderSalary = salaries => {
    return salaries.map((el, i) => {
      return (
        <TouchableOpacity
          key={i}
          onPress={() => {
            this.setState({
              salarySelected: el.salary_id,
              salaryNameSelected: el.description
            });
            this.RBSheet.close();
          }}
        >
          <View style={Themes.rbSheetItemContainer}>
            <View style={{ marginBottom: 15 }}></View>
            <Text>{el.description}</Text>
            <View style={[Themes.divider, { marginTop: 15 }]}></View>
          </View>
        </TouchableOpacity>
      );
    });
  };

  TakePhoto = async () => {
    status === "granted";
    let { status } = await Permissions.getAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      status = await Permissions.askAsync(Permissions.CAMERA_ROLL).status;
      if (status !== "granted") {
        return;
      }
    }
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3]
    });

    if (!result.cancelled) {
      this.setState({ imageUri: result.uri, isPickImage: true });
    }
  };

  RequestCreatePost = async () => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem("token");
    const post = {
      token: JSON.parse(token),
      job_title: this.state.jobName,
      careers: this.state.careerIdSelected + "",
      position: this.state.positionName,
      salary_id: this.state.salarySelected + "",
      job_group: this.state.jobGroupIdSelected + "",
      description: this.state.description,
      image: ""
    };

    if (this.state.isPickImage == true && this.state.imageUri != "") {
      post.image = await FileSystem.readAsStringAsync(this.state.imageUri, {
        encoding: "base64"
      });
      post.image = "data:image/png;base64," + post.image;
      this.setState({ isPickImage: false });
    }

    if (
      !Validator.Required([post.job_title, post.position, post.description])
    ) {
      this.setState({ loading: false });
      Alert.alert("Các trường trong form không được để trống");
      return;
    }

    if (this.state.careerIdSelected == 0) {
      this.setState({ loading: false });
      Alert.alert("Bạn phải chọn loại ngành nghề!");
      return;
    }

    if (this.state.salarySelected == 0) {
      this.setState({ loading: false });
      Alert.alert("Bạn phải chọn mức lương!");
      return;
    }

    if (this.state.jobGroupIdSelected == 0) {
      this.setState({ loading: false });
      Alert.alert("Bạn phải chọn loại công việc");
      return;
    }

    await CreatePostAPI.Create(post)
      .then(res => {
        this.setState({ loading: false });
        if (res[0] == 200) {
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "Management" })]
          });
          this.props.navigation.dispatch(resetAction);
        } else Alert.alert("Có lỗi xảy ra!");
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();
  };

  renderByType = type => {
    if (type == 1)
      return (
        <View style={{ width: "100%" }}>
          <View style={Themes.headerBottomSheet}>
            <TouchableOpacity
              onPress={() => {
                this.RBSheet.close();
              }}
            >
              <Ionicons name="ios-close" size={40} />
            </TouchableOpacity>
            <Text style={Themes.rbSheetTitleHeader}>Loại việc làm</Text>
            <View></View>
          </View>
          <ScrollView style={{ height: 300 }}>
            <View>
              <View style={Themes.divider}></View>
              {this.renderJob(this.state.careers)}
            </View>
          </ScrollView>
        </View>
      );
    else if (type == 2)
      return (
        <View style={{ width: "100%" }}>
          <View style={Themes.headerBottomSheet}>
            <TouchableOpacity
              onPress={() => {
                this.RBSheet.close();
              }}
            >
              <Ionicons name="ios-close" size={40} />
            </TouchableOpacity>
            <Text style={Themes.rbSheetTitleHeader}>Chọn mức lương</Text>
            <View></View>
          </View>
          <ScrollView style={{ height: 300 }}>
            <View>
              <View style={Themes.divider}></View>
              {this.renderSalary(this.state.salaries)}
            </View>
          </ScrollView>
        </View>
      );
    else
      return (
        <View style={{ width: "100%" }}>
          <View style={Themes.headerBottomSheet}>
            <TouchableOpacity
              onPress={() => {
                this.RBSheet.close();
              }}
            >
              <Ionicons name="ios-close" size={40} />
            </TouchableOpacity>
            <Text style={Themes.rbSheetTitleHeader}>Chọn loại công việc</Text>
            <View></View>
          </View>
          <ScrollView style={{ height: 300 }}>
            <View>
              <View style={Themes.divider}></View>
              {this.renderJobGroup(this.state.jobGroups)}
            </View>
          </ScrollView>
        </View>
      );
  };

  render() {
    let { searchType } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <Loader loading={this.state.loading} />
        <View style={Themes.header}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Ionicons name={"ios-arrow-back"} size={30} color={"white"} />
          </TouchableOpacity>
          <Text style={{ color: "white", fontWeight: "bold" }}>
            TẠO TIN TUYỂN DỤNG
          </Text>
          <TouchableOpacity onPress={() => this.RequestCreatePost()}>
            <Title style={{ color: "white" }}>Đăng</Title>
          </TouchableOpacity>
        </View>
        <KeyboardAwareScrollView>
          <View style={{ paddingHorizontal: 15 }}>
            <Item inlineLabel last>
              <Input
                placeholder="Tên công việc"
                value={this.state.jobName}
                onChangeText={text => this.setState({ jobName: text })}
              />
            </Item>
            <TouchableOpacity
              onPress={() => {
                this.setState({ searchType: 1 });
                this.RBSheet.open();
              }}
            >
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: Colors.note,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingVertical: 13,
                  paddingHorizontal: 5
                }}
              >
                <Text style={{ fontSize: 17, color: "gray" }}>
                  {this.state.careerNameIsSelected}
                </Text>
                <Ionicons name="ios-arrow-back" size={20}></Ionicons>
              </View>
            </TouchableOpacity>
            <Item inlineLabel last>
              <Input
                placeholder="Chức vụ"
                value={this.state.positionName}
                onChangeText={text => this.setState({ positionName: text })}
              />
            </Item>
            <TouchableOpacity
              onPress={() => {
                this.setState({ searchType: 2 });
                this.RBSheet.open();
              }}
            >
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: Colors.note,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingVertical: 13,
                  paddingHorizontal: 5
                }}
              >
                <Text style={{ fontSize: 17, color: "gray" }}>
                  {this.state.salaryNameSelected}
                </Text>
                <Ionicons name="ios-arrow-back" size={20}></Ionicons>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.setState({ searchType: 3 });
                this.RBSheet.open();
              }}
            >
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: Colors.note,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingVertical: 13,
                  paddingHorizontal: 5
                }}
              >
                <Text style={{ fontSize: 17, color: "gray" }}>
                  {this.state.jobGroupNameIsSelected}
                </Text>
                <Ionicons name="ios-arrow-back" size={20}></Ionicons>
              </View>
            </TouchableOpacity>
            <Textarea
              placeholder="Mô tả công việc"
              style={{
                height: 70,
                borderBottomWidth: 1,
                borderColor: Colors.note,
                fontSize: 17,
                paddingLeft: 5,
                marginTop: 10
              }}
              value={this.state.description}
              onChangeText={text => this.setState({ description: text })}
            />
            <View style={{ marginTop: 10 }}>
              <Text style={{ fontSize: 17, marginBottom: 5 }}>
                Ảnh (Không bắt buộc)
              </Text>
              <View style={{ borderRadius: 10 }}>
                {this.state.imageUri ? (
                  <TouchableOpacity
                    onPress={() => {
                      this.TakePhoto();
                    }}
                  >
                    <ImageBackground
                      source={{ uri: this.state.imageUri }}
                      style={{ width: dimensions.fullWidth - 30, height: 200 }}
                      imageStyle={{
                        borderRadius: 10,
                        resizeMode: Platform.OS === "ios" ? "sketch" : "contain"
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          borderRadius: 5,
                          padding: 5,
                          backgroundColor: "rgba(52, 52, 52, 0.5)",
                          width: 120,
                          justifyContent: "center",
                          position: "absolute",
                          marginTop: 10,
                          marginLeft: 10
                        }}
                      >
                        <Ionicons name="ios-images" size={20} color={"white"} />
                        <Text style={{ color: "white", marginLeft: 5 }}>
                          Chỉnh sửa
                        </Text>
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    onPress={() => {
                      this.TakePhoto();
                    }}
                  >
                    <ImageBackground
                      source={require("../../../assets/images/card.png")}
                      style={{ width: dimensions.fullWidth - 30, height: 200 }}
                      imageStyle={{
                        borderRadius: 10,
                        resizeMode: Platform.OS === "ios" ? "sketch" : "contain"
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          borderRadius: 5,
                          padding: 5,
                          backgroundColor: "rgba(52, 52, 52, 0.5)",
                          width: 120,
                          justifyContent: "center",
                          position: "absolute",
                          marginTop: 10,
                          marginLeft: 10
                        }}
                      >
                        <Ionicons name="ios-images" size={20} color={"white"} />
                        <Text style={{ color: "white", marginLeft: 5 }}>
                          Chỉnh sửa
                        </Text>
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={350}
          duration={250}
          customStyles={{
            container: Themes.rbSheetContainer
          }}
        >
          {this.renderByType(searchType)}
        </RBSheet>
      </View>
    );
  }
}
