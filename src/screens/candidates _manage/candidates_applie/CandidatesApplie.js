import React, { Component } from 'react';
import { TouchableOpacity, FlatList, Image, Text, AsyncStorage, Platform, ActivityIndicator } from 'react-native';
import {View, Button} from 'native-base';
import Themes from '../../../styles/Theme';
import style from '../style';
import Loader from '../../../components/Loader';
import { NavigationEvents } from 'react-navigation';
import Modal from 'react-native-modalbox';

export default class CandidatesApplie extends Component {
    constructor(props) {
        super(props);
        this.state={
            postRecruitments: [],
            loading: true,
            listCandidates: [],
            next_page_url: '',
            refreshing: false,
            token: '',
            modalHiddenNotifi: false,
            item: ''
        }
    }
    stateTransitions = async(order_id,employee_id) =>{
        this.setState({modalHiddenNotifi:!this.state.modalHiddenNotifi})
        let res = await fetch('https://tiva.vn/api/cap-nhat-trang-thai-uv',{
          method:'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body:JSON.stringify({
            token: this.state.token,
            status: 1,
            order_id: order_id
          })
        })
        let pre = await res.json();
        if(pre.message == 'Cập nhật trạng thái ứng viên sang phỏng vấn thành công') {
            this.props.Detail(this.state.item)
        }
        if(pre.message == 'Tài khoản của bạn hiện không đủ. Vui lòng nạp thêm tiền') {
            alert('Tài khoản của bạn hiện không đủ. Vui lòng nạp thêm tiền')
        }
        if(pre.message == 'Lỗi xảy ra trong quá trình cập nhật.') {
            alert('Ứng viên hoặc công việc này đã bị xóa')
        }
        return pre;
    }
    getListCandidates = async () => {
        const token = JSON.parse(await AsyncStorage.getItem('token'));
        return fetch('https://tiva.vn/api/danh-sach-ung-vien-ung-tuyen-theo-trang-thai?token=' + token + '&status=0')
        .then((response) => response.json())
        .then((responseJson) => {
                this.setState({listCandidates:responseJson.data.data, loading: false, next_page_url: responseJson.data.next_page_url,
                    refreshing:false, token:token, })
            return responseJson
        })
        .catch((e) => {
            this.setState({loading:false})
            console.log('loi lấy ứng viên')
        })
    }

    renderCompanyItem = (item) => {
        return (
            // <TouchableOpacity onPress={() => this.props.Detail(item)}>
            <TouchableOpacity onPress={() => this.setState({item:item,modalHiddenNotifi:!this.state.modalHiddenNotifi})} >
                <View style={style.jobContainer}>
                    {
                        item.image 
                        ? (
                            <Image source={{uri: item.image}} style={style.logoCompany}/>
                        )
                        : (
                            <Image source={require("../../../../assets/images/employer.png")} style={style.logoCompany}/>
                        )
                    }
                    <View style={{height:50}}>
                        <Text style={style.jobTitle} numberOfLines={2} ellipsizeMode='tail'>Tên: {item.employee_name}</Text>
                        <Text>Địa chỉ: {item.address}</Text>
                        <Text>Ngày ứng tuyển: {item.date_order}</Text>
                    </View>
                </View>
                <View style={[Themes.divider,{marginTop:15}]}></View>
            </TouchableOpacity>
        )
    }
    nextPage() {
        return fetch(this.state.next_page_url + '&token='  + this.state.token + '&status=0')
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson)
            {
                let result = responseJson.data.data;
                let preArr = this.state.listCandidates
                let endresult = preArr.concat(result);
                this.setState({listCandidates:endresult,next_page_url:responseJson.data.next_page_url, loading:false})
                return responseJson
            }
        })
        .catch((e) => {
            this.setState({loading:false})
            console.log('lỗi chuyển trang')
        })
    }
    Refresh = async () => {
        await this.setState({refreshing: true})
        this.getListCandidates();
        
        // kéo xuống cập nhật dữ liệu mới
    }
    componentDidMount() {
        this.getListCandidates();
    }
    render() {
        console.log(this.state.loading)
        return(
            <View style={{flex:1}}>
                <NavigationEvents
                        onDidFocus={async() => {
                            this.Refresh();
                            }}
                    />
                <FlatList                        
                            data={this.state.listCandidates}
                            renderItem={({ item }) => this.renderCompanyItem(item)}
                            onEndReached = {async() => {
                            await this.setState({loading: true}) // phải thêm loading: true ở đây, vì khi kéo xuống loading sẽ luôn false, không nhảy thành true được nữa
                            await this.nextPage()
                            }}
                            onEndReachedThreshold={0.01}
                            refreshing = {this.state.refreshing}
                            onRefresh ={this.Refresh}
                            keyExtractor={(item) => item.order_id}
                />
                <Modal style={style.modal}
                    isOpen={this.state.modalHiddenNotifi}
                    position={'center'}
                    backdropOpacity={0.5}
                >
                    <Text style={{fontWeight:'bold', fontSize:16}}>Ứng Viên Muốn Ứng Tuyển Công Việc</Text>
                    <View style={{marginTop:5}}>
                        <Text>Bạn có muốn xem chi tiết ứng viên và hẹn lịch phỏng vấn</Text>
                        <Text>Phí dịch vụ là 10k</Text>
                        <Text>Tiva rất vui khi được bạn tin dùng. Chúc bạn có một ngày mới vui vẻ</Text>
                        <Text>Chân thành cảm ơn bạn!</Text>
                    </View>
                    <View style={{flexDirection:'row', justifyContent:'space-around'}}>
                        <TouchableOpacity onPress={() => this.setState({modalHiddenNotifi:!this.state.modalHiddenNotifi})} 
                            style={{backgroundColor: 'red', marginTop:20, padding:5, paddingHorizontal:20 ,borderRadius:10}}>
                            <Text style={{color:'white'}}> Để sau </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress = {() => this.stateTransitions(this.state.item.order_id,this.state.item.employee_id)}
                            style={{backgroundColor: 'blue', marginTop:20, padding:5, paddingHorizontal:20 ,borderRadius:10}}>
                            <Text style={{color:'white'}}>   Xác nhận   </Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                {this.state.loading && 
                        <View style={Themes.activityIndicatorView}>
                            <ActivityIndicator 
                                size = {Platform.OS === 'ios'?0:'large'}
                                color = "blue"
                            />
                        </View>
                    }
            </View>
        );
    }
}