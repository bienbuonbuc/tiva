import React, { Component } from 'react';
import Themes from '../../styles/Theme';
import style from './style';
import {View, Button} from 'native-base';
import { TouchableOpacity, FlatList, Image, Text, AsyncStorage, Alert } from 'react-native';
import Colors from '../../styles/Color';
import Loader from '../../components/Loader';
import {ScrollableTabView} from '@valdio/react-native-scrollable-tabview';
import CandidatesApplie from './candidates_applie/CandidatesApplie';
import CandidatesFalse from './candidates_false/CandidatesFalse';
import CandidatesSuccess from './candidates_success/CandidatesSuccess';
import CandidatesInterview from './candidates_interview/CandidatesInterview';


export default class CandidatesManage extends Component {

    // constructor(props) {
    //     super(props);
    //     this.state={
    //         postRecruitments: [],
    //         loading: false,
    //         listCandidates: []
    //     }
    // }
    // getListCandidates = async () => {
    //     const token = JSON.parse(await AsyncStorage.getItem('token'));
    //     return fetch('https://tiva.vn/api/danh-sach-ung-vien-ung-tuyen-theo-trang-thai?token=' + token + '&status=0')
    //     .then((response) => response.json())
    //     .then((responseJson) => {
    //             this.setState({listCandidates:responseJson.data.data})
    //         return responseJson
    //     })
    //     .catch((e) => {
    //         console.log('loi lấy ứng viên')
    //     })
    // }

    // renderCompanyItem = (item) => {
    //     return (
    //         <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileRecruitment',{jobId:item.order_id})}>
    //             <View style={style.jobContainer}>
    //                 {
    //                     item.image 
    //                     ? (
    //                         <Image source={{uri: item.image}} style={style.logoCompany}/>
    //                     )
    //                     : (
    //                         <Image source={require("../../../assets/images/employer.png")} style={style.logoCompany}/>
    //                     )
    //                 } 
    //                 <View style={{height:50}}>
    //                     <Text style={style.jobTitle} numberOfLines={2} ellipsizeMode='tail'>{item.title}</Text>
    //                     <Text>{item.phone} đơn ứng tuyển</Text>
    //                     <Text>{item.enterprise_name}</Text>        
    //                 </View>                          
    //             </View>
    //             <View style={[Themes.divider,{marginTop:15}]}></View>
    //         </TouchableOpacity>
    //     )
    // }
    // componentDidMount() {
    //     this.getListCandidates();
    // }
    render() {
        return (
            <View style={{flex:1,backgroundColor:'white'}}>
                {/* <Loader loading={this.state.loading}/> */}
                <View style={Themes.header}>    
                    <View></View>                
                    <Text style={style.titleHeader}>QUẢN LÍ ỨNG VIÊN</Text>
                    <View></View>
                </View>
                <ScrollableTabView tabBarActiveTextColor ={"black"} locked = {false} showsHorizontalScrollIndicator={false}
                                   tabBarInactiveTextColor={'#0090FF'} tabBarUnderlineStyle={{backgroundColor:'black', height: 2}}
                                   tabBarTextStyle = {{}}
                                   style={{borderTopWidth: 1, borderTopColor: 'lightgray', alignItems: 'center', }}
                >
                    <CandidatesApplie Detail={(employee_id)=>this.props.navigation.navigate('DetailCandidates',{info:employee_id})} tabLabel="Đã ứng tuyển" />
                    <CandidatesInterview Detail={(employee_id)=>this.props.navigation.navigate('DetailCandidates',{info:employee_id})} tabLabel="Phỏng vấn" />
                    {/* <CandidatesFalse Detail={(employee_id)=>this.props.navigation.navigate('DetailCandidates',{info:employee_id})} tabLabel="Thất bại" /> */}
                    <CandidatesFalse tabLabel="Thất bại" />
                    <CandidatesSuccess Detail={(employee_id)=>this.props.navigation.navigate('DetailCandidates',{info:employee_id})} tabLabel="Đã đi làm" />
                </ScrollableTabView>
                {/* {
                    this.state.listCandidates.length == 0 && this.state.loading == false
                    ? (
                        <View style={{height:'100%',width:'100%',justifyContent:'center',alignItems:'center'}}>
                            <Text style={{color:Colors.active,fontWeight:'bold',fontSize:17}}>Không có dữ liệu</Text>
                        </View>
                    )
                    : (
                        <FlatList                        
                            data={this.state.listCandidates}
                            renderItem={({ item }) => this.renderCompanyItem(item)}
                            keyExtractor={(item) => item.order_id}
                        /> 
                    )
                }                */}
            </View>
        );
    }
}