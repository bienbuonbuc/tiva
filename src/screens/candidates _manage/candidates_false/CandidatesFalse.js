import React, { Component } from 'react';
import { TouchableOpacity, FlatList, Image, Text, AsyncStorage, Platform, ActivityIndicator } from 'react-native';
import {View, Button} from 'native-base';
import Themes from '../../../styles/Theme';
import style from '../style';
import Loader from '../../../components/Loader';
import { NavigationEvents } from 'react-navigation';

export default class CandidatesFalse extends Component {
    constructor(props) {
        super(props);
        this.state={
            postRecruitments: [],
            loading: true,
            listCandidates: []
        }
    }
    getListCandidates = async () => {
        const token = JSON.parse(await AsyncStorage.getItem('token'));
        return fetch('https://tiva.vn/api/danh-sach-ung-vien-ung-tuyen-theo-trang-thai?token=' + token + '&status=2')
        .then((response) => response.json())
        .then((responseJson) => {
                console.log(responseJson)
                this.setState({listCandidates:responseJson.data.data, loading: false})
            return responseJson
        })
        .catch((e) => {
            this.setState({loading:false})
            console.log('loi lấy ứng viên')
        })
    }

    renderCompanyItem = (item) => {
        return (
            // <TouchableOpacity onPress={() => this.props.Detail(item)}>
            <TouchableOpacity>
                <View style={style.jobContainer}>
                    {
                        item.image 
                        ? (
                            <Image source={{uri: item.image}} style={style.logoCompany}/>
                        )
                        : (
                            <Image source={require("../../../../assets/images/employer.png")} style={style.logoCompany}/>
                        )
                    }
                    <View style={{height:50}}>
                        <Text style={style.jobTitle} numberOfLines={2} ellipsizeMode='tail'>Tên: {item.employee_name}</Text>
                        <Text>Địa chỉ: {item.address}</Text>
                        <Text>Ngày ứng tuyển: {item.date_order}</Text>
                    </View>
                </View>
                <View style={[Themes.divider,{marginTop:15}]}></View>
            </TouchableOpacity>
        )
    }
    componentDidMount() {
        this.getListCandidates();
    }
    render() {
        return(
            <View>
                <FlatList                        
                            data={this.state.listCandidates}
                            renderItem={({ item }) => this.renderCompanyItem(item)}
                            keyExtractor={(item) => item.order_id}
                />
                {this.state.loading && 
                        <View style={Themes.activityIndicatorView}>
                            <ActivityIndicator 
                                size = {Platform.OS === 'ios'?0:'large'}
                                color = "blue"
                            />
                        </View>
                    }
            </View>
        );
    }
}