import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
const style = StyleSheet.create({
    titleHeader: {
        color:'white',
        fontWeight:'bold'
    },
    jobContainer: {
        width:'100%',
        paddingHorizontal:15,
        flexDirection:'row',
        alignItems:'center',
        paddingTop:15
    },
    jobTitle: {        
        color: Colors.active,
        fontWeight:'bold'
    },
    logoCompany: {
        width:50,
        height:50,
        marginRight:15
    },
    modal: {
        height: dimensions.fullHeight*0.35,
        borderRadius: 10,
        padding: 10,
        width: dimensions.fullWidth*0.85
    },
});
export default style;