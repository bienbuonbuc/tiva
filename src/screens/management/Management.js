import React from 'react';
import {View, Button} from 'native-base';
import Themes from '../../styles/Theme';
import style from './style';
import { TouchableOpacity, FlatList, Image, Text, AsyncStorage, Alert } from 'react-native';
import Colors from '../../styles/Color';
import ManagementAPI from '../../api/ManagementAPI';
import NotifyEmptyData from '../../components/NotifyEmptyData';
import Loader from '../../components/Loader';

export default class Management extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            postRecruitments: [],
            loading: false,
        }
    }

    async componentDidMount() {
        this.setState({loading:true});
        const token = await AsyncStorage.getItem('token');                 
        await ManagementAPI.GetAllPostRecruitment({token:token})
        .then(async (res) => {
            //console.log(res);
            this.setState({loading:false});
            if (res[0] == 200) {
                //console.log(res[0]);
                this.setState({postRecruitments:res[1].data.data});
            }
            else {
                this.setState({postRecruitments:res[1].data});
            } 
        })
        .catch((err) => {
            this.setState({loading:false});
            Alert.alert('Lỗi: ' + err);
        })
        .done();
    }

    renderCompanyItem = (item) => {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileRecruitment',{jobId:item.job_id})}>
                <View style={style.jobContainer}>
                    {
                        item.image 
                        ? (
                            <Image source={{uri: item.image}} style={style.logoCompany}/>
                        )
                        : (
                            <Image source={require("../../../assets/images/employer.png")} style={style.logoCompany}/>
                        )
                    } 
                    <View style={{height:50}}>
                        <Text style={style.jobTitle} numberOfLines={2} ellipsizeMode='tail'>{item.title}</Text>
                        <Text>{item.number_recruited} đơn ứng tuyển</Text>
                        <Text>{item.status}</Text>        
                    </View>                          
                </View>
                <View style={[Themes.divider,{marginTop:15}]}></View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{flex:1,backgroundColor:'white'}}>
                <Loader loading={this.state.loading}/>
                <View style={Themes.header}>    
                    <View></View>                
                    <Text style={style.titleHeader}>QUẢN LÝ TUYỂN DỤNG</Text>
                    <View></View>
                </View>
                <Button full style={{backgroundColor:'white',justifyContent:'flex-start',paddingHorizontal:15}}
                    onPress={() => this.props.navigation.navigate('CreatePost')}>
                    <Text style={{color:Colors.active}}>Tạo tin tuyển dụng...</Text>
                </Button>
                <View style={Themes.dividerLarge}></View>
                {
                    this.state.postRecruitments.length == 0 && this.state.loading == false
                    ? (
                        <View style={{height:'100%',width:'100%',justifyContent:'center',alignItems:'center'}}>
                            <Text style={{color:Colors.active,fontWeight:'bold',fontSize:17}}>Không có dữ liệu</Text>
                        </View>
                    )
                    : (
                        <FlatList                        
                            data={this.state.postRecruitments}
                            renderItem={({ item }) => this.renderCompanyItem(item)}
                            keyExtractor={(item) => item.job_id}
                        /> 
                    )
                }               
            </View>
        )
    }
}