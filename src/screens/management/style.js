import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
const style = StyleSheet.create({
    titleHeader: {
        color:'white',
        fontWeight:'bold'
    },
    jobContainer: {
        width:'100%',
        paddingHorizontal:15,
        flexDirection:'row',
        alignItems:'center',
        paddingTop:15
    },
    jobTitle: {        
        color: Colors.active,
        fontWeight:'bold'
    },
    logoCompany: {
        width:50,
        height:50,
        marginRight:15
    },
});
export default style;