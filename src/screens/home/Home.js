import React from 'react';
import {View, Button} from 'native-base';
import style from './style';
import {Ionicons} from '@expo/vector-icons';
import { ScrollView, TouchableOpacity, FlatList, TextInput, Text, Alert, ActivityIndicator } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Themes from '../../styles/Theme';
import { CheckBox } from 'react-native-elements'
import HomeAPI from '../../api/HomeAPI';
import Colors from '../../styles/Color';
import Loader from '../../components/Loader';

//#region fake data
const searchs = [
    {
        Id: 1,
        Name: 'Địa điểm'
    },
    {
        Id: 2,
        Name: 'Ngành nghề'
    },
    {
        Id: 3,
        Name: 'Loại công việc'
    },
    
];
//#endregion

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchType: 1,
            careerChecked: [],   
            addressChecked: [],
            jobTypesChecked: [],    
            jobs:[],
            loading: false,
            page:1,
            last_page:0,
            filterCondition: {
                group_id: [],
                career_id: [],
                province_id: [],
                job_title: ''
            }
        }
    }

    async componentDidMount() {
        this.setState({loading:true});

        // TODO: init career checked add property check to object (customize object)        
        await HomeAPI.GetCareers()
        .then(async (res) => {
            await res[1].map((el,i) => {
                el.checked = false;
            });
            this.setState({careerChecked: res[1]});
        })
        .catch(err => {
            //this.setState({loading:false});
            Alert.alert('Lỗi: ' + err);
        })
        .done();
        
        // TODO: init address checked
        await HomeAPI.GetProvinces()
        .then(async(res) => {
            await res[1].provinces.map((el,i) => {
                el.checked = false;
            });
            this.setState({addressChecked:res[1].provinces});
        })
        .catch(err => {
            //this.setState({loading:false});
            Alert.alert('Lỗi: ' + err);
        })
        .done();

        // TODO: init job type checked
        await HomeAPI.GetJobTypes() 
        .then(async(res) => {
            await res[1].job_group.map((el,i) => {
                el.checked = false;
            });
            this.setState({jobTypesChecked:res[1].job_group});         
        })
        .catch(err => {
            //this.setState({loading:false}); 
            Alert.alert('Lỗi: ' + err);
        })
        .done();         
        
        await this.GetAllJob(this.state.filterCondition,this.state.page);      
    }

    renderJobItem = (item) => {
        return (
            <TouchableOpacity onPress={() => {this.props.navigation.navigate('JobDetail',{jobId:item.job_id});}}>
                <View style={style.jobContainer}>
                    <Text style={style.jobTitle}>{item.title}</Text>
                    <View style={style.timeContainer}>
                        <Text style={style.txtSalary}>{item.salary}</Text>
                    </View>
                    <Text>{item.enterprise_name}</Text>
                    <Text>{item.address}</Text>
                    <View style={style.divider}></View>
                </View>
            </TouchableOpacity>
        )
    }

    searchPress = (item) => {                
        this.setState({searchType: item.Id});
        this.RBSheet.open();        
    }

    renderItemSearch = (searchData) => {
        return searchData.map((el,i) => {
            return (
                <TouchableOpacity key={i} onPress={() => {this.searchPress(el)}}>
                    <View style={Themes.searchItem}>
                        <Text>{el.Name}</Text>                        
                    </View>
                </TouchableOpacity>
            )
        })
    }

    //#region filter handle
    HandleLoadmore = (last_page) => {        
        console.log('1');
        if (this.state.page != last_page) {
            console.log('h')
            //this.componentRef.FlatList.scrollToOffset({ animated: true, offset: 1 });
            this.setState({page: this.state.page + 1});
            this.GetAllJob(this.state.filterCondition,this.state.page + 1);                
        }      
    }    

    // core
    GetAllJob = async (filter,page) => {
        console.log(filter);
        await HomeAPI.GetAllJob(filter,page)
        .then((res) => {
            this.setState({
                loading:false, 
                last_page: res[1].data.last_page, 
                jobs: this.state.jobs.concat(res[1].data.data)
            });
        })
        .catch((err) => {
            this.setState({loading:false});
            Alert.alert('Lỗi: ' + err)
        });
    }

    FilterByName = () => {
        this.setState({loading:true,jobs:[]});
        //console.log('FilterByName',this.state.filterCondition);
        this.GetAllJob(this.state.filterCondition,this.state.page);
    }

    FilterByCareer = () => {
        this.setState({jobs:[]});                  
        let careerIds = [];
        this.state.careerChecked.map((el,i) => {
            if (el.checked === true) {
                careerIds.push(el.career_category_id);
            }
        });
        const filterConditionNew = this.state.filterCondition;
        filterConditionNew.career_id = careerIds;
        this.setState({loading:true,filterCondition:filterConditionNew});
        this.RBSheet.close();
        //console.log('FilterByCareer',this.state.filterCondition);
        this.GetAllJob(this.state.filterCondition,this.state.page);
    }

    FilterByProvince = () => {    
        this.setState({jobs:[]});       
        let provinceIds = [];
        this.state.addressChecked.map((el,i) => {
            if (el.checked === true) {
                provinceIds.push(el.province_id);
            }
        });
        const filterConditionNew = this.state.filterCondition;
        filterConditionNew.province_id = provinceIds;
        this.setState({loading:true,filterCondition:filterConditionNew});
        this.RBSheet.close();
        //console.log('FilterByProvince',this.state.filterCondition);
        this.GetAllJob(this.state.filterCondition,this.state.page);
    }

    FilterByJobType = () => { 
        this.setState({jobs:[]});             
        let groupIds = [];
        this.state.jobTypesChecked.map((el,i) => {
            if (el.checked === true) {
                groupIds.push(el.job_group_id);
            }
        });
        const filterConditionNew = this.state.filterCondition;
        filterConditionNew.group_id = groupIds;
        this.setState({loading:true,filterCondition:filterConditionNew});
        this.RBSheet.close();
        //console.log('FilterByJobType',this.state.filterCondition);
        this.GetAllJob(this.state.filterCondition,this.state.page);
    }
    //#endregion

    //#region career 
    checkBoxCheckCareer = (index) => {
        let tempObjectChecked = this.state.careerChecked;
        tempObjectChecked.map((el,i) => {
            if (i == index) {
                el.checked = !el.checked;
            }
        });
        this.setState({careerChecked:tempObjectChecked});
    }

    renderCareer = (careerData) => {
        // TODO: use customize object alternative origin object to use property checked for checkbox       
        return (
            <FlatList                
                data={careerData}
                renderItem={({item,index}) => {
                    return (
                        <View>
                            <View style={Themes.rbSheetItemContainer}>                                                                         
                                <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center'}}>
                                    <Text>{item.career_category_name}</Text>
                                    <CheckBox 
                                        checked={item.checked}
                                        onPress={() => this.checkBoxCheckCareer(index)}
                                    />
                                </View>                                         
                                <View style={Themes.divider}></View>
                            </View>
                        </View>  
                    )
                }}
                keyExtractor={(item) => item.career_category_id.toString()}
            />
        )
    }

    reCheckAllCareer = () => {
        let tempObjectChecked = this.state.careerChecked;
        tempObjectChecked.map((el,i) => {
            el.checked = false;
        });
        this.setState({careerChecked:tempObjectChecked});
    }
    //#endregion

    //#region address
    checkBoxCheckAddress = (index) => {
        let tempObjectChecked = this.state.addressChecked;
        tempObjectChecked.map((el,i) => {
            if (i == index) {
                el.checked = !el.checked;
            }
        });
        this.setState({addressChecked:tempObjectChecked});
    }

    renderAddress = (addressData) => {
        // TODO: use customize object alternative origin object to use property checked for checkbox
        return (
            <FlatList
                data={addressData}
                renderItem={({item,index}) => {
                    return (
                        <View>
                            <View style={Themes.rbSheetItemContainer}>                                                                         
                                <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center'}}>
                                    <Text>{item.province_name}</Text>
                                    <CheckBox 
                                        checked={item.checked}
                                        onPress={() => this.checkBoxCheckAddress(index)}
                                    />
                                </View>                                         
                                <View style={Themes.divider}></View>
                            </View>
                        </View>  
                    )
                }}
                keyExtractor={(item) => item.province_id.toString()}
            />
        )    
    }

    reCheckAllAddress = () => {
        let tempObjectChecked = this.state.addressChecked;
        tempObjectChecked.map((el,i) => {
            el.checked = false;
        });
        this.setState({addressChecked:tempObjectChecked});
    }
    //#endregion

    //#region job type
    checkBoxCheckJobType = (index) => {
        let tempObjectChecked = this.state.jobTypesChecked;
        tempObjectChecked.map((el,i) => {
            if (i == index) {
                el.checked = !el.checked;
            }
        });
        this.setState({jobTypesChecked:tempObjectChecked});
    }

    renderJobType = (jobTypesData) => {
        return (
            <FlatList
                data={jobTypesData}
                renderItem={({item,index}) => {
                    return (
                        <View>
                            <View style={Themes.rbSheetItemContainer}>                                                                         
                                <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center'}}>
                                    <Text>{item.job_group_name}</Text>
                                    <CheckBox 
                                        checked={item.checked}
                                        onPress={() => this.checkBoxCheckJobType(index)}
                                    />
                                </View>                                         
                                <View style={Themes.divider}></View>
                            </View>
                        </View>  
                    )
                }}
                keyExtractor={(item) => item.job_group_id.toString()}
            />
        )         
    }

    reCheckAllJobType = () => {
        let tempObjectChecked = this.state.jobTypesChecked;
        tempObjectChecked.map((el,i) => {
            el.checked = false;
        });
        this.setState({jobTypesChecked:tempObjectChecked});
    }
    //#endregion

    renderByType = (type) => {
        if (type === 1) {
            return (
                <View style={{width:'100%'}}>
                     <View style={Themes.activityIndicatorView}>        
                        <ActivityIndicator animating={this.state.loading} size={'large'} color={Colors.active}/>        
                    </View>
                    <View style={Themes.headerBottomSheet}>
                        <TouchableOpacity onPress={() => {this.RBSheet.close()}}>
                            <Ionicons name='ios-close' size={40} />
                        </TouchableOpacity>
                        <Text style={Themes.rbSheetTitleHeader}>Địa điểm</Text>
                        <TouchableOpacity onPress={() => this.reCheckAllAddress() }>
                            <Text style={Themes.rbSheetBtReCheck}>Đặt lại</Text>
                        </TouchableOpacity>
                    </View>                   
                    <View style={{height:300}}>
                        <View style={Themes.divider}></View>                                   
                        {this.renderAddress(this.state.addressChecked)}
                    </View>                   
                    <View style={{flexDirection:'row', width:'100%', padding:10,}}>
                        <Button 
                            onPress={() => this.FilterByProvince()}
                            style={{flex:1, backgroundColor: '#E4E5EA', marginBottom:5, marginRight:5}} block>
                            <Text style={{color:'black'}}>Áp dụng</Text>
                        </Button>
                        <Button style={{flex:1, backgroundColor: '#E4E5EA', marginBottom:5, marginLeft:5}} block
                            onPress={() => this.RBSheet.close()}>
                            <Text style={{color:'black'}}>Hủy</Text>
                        </Button>
                    </View>
                </View>
            )
        } else if (type === 2) {
            return (
                <View style={{width:'100%'}}>
                    <View style={Themes.headerBottomSheet}>
                        <TouchableOpacity onPress={() => {this.RBSheet.close()}}>
                            <Ionicons name='ios-close' size={40} />
                        </TouchableOpacity>
                        <Text style={Themes.rbSheetTitleHeader}>Ngành nghề</Text>
                        <TouchableOpacity onPress={() => this.reCheckAllCareer() }>
                            <Text style={Themes.rbSheetBtReCheck}>Đặt lại</Text>
                        </TouchableOpacity>
                    </View>                 
                    <View style={{height:300}}>
                        <View style={Themes.divider}></View>                                   
                        {this.renderCareer(this.state.careerChecked)}
                    </View>                   
                    <View style={{flexDirection:'row', width:'100%', padding:10}}>
                        <Button 
                            onPress={() => this.FilterByCareer()}
                            style={{flex:1, backgroundColor: '#E4E5EA', marginBottom:5, marginRight:5}} block>
                            <Text style={{color:'black'}}>Áp dụng</Text>
                        </Button>
                        <Button style={{flex:1, backgroundColor: '#E4E5EA', marginBottom:5, marginLeft:5}} block
                            onPress={() => this.RBSheet.close()}>
                            <Text style={{color:'black'}}>Hủy</Text>
                        </Button>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{width:'100%'}}>
                    <View style={Themes.headerBottomSheet}>
                        <TouchableOpacity onPress={() => {this.RBSheet.close()}}>
                            <Ionicons name='ios-close' size={40} />
                        </TouchableOpacity>
                        <Text style={Themes.rbSheetTitleHeader}>Loại công việc</Text>
                        <TouchableOpacity onPress={() => this.reCheckAllJobType() }>
                            <Text style={Themes.rbSheetBtReCheck}>Đặt lại</Text>
                        </TouchableOpacity>
                    </View>                 
                    <View style={{height:300}}>
                        <View style={Themes.divider}></View>                                   
                        {this.renderJobType(this.state.jobTypesChecked)}
                    </View>                   
                    <View style={{flexDirection:'row', width:'100%', padding:10}}>
                        <Button 
                            onPress={() => this.FilterByJobType()}
                            style={{flex:1, backgroundColor: '#E4E5EA', marginBottom:5, marginRight:5}} block>
                            <Text style={{color:'black'}}>Áp dụng</Text>
                        </Button>
                        <Button style={{flex:1, backgroundColor: '#E4E5EA', marginBottom:5, marginLeft:5}} block
                            onPress={() => this.RBSheet.close()}>
                            <Text style={{color:'black'}}>Hủy</Text>
                        </Button>
                    </View>
                </View>
            )
        }
    }

    componentRef = {};

    render() {
        let {searchType} = this.state;
        return (
            <View style={{flex:1,backgroundColor:'white'}}>    
                <Loader loading={this.state.loading}/>
                <View style={style.headerSearch}>
                    <TouchableOpacity
                        onPress={() => this.FilterByName()}>
                        <Ionicons name='ios-search' size={30} color={'white'}/>
                    </TouchableOpacity>
                    <TextInput style={style.titleHeader} placeholder='Công việc bạn muốn tìm kiếm' 
                        placeholderTextColor='white'
                        value={this.state.filterCondition.job_title}
                        onChangeText={(text) => {
                            const filterConditionNew = this.state.filterCondition;
                            filterConditionNew.job_title = text;
                            this.setState({filterCondition:filterConditionNew});
                        }}
                        />
                    {/* <Text style={style.titleHeader}>Công việc bạn muốn tìm kiếm</Text> */}
                </View>
                <View>
                    <ScrollView horizontal={true}>
                        <View style={Themes.searchContainer}>
                            {this.renderItemSearch(searchs)}
                        </View>
                    </ScrollView>
                </View>
                <FlatList           
                    ListFooterComponent={() => {
                        //console.log('cr page',this.state.page);
                        //console.log('last page', this.state.last_page);
                        return this.state.page === this.state.last_page || this.state.page === 1 || this.state.jobs.length == 0                
                            ? <View></View> 
                            : 
                            <View style={{marginHorizontal:15, marginTop:5,marginBottom:10, flexDirection:'row', backgroundColor: Colors.active, alignItems: 'center', justifyContent: 'center',padding: 10}}>
                                <ActivityIndicator animating={true} size={'small'} color={'white'}/>      
                                <Text style={{color: 'white', fontWeight: 'bold', marginLeft:10}}>Loading...</Text>
                            </View>
                        }
                    }            
                    ref={ (ref) => this.componentRef.FlatList = ref }
                    data={this.state.jobs}
                    renderItem={({ item }) => this.renderJobItem(item)}
                    keyExtractor={(item) => item.job_id.toString()}
                    onEndReached={() => this.HandleLoadmore(this.state.last_page)}
                    onEndReachedThreshold={0.5}
                />  
                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    height={420}
                    duration={250}                                               
                    customStyles={{ 
                        container: Themes.rbSheetContainer,                               
                        }}>
                    {
                        this.renderByType(searchType)
                    }                    
                </RBSheet>
            </View>
        )
    }
}