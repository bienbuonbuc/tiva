import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
import {Platform} from 'react-native';

const style = StyleSheet.create({
    headerSearch: {
        flexDirection:'row',
        backgroundColor:Colors.active,
        height: Platform.OS === 'android' ? 50 : 70,
        width:dimensions.fullWidth,
        alignItems: 'center',
        paddingHorizontal:15,
        paddingBottom:5,
        paddingTop: Platform.OS === 'ios' ? 25 : 0
    },
    titleHeader: {
        color: 'white',
        marginLeft:15,
        fontSize:17
    },
    searchItem: {
        borderRadius:100, 
        marginLeft: 10, 
        padding:10, 
        borderWidth:1, 
        borderColor: '#525252'
    },
    searchContainer: {
        flexDirection:'row', 
        paddingVertical: 10,
        paddingRight:10,
        paddingBottom:10,
    },   
    divider: {
        width:'100%',
        height:1,
        backgroundColor: '#B9BABF',
        marginVertical:5
    },
    jobContainer: {
        width:'100%',
        paddingHorizontal:15
    },
    jobTitle: {
        textTransform:'uppercase',
        color: Colors.active,
        fontWeight:'bold'
    },
    txtSalary: {
        color: Colors.money,
        fontWeight:'bold'
    },
    timeContainer: {
        flexDirection:'row',        
    },    
    
});

export default style;