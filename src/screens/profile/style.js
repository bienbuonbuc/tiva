import {StyleSheet, Platform} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';

const style = StyleSheet.create({
    header: {
        width:'100%',
        height:80,
        backgroundColor:Colors.active
    }
});
export default style;