import React from "react";
import { View, Thumbnail, Button } from "native-base";
import style from "./style";
import {
  Platform,
  ImageBackground,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  ActivityIndicator
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Themes from "../../styles/Theme";
import Colors from "../../styles/Color";
import ProfileAPI from "../../api/ProfileAPI";
import Loader from "../../components/Loader";

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberRecruitmentApplied: 0,
      numberPosted: 0,
      numberRecruitment: 0,
      loading: false,
      emterpriseInfo: {},
      userInfo: {}
    };
    this.props.navigation.addListener("didFocus", this.onScreenFocus);
  }

  onScreenFocus = () => {
    this.Init();
  };

  async Init() {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem("token");

    await ProfileAPI.GetAccountInfo(JSON.parse(token))
      .then(res => {
        this.setState({ loading: false });
        console.log(res[1]);
        if (res[0] == 200) {
          this.setState({
            emterpriseInfo: res[1].employer,
            userInfo: res[1].user
          });
        } else Alert.alert("get account info" + res[1].message);
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();

    await ProfileAPI.NumberRecruitApplied({ token: JSON.parse(token) })
      .then(res => {
        if (res[0] == 200) {
          this.setState({ numberRecruitmentApplied: res[1].data });
        } else Alert.alert("number recruit applied" + res[1].message);
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();

    await ProfileAPI.GetAllPosted({ token: JSON.parse(token) })
      .then(res => {
        if (res[0] == 200) {
          this.setState({ numberPosted: res[1].data });
        } else Alert.alert("all posted" + res[1].message);
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();

    await ProfileAPI.GetAllRecruitment({ token: JSON.parse(token) })
      .then(res => {
        if (res[0] == 200) {
          this.setState({ numberRecruitment: res[1].data });
        }
        // else Alert.alert("all recruitment" + res[1].message);
        // else nếu ứng viên sẽ không xem được thống-kê-ứng-viên, viên bỏ alert này đi, xem được thì xem, ko thì ẩn ^^
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();
  }

  Logout = async () => {
    await AsyncStorage.multiRemove(["token", "role"]);
    await AsyncStorage.multiRemove(["email", "password"]);
    await this.props.navigation.navigate("Login");
  };

  EditAccountInfo = () => {
    const account = {
      name: this.state.userInfo.name,
      enterprise_name: this.state.emterpriseInfo.enterprise_name,
      email: this.state.userInfo.email,
      phone: this.state.userInfo.phone,
      image: this.state.userInfo.image
    };
    this.props.navigation.navigate("EditAccount", { account: account });
  };

  EditCompanyInfo = () => {
    const companyInfo = {
      name: this.state.emterpriseInfo.enterprise_name,
      phone: this.state.emterpriseInfo.phone,
      email: this.state.emterpriseInfo.email,
      address: this.state.emterpriseInfo.address,
      introduction: this.state.emterpriseInfo.introduction,
      image: this.state.emterpriseInfo.image
    };
    this.props.navigation.navigate("CompanyInfo", { companyInfo: companyInfo });
  };

  EditPersonalInfo = () => {
    const personal = {
      name: this.state.userInfo.name,
      phone: this.state.userInfo.phone,
      email: this.state.userInfo.email,
      image: this.state.userInfo.image
    };
    this.props.navigation.navigate("PersonalInfo", { personal: personal });
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <View style={style.header}></View>
        {/* <Loader loading={this.state.loading}/> */}
        <View
          style={[
            Themes.activityIndicatorView,
            { zIndex: 200000, position: "absolute" }
          ]}
        >
          <ActivityIndicator
            animating={this.state.loading}
            size={"large"}
            color={Colors.active}
          />
        </View>
        <View
          style={{
            borderRadius: 1000,
            position: "absolute",
            zIndex: 100000,
            alignItems: "center",
            justifyContent: "center",
            alignSelf: "center",
            backgroundColor: "#3598DB",
            marginTop: 60 / 2,
            height: 100,
            width: 100,
            borderWidth: 3,
            borderColor: "white"
          }}
        >
          {this.state.emterpriseInfo != null ? (
            this.state.emterpriseInfo.image ? (
              <Image
                source={{ uri: this.state.userInfo.image }}
                style={{ width: 60, height: 60, resizeMode: "contain" }}
              />
            ) : (
              <Image
                source={require("../../../assets/images/user.png")}
                style={{ width: 60, height: 60, resizeMode: "contain" }}
              />
            )
          ) : (
            <Image
              source={require("../../../assets/images/user.png")}
              style={{ width: 60, height: 60, resizeMode: "contain" }}
            />
          )}
        </View>
        <ImageBackground
          source={require("../../../assets/images/bg.jpg")}
          style={{ paddingTop: 100 / 2, width: "100%", height: 130 }}
        >
          <View style={{ alignItems: "center", paddingHorizontal: 15 }}>
            <TouchableOpacity onPress={() => this.EditAccountInfo()}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                {this.state.userInfo != null ? (
                  this.state.userInfo.name ? (
                    <Text style={{ fontWeight: "bold", marginRight: 5 }}>
                      {this.state.userInfo.name}
                    </Text>
                  ) : (
                    <Text style={{ fontWeight: "bold", marginRight: 5 }}>
                      Chưa cập nhật tên doanh nghiệp
                    </Text>
                  )
                ) : (
                  <Text style={{ fontWeight: "bold", marginRight: 5 }}>
                    Chưa cập nhật tên doanh nghiệp
                  </Text>
                )}
                <Ionicons name="md-create" size={20} />
              </View>
            </TouchableOpacity>
            {this.state.userInfo != null ? (
              this.state.userInfo.name ? (
                <Text numberOfLines={1} ellipsizeMode="tail">
                  {this.state.userInfo.name}
                </Text>
              ) : (
                <Text style={{ fontWeight: "bold", marginRight: 5 }}>
                  Chưa cập nhật tên doanh nghiệp
                </Text>
              )
            ) : (
              <Text style={{ fontWeight: "bold", marginRight: 5 }}>
                Chưa cập nhật tên doanh nghiệp
              </Text>
            )}
            {/* <Text numberOfLines={1} ellipsizeMode='tail'>{this.state.emterpriseInfo.enterprise_name}</Text> */}
            {this.state.userInfo != null ? (
              this.state.userInfo.email ? (
                <Text>{this.state.emterpriseInfo.email}</Text>
              ) : (
                <Text>Chưa cập nhật email doanh nghiệp</Text>
              )
            ) : (
              <Text>Chưa cập nhật email doanh nghiệp</Text>
            )}
            {/* <Text>{this.state.emterpriseInfo.email}</Text> */}
            {this.state.userInfo != null ? (
              this.state.userInfo.email ? (
                <Text>{this.state.emterpriseInfo.phone}</Text>
              ) : (
                <Text>Chưa cập nhật số điện thoại doanh nghiệp</Text>
              )
            ) : (
              <Text>Chưa cập nhật số điện thoại doanh nghiệp</Text>
            )}
            {/* <Text>{this.state.emterpriseInfo.phone}</Text> */}
          </View>
        </ImageBackground>
        <ScrollView>
          <View>
            <View style={Themes.dividerLarge}></View>
            <View
              style={{
                width: "100%",
                alignItems: "center",
                backgroundColor: Colors.active,
                paddingVertical: 10
              }}
            >
              <Text style={{ fontWeight: "bold", color: "white" }}>
                Thống kê dành cho bạn
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  width: "100%"
                }}
              >
                <View
                  style={{
                    width: 100,
                    height: 50,
                    backgroundColor: "white",
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 10,
                    marginTop: 10
                  }}
                >
                  <Text style={{ fontWeight: "bold" }}>
                    {this.state.numberRecruitment + ""}
                  </Text>
                  <Text style={{ fontSize: 12 }}>Tất cả ứng viên</Text>
                </View>
                <View
                  style={{
                    marginHorizontal: 5,
                    width: 100,
                    height: 50,
                    backgroundColor: "white",
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 10,
                    marginTop: 10
                  }}
                >
                  <Text style={{ fontWeight: "bold" }}>
                    {this.state.numberPosted + ""}
                  </Text>
                  <Text style={{ fontSize: 12 }}>Tin đã đăng</Text>
                </View>
                <View
                  style={{
                    width: 100,
                    height: 50,
                    backgroundColor: "white",
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 10,
                    marginTop: 10
                  }}
                >
                  <Text style={{ fontWeight: "bold" }}>
                    {this.state.numberRecruitmentApplied + ""}
                  </Text>
                  <Text style={{ fontSize: 12 }}>Đã ứng tuyển</Text>
                </View>
              </View>
            </View>
            <View style={Themes.dividerLarge}></View>
            <View
              source={require("../../../assets/images/bg.jpg")}
              style={{ width: "100%", height: "100%", padding: 10 }}
            >
              <View>
                <Text style={{ color: Colors.active, fontWeight: "bold" }}>
                  Cập nhật hồ sơ công ty - cá nhân
                </Text>
                <TouchableOpacity onPress={() => this.EditCompanyInfo()}>
                  <View
                    style={{
                      flexDirection: "row",
                      paddingHorizontal: 10,
                      paddingVertical: 5,
                      borderRadius: 10,
                      borderWidth: 1,
                      marginTop: 10
                    }}
                  >
                    <Image
                      source={require("../../../assets/images/book.png")}
                      style={{ width: 35, height: 35, marginRight: 10 }}
                    />
                    <View>
                      <Text style={{ fontWeight: "bold" }}>
                        Cập nhật hồ sơ công ty
                      </Text>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={{ fontStyle: "italic", width: "80%" }}
                      >
                        Hồ sơ được lưu lại dùng cho thông tin công ty
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.EditPersonalInfo()}>
                  <View
                    style={{
                      flexDirection: "row",
                      paddingHorizontal: 10,
                      paddingVertical: 5,
                      borderRadius: 10,
                      borderWidth: 1,
                      marginTop: 10
                    }}
                  >
                    <Image
                      source={require("../../../assets/images/cv_PNG12.png")}
                      style={{ width: 35, height: 35, marginRight: 10 }}
                    />
                    <View>
                      <Text style={{ fontWeight: "bold" }}>
                        Cập nhật hồ sơ cá nhân - CV
                      </Text>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={{ fontStyle: "italic", width: "80%" }}
                      >
                        Hồ sơ được lưu lại dùng cho thông tin cá nhân
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <Text
                  style={{
                    color: Colors.active,
                    fontWeight: "bold",
                    marginTop: 10
                  }}
                >
                  Quản lý tài khoản nhà tuyển dụng
                </Text>
                <View>
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                  >
                    <Text>Số dư tài khoản</Text>
                    <View style={{ flexDirection: "row" }}>
                      {this.state.emterpriseInfo != null ? (
                        this.state.emterpriseInfo.total_money ? (
                          <Text
                            style={{ color: Colors.money, fontWeight: "bold" }}
                          >
                            {this.state.emterpriseInfo.total_money + " "}
                          </Text>
                        ) : (
                          <Text>0</Text>
                        )
                      ) : (
                        <Text>0</Text>
                      )}
                      {/* <Text style={{color:Colors.money,fontWeight:'bold'}}>{this.state.emterpriseInfo.total_money + ' '}</Text> */}
                      <Text>đ</Text>
                    </View>
                  </View>
                  <View style={Themes.divider}></View>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("HistoryTransaction")
                    }
                    style={{
                      marginTop: 15,
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                  >
                    <Text>Lịch sử giao dịch</Text>
                    <Ionicons name="ios-arrow-back" size={15} />
                  </TouchableOpacity>
                  <View style={Themes.divider}></View>
                </View>
                <View>
                  <View
                    style={{
                      marginTop: 15,
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                  >
                    <Text>Nạp tiền tài khoản</Text>
                    <Ionicons name="ios-arrow-down" size={15} />
                  </View>
                  <View style={Themes.divider}></View>
                </View>
                <Text style={{ alignSelf: "center", marginVertical: 10 }}>
                  Chọn hình thức thanh toán
                </Text>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("OnlinePayment")
                    }
                    style={{
                      flex: 1,
                      padding: 10,
                      backgroundColor: "#E3D4E5",
                      borderRadius: 10,
                      padding: 10,
                      marginRight: 5,
                      alignItems: "center"
                    }}
                  >
                    <Image
                      source={require("../../../assets/images/visa.png")}
                      style={{ resizeMode: "contain", width: 120 }}
                    />
                    <Text style={{ fontWeight: "bold", marginTop: 5 }}>
                      TT trực tuyến
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("TransferPayment")
                    }
                    style={{
                      flex: 1,
                      padding: 10,
                      backgroundColor: "#E3D4E5",
                      borderRadius: 10,
                      padding: 10,
                      marginLeft: 5,
                      alignItems: "center"
                    }}
                  >
                    <Image
                      source={require("../../../assets/images/bank.png")}
                      style={{ resizeMode: "contain", width: 120 }}
                    />
                    <Text style={{ fontWeight: "bold", marginTop: 5 }}>
                      TT chuyển khoản
                    </Text>
                  </TouchableOpacity>
                </View>
                <Button
                  block
                  style={{
                    backgroundColor: Colors.active,
                    borderRadius: 100,
                    marginTop: 10
                  }}
                  onPress={() => this.Logout()}
                >
                  <Text style={{ color: "white" }}>Đăng xuất</Text>
                </Button>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
