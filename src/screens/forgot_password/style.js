import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
const style = StyleSheet.create({
    ImageBG: {
        width: '100%',
        height: '100%',        
    },
    Logo: {
        width: dimensions.fullWidth-80,        
        resizeMode: 'contain',
        marginTop: 80
    },
    root: {               
        alignItems: 'center'
    },
    wellcome: {
        color: Colors.active,
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontSize: 15
    },
    formGroup: {
        marginTop: 50,
        flexDirection: 'column',
        alignItems: 'center'
    },
    form: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    divider: {
        backgroundColor: Colors.active,
        height: 1,
        width: dimensions.fullWidth-30,
    },
    btBlock: {
        backgroundColor: Colors.active,
        borderRadius: 100,
        marginTop: 20,
        marginHorizontal: 15
    },
    options: {
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop: 5
    },
    textOption: {
        fontWeight:'bold',
        color:Colors.active,        
    },
    viewBack: {
        flexDirection:'row',
        alignItems:'center',
        alignSelf:'flex-start',
        marginLeft:10      
    },
    description: {
        width: dimensions.fullWidth-30,        
    }
});
export default style;