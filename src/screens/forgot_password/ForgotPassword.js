import React from 'react';
import {View, Text, Input, Button} from 'native-base';
import {ImageBackground, Image, TouchableOpacity, Alert, ActivityIndicator} from 'react-native';
import style from './style';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../styles/Color';
import {iconSize} from '../../styles/Dimension';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AccountAPI from '../../api/AccountAPI';
import Themes from '../../styles/Theme';
import Loader from '../../components/Loader';

export default class ForgotPassword extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            email: '',
            loading:false
        }
    }

    GetPasswordNow = async () => {
        this.setState({loading:true});
        await AccountAPI.ForgotPassword({email:this.state.email})
        .then(async (res) => {
            this.setState({loading:false});     
            if (res[0] == 200) {          
                await this.props.navigation.navigate('Login');
            } else {              
                Alert.alert("Có lỗi xảy ra");
            }
        })
        .catch((err) => {
            this.setState({loading:false});     
            Alert.alert('Lỗi: ' + err)
        })
        .done();
    }

    render() {
        return (
            <ImageBackground source={require('../../../assets/images/bg.jpg')} style={style.ImageBG}>
                <Loader loading={this.state.loading}/>
                <KeyboardAwareScrollView>
                    <View style={style.root}>
                        <Image source={require('../../../assets/images/logo.png')} style={style.Logo}/>
                        <Text style={style.wellcome}>Chào mứng bạn đến với TIVA !</Text>
                        <View style={style.formGroup}>
                            <View style={style.form}>
                                <Ionicons name='ios-send' size={iconSize.md} color={Colors.active}/>
                                <Input placeholder='Nhập email của bạn' style={{marginLeft: 5}}
                                    onChangeText={(text) => this.setState({email:text})}
                                />
                            </View>
                            <View style={style.divider}></View>                         
                        </View>
                        <Button block style={style.btBlock}
                            onPress={() => this.GetPasswordNow()}>
                            <Text>LẤY LẠI MẬT KHẨU</Text>
                        </Button>
                        <TouchableOpacity style={style.viewBack} onPress={()=>this.props.navigation.navigate('Login')}>
                            <View style={style.viewBack}>                                                            
                                <Ionicons name='ios-arrow-round-back' size={45} color={Colors.active}/>
                                <Text style={{color: Colors.active, marginLeft: 10}}>Quay lại đăng nhập</Text>                                       
                            </View>
                        </TouchableOpacity>
                        <Text style={style.description}>Mật khẩu truy cập sẽ được gửi vào Email dùng để đăng ký tài khoản của bạn. Nếu bạn không thấy Email, vui lòng thực hiện lại thao tác</Text>
                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>
        )
    }
} 