import React from "react";
import {
  View,
  Item,
  Input,
  DatePicker,
  Picker,
  Textarea,
  Button
} from "native-base";
import {
  Text,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  ActivityIndicator
} from "react-native";
import BasicHeader from "../../components/BasicHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import style from "./style";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../../styles/Color";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import * as FileSystem from "expo-file-system";
import Validator from "../../utils/Validator";
import CompanyInfoAPI from "../../api/CompanyInfoAPI";
import Loader from "../../components/Loader";
import { StackActions, NavigationActions } from "react-navigation";
import Themes from "../../styles/Theme";

export default class CompanyInfo extends React.Component {
  constructor(props) {
    super(props);
    const companyInfo = this.props.navigation.getParam("companyInfo");
    this.state = {
      enterpriseName: companyInfo.name,
      phoneNumber: companyInfo.phone,
      email: companyInfo.email,
      address: companyInfo.address,
      description: companyInfo.introduction,
      avatarUri: companyInfo.image ? companyInfo.image : "",
      isPickImage: false,
      loading: false
    };
  }

  TakePhoto = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3]
    });

    if (!result.cancelled) {
      this.setState({ avatarUri: result.uri, isPickImage: true });
    }
  };

  UpdateInfo = async () => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem("token");
    //console.log(token);
    const companyInfo = {
      token: JSON.parse(token),
      enterprise_name: this.state.enterpriseName,
      phone: this.state.phoneNumber,
      email: this.state.email,
      address: this.state.address,
      introduction: this.state.description
    };

    if (this.state.isPickImage == true && this.state.avatarUri != "") {
      companyInfo.image = await FileSystem.readAsStringAsync(
        this.state.avatarUri,
        { encoding: "base64" }
      );
      companyInfo.image = "data:image/png;base64," + companyInfo.image;
      this.setState({ isPickImage: false });
    }

    //const enterprise = JSON.stringify(companyInfo);
    //console.log(enterprise);

    if (
      !Validator.Required([
        companyInfo.enterprise_name,
        companyInfo.phone_number,
        companyInfo.email,
        companyInfo.address,
        companyInfo.description
      ])
    ) {
      this.setState({ loading: false });
      Alert.alert("Các trường trong form không được để trống!");
      return;
    }

    if (!Validator.IsPhoneNumberVN(companyInfo.phone)) {
      this.setState({ loading: false });
      Alert.alert("Số điện thoại sai định dạng!");
      return;
    }

    if (!Validator.IsEmail(companyInfo.email)) {
      this.setState({ loading: false });
      Alert.alert("Email sai định dạng!");
      return;
    }

    await CompanyInfoAPI.UpdateEnterpriseInfo(companyInfo)
      .then(res => {
        //console.log(res);
        this.setState({ loading: false });
        if (res[0] == 200) {
          // const resetAction = StackActions.reset({
          //     index: 0,
          //     actions: [NavigationActions.navigate({ routeName: 'Profile' })],
          // });
          // this.props.navigation.dispatch(resetAction);
          this.props.navigation.goBack();
        } else Alert.alert("Có lỗi xảy ra!");
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <BasicHeader
          title="THÔNG TIN CÔNG TY"
          navigation={this.props.navigation}
        />
        <Loader loading={this.state.loading} />
        <KeyboardAwareScrollView>
          <View style={{ paddingHorizontal: 15 }}>
            <Item inlineLabel last>
              <Input
                placeholder="Tên công ty"
                value={this.state.enterpriseName}
                onChangeText={text => this.setState({ enterpriseName: text })}
              />
            </Item>
            <Item inlineLabel last>
              <Input
                placeholder="Số điện thoại"
                value={this.state.phoneNumber}
                onChangeText={text => this.setState({ phoneNumber: text })}
              />
            </Item>
            <Item inlineLabel last>
              <Input
                placeholder="Email"
                value={this.state.email}
                onChangeText={text => this.setState({ email: text })}
              />
            </Item>
            <Item inlineLabel last>
              <Input
                placeholder="Địa chỉ"
                value={this.state.address}
                onChangeText={text => this.setState({ address: text })}
              />
            </Item>
            <Textarea
              placeholder="Mô tả công ty"
              style={{
                height: 70,
                borderBottomWidth: 1,
                borderColor: Colors.note,
                fontSize: 17,
                paddingLeft: 5,
                marginTop: 10
              }}
              value={this.state.description}
              onChangeText={text => this.setState({ description: text })}
            />
            <View style={{ width: "40%" }}>
              <Text style={{ marginTop: 10 }}>Ảnh logo công ty</Text>
              {this.state.avatarUri === "" ? (
                <TouchableOpacity onPress={this.TakePhoto}>
                  <Image
                    source={require("../../../assets/images/logo.png")}
                    style={{
                      marginVertical: 10,
                      width: 70,
                      height: 70,
                      resizeMode: "contain"
                    }}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={this.TakePhoto}>
                  <Image
                    source={{ uri: this.state.avatarUri }}
                    style={{
                      marginVertical: 10,
                      width: 70,
                      height: 70,
                      resizeMode: "contain"
                    }}
                  />
                </TouchableOpacity>
              )}
              <Button
                onPress={() => this.UpdateInfo()}
                style={{
                  backgroundColor: Colors.active,
                  paddingHorizontal: 15,
                  marginBottom: 15
                }}
              >
                <Text style={{ color: "white" }}>Cập nhập</Text>
              </Button>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
