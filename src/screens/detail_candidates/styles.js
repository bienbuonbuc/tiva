import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
const style = StyleSheet.create({
    recruitmentInfoContainer: {
        paddingHorizontal:15,
        paddingTop:15,
        paddingBottom:5
    },
    recruitmentInfo: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',       
    },
    btChangeStatus: {
        marginTop:10,
        borderWidth:1,
        width:'85%',
        height:30,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row'
    },
    favorite: {
        justifyContent:'center',
        alignItems:'center',
        marginLeft:5,
        marginTop:10,
        width:'15%',
        height:30,
        backgroundColor:'#3665FF'
    },
    contactContainer: {
        paddingHorizontal:15,
        paddingVertical:5
    },
    contactItem: {
        flexDirection:'row',
        alignItems:'center',
        paddingVertical:5,
        borderBottomWidth:1,
        borderColor:Colors.note
    },
    experienceContainer: {
        paddingHorizontal:15,
        paddingVertical:5
    }
});
export default style;