import React from 'react';
import {View} from 'native-base';
import {Text, TouchableOpacity, ScrollView, Alert, AsyncStorage} from 'react-native';
import style from './styles';
import Themes from '../../styles/Theme';
import BasicHeader from '../../components/BasicHeader';
import {Ionicons} from '@expo/vector-icons';
import RBSheet from 'react-native-raw-bottom-sheet';
import * as MailComposer from 'expo-mail-composer';
import * as SMS from 'expo-sms';
import ApplicationAPI from '../../api/ApplicationAPI';
import Loader from '../../components/Loader';

export default class DetailCandidates extends React.Component {

    constructor(props) {
        super(props);
        const info = this.props.navigation.getParam('info');
        this.state={
            employee_id:info.employee_id,
            job_id: info.job_id,
            profile:{},
            loading:false,
            display: ''
        }
    }

    async componentDidMount() {
        this.setState({loading:true});
        const token = await AsyncStorage.getItem('token');        
        const condition = {
            token:JSON.parse(token),
            employee_id:this.state.employee_id
        };
        await ApplicationAPI.GetEmployeeInfo(condition)
        .then((res) => {            
            this.setState({loading:false});
            if(res[0] == 200) {
                this.setState({profile:res[1].data});
            }
            else Alert.alert("Có lỗi xảy ra!");
        })
        .catch((err) => {
            this.setState({loading:false});
            Alert.alert('Lỗi: ' + err);
        })
        .done();
    }

    HandleSendEmail = async (recipient) => {        
        const {result} = await MailComposer.composeAsync({recipients:[recipient],subject:'Tiva'});            
        // if (result != 'sent') {
        //     Alert.alert('Gửi email không thành công');
        // } 
    };

    HandleSendSms = async (phone) =>  {
        const isAvailable = await SMS.isAvailableAsync();
        if (isAvailable) {
            const { result } = await SMS.sendSMSAsync(
                [phone],
                'Nhập nội dung tin nhắn'
            );            
            // console.log('result',result); // 'sent' is success
            // if (result === "sent") {
            //     Alert.alert('Gửi thành công!');
            // }
        } else {
            Alert.alert('Bạn không có SMS trên thiết bị!');
        }       
    }

    render() {
        return (
            <View style={{flex:1,backgroundColor:'white'}}>
                <BasicHeader title='Thông tin ứng viên' navigation={this.props.navigation}/>
                <Loader loading={this.state.loading}/>
                <View style={style.recruitmentInfoContainer}>
                    <View style={style.recruitmentInfo}>
                        <View>
                            <Text>Họ tên: <Text style={{fontWeight:'bold'}}>{this.state.profile.employee_name}</Text></Text>
                            <Text>Địa chỉ: <Text style={{fontWeight:'bold'}}>{this.state.profile.address}</Text></Text>
                            <Text>Email: <Text style={{fontWeight:'bold'}}>{this.state.profile.email}</Text></Text>
                            <Text>Số điện thoại: <Text style={{fontWeight:'bold'}}>{this.state.profile.phone}</Text></Text>
                        </View>
                        {/* <View style={Themes.statusContainer}>
                            <Text style={{color:'white'}}>{this.GetStatusName(this.state.profile.status == null ? 0 : this.state.profile.status)}</Text>
                        </View> */}
                    </View>                
                </View>
                <View style={Themes.dividerLarge}></View>
                <ScrollView>
                    <View>
                        <View style={style.contactContainer}>
                            <Text style={{fontWeight:'bold'}}>Liên hệ</Text>
                            <View style={[Themes.divider,{marginTop:5}]}></View>
                            <TouchableOpacity style={style.contactItem}
                                onPress={() => this.HandleSendSms(this.state.profile.phone)}>
                                <View style={{width:25}}>
                                    <Ionicons name='ios-chatbubbles' size={20}/>
                                </View>
                                <Text>Gửi tin nhắn</Text>
                            </TouchableOpacity>
                            {/* <TouchableOpacity style={style.contactItem}
                                onPress={() => this.HandlePhoneCall()}>
                                <View style={{width:25}}>
                                    <Ionicons name='ios-call' size={20}/>
                                </View>
                                <Text>Gọi điện</Text>
                            </TouchableOpacity> */}
                            <TouchableOpacity style={style.contactItem}
                                onPress={() => this.HandleSendEmail(this.state.profile.email)}>
                                <View style={{width:25}}>
                                    <Ionicons name='ios-filing' size={20}/>
                                </View>
                                <Text>Gửi Email</Text>
                            </TouchableOpacity>
                            {/* <TouchableOpacity style={style.contactItem}>
                                <View style={{width:25}}>
                                    <Ionicons name='ios-calendar' size={20}/>
                                </View>
                                <Text>Lên lịch phỏng vấn</Text>
                            </TouchableOpacity> */}
                        </View>
                        <View style={Themes.dividerLarge}></View>               
                        <View style={style.experienceContainer}>
                            <Text style={{fontWeight:'bold'}}>Kinh nghiệm làm việc</Text>
                            <Text>{this.state.profile.experience}</Text>
                            {/* <Text style={{fontStyle:'italic'}}>Nhân viên</Text>
                            <Text>{`Kho giao nhận 1,5 năm công ty Lock&Lock tháng 2/2019 - tháng 12/2019 Trực page online bán kính thể thao, lọ hoa, đèn led oto`}</Text> */}
                        </View>
                        {
                            this.state.profile.literacy
                            ?
                            (
                                <View>
                                    <View style={Themes.dividerLarge}></View>
                                    <View style={style.experienceContainer}>
                                        <Text style={{fontWeight:'bold'}}>Học vấn</Text>
                                        <Text>{this.state.profile.literacy}</Text>
                                        {/* <View>
                                            <Text style={{fontStyle:'italic'}}>THPT Tứ Sơn - Lục Nam - Bắc Giang</Text>
                                            <Text style={{fontStyle:'italic'}}>2007 - 2010</Text>
                                            <View style={[Themes.divider,{marginVertical:5}]}></View>
                                        </View>
                                        <View>
                                            <Text style={{fontStyle:'italic'}}>Cao đẳng Nông Lâm Bắc Giang</Text>
                                            <Text style={{fontStyle:'italic'}}>2010 - 2013</Text>
                                            <View style={[Themes.divider,{marginVertical:5}]}></View>
                                        </View> */}
                                    </View>
                                </View>
                            )
                            : <View></View>
                        }                        
                    </View>
                </ScrollView>
            </View>
        )
    }
}