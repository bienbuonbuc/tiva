import React from 'react';
import {View} from 'native-base';
import {Text, AsyncStorage, Alert, FlatList} from 'react-native';
import BasicHeader from '../../components/BasicHeader';
import HistoryTransactionAPI from '../../api/TransactionHistoryAPI';
import Colors from '../../styles/Color';
import Themes from '../../styles/Theme';
import Loader from '../../components/Loader';
import NotifyEmptyData from '../../components/NotifyEmptyData';

export default class HistoryTransaction extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            loading:false,
            historyTransactionData: []
        }
    }

    async componentDidMount() {
        this.setState({loading:true});
        const token = await AsyncStorage.getItem('token');
        await HistoryTransactionAPI.GetAllHistoryTransaction({token:JSON.parse(token)})
        .then((res) => {
            //console.log(res[1].data);
            this.setState({loading:false,historyTransactionData:res[1].data});
        })
        .catch((err) => {
            this.setState({loading:false})
            Alert.alert('Lỗi: ' + err);
        })
        .done();
    }

    renderItem = (item) => {
        return (
            <View>
                <Text style={{color:Colors.active,fontWeight:'bold',textTransform:'uppercase'}}>{item.reason}</Text>
                <Text>Số tiền giao dịch: {item.money}</Text>
                <View style={[Themes.divider,{marginVertical:5}]}></View>
            </View>
        )
    }

    render() {
        return (
            <View style={{flex:1,backgroundColor:'white'}}>
                <BasicHeader title='Lịch sử giao dịch' navigation={this.props.navigation}/>
                <Loader loading={this.state.loading}/>            
                    {
                        this.state.historyTransactionData.length == 0 && this.state.loading==false
                        ? (
                            <NotifyEmptyData/>
                        )
                        : (
                            <View style={{margin:15}}>
                                <FlatList
                                    data={this.state.historyTransactionData}
                                    renderItem={({item}) => this.renderItem(item)}
                                    keyExtractor={(item) => item.employer_transaction_id.toString()}
                                />
                            </View>
                        )
                    }                                    
            </View>
        )
    }
}