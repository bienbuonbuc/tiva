import React from 'react';
import {View} from 'native-base';
import {Text, FlatList} from 'react-native';
import BasicHeader from '../../components/BasicHeader';
import Themes from '../../styles/Theme';
import Colors from '../../styles/Color';

const fakeData = [
    {
        Id:1,
        Name:'Nguyễn Xuân Kết',
        Code:'0711 0003 06679',
        Bank:'Ngân hàng Ngoại thương Việt Nam VCB - chi nhánh Thanh Xuân, Hà Nội',
        Content:'TIVA-N1000'
    },
    {
        Id:2,
        Name:'Công ty cổ phần tri thức Vì Dân',
        Code:'0310 1012 2838 25',
        Bank:'Ngân hàng TMCP Hàng Hải Việt Nam MSB - chi nhánh Đống Đa, Hà Nội',
        Content:'TIVA-N1000'
    },
];
export default class TransferPayment extends React.Component {

    renderItem = (item) => {
        return (
            <View>
                <Text style={{fontWeight:'bold',textTransform:'uppercase',color:'#038AFF'}}>{item.Name}</Text>
                <Text style={{fontWeight:'bold',color: Colors.money}}>{item.Code}</Text>
                <Text>{item.Bank}</Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Text>Nội dung chuyển khoản: </Text>
                    <Text style={{fontWeight:'bold',textTransform:'uppercase',color:Colors.active}}>{item.Content}</Text>
                </View>
                <View style={[Themes.divider,{marginVertical:10}]}></View>
            </View>
        )
    }

    render() {
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <BasicHeader title='THANH TOÁN CHUYỂN KHOẢN' navigation={this.props.navigation}/>
                <View style={{padding:15}}>
                    <Text>{`Bạn có thể đến bất kỳ ngân hàng nào ở Việt Nam (hoặc sử dụng Internet Banking để chuyển tiền theo thông tin bên dưới:)`}</Text>
                    <FlatList
                        style={{marginTop:10}}
                        data={fakeData}
                        renderItem={({item}) => this.renderItem(item)}
                        keyExtractor={(item) => item.Id}
                    />
                </View>
            </View>
        )
    }
}