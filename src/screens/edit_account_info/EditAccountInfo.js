import React from 'react';
import {View,Item,Input,DatePicker,Picker,Textarea, Button} from 'native-base';
import {Text,Image, TouchableOpacity, AsyncStorage, Alert} from 'react-native';
import BasicHeader from '../../components/BasicHeader';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import style from './style';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../styles/Color';
import {CheckBox} from 'react-native-elements';
import * as ImagePicker from 'expo-image-picker';
import * as FileSystem from 'expo-file-system';
import Validator from '../../utils/Validator';
import EditAccountAPI from '../../api/EditAccountAPI';
import Loader from '../../components/Loader';
import { StackActions, NavigationActions } from 'react-navigation';

export default class EditAccount extends React.Component{ 

    constructor(props) {
        super(props);
        const account = this.props.navigation.getParam('account');
        this.state={
            checked:false,
            avatarUri: account.image ? account.image : '',
            //image: account.image ? ('data:image/png;base64,' + account.image) : null,
            //image: account.image ? account.image.length : 0,
            isPickImage:false,
            name: account.name,
            enterpriseName: account.enterprise_name,
            email:account.email,
            phoneNumber:account.phone,
            newPass:'',
            loading:false,
        }
    }

    TakePhoto = async () => {                 
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,            
            aspect: [4, 3],
        });

        if (!result.cancelled) {
            this.setState({avatarUri:result.uri,isPickImage:true});           
        }
    }

    UpdateInfo = async () => {
        this.setState({loading:true});
        const token = await AsyncStorage.getItem('token');      
        const account = {
            token: JSON.parse(token),
            name:this.state.name,
            enterprise_name:this.state.enterpriseName,
            email:this.state.email,
            phone_number:this.state.phoneNumber,
            image:''                
        }
        
        if (this.state.isPickImage == true && this.state.avatarUri != '') {
            account.image = await FileSystem.readAsStringAsync(this.state.avatarUri,{encoding:'base64'});
            account.image = 'data:image/png;base64,' + account.image;            
            this.setState({isPickImage:false});
        }

        if (this.state.checked) {
            account.new_pass = this.state.newPass
            
            if (!Validator.Required([account.new_pass])) {    
                this.setState({loading:false});       
                Alert.alert('Password không được để trống');
                return;
            }

            if (!Validator.MinLength(account.new_pass,8)) {       
                this.setState({loading:false});    
                Alert.alert('Password phải tối thiểu 8 kí tự');
                return;
            }
        }

        if (!Validator.Required([account.name,account.enterprise_name,account.email,account.phone_number])) {           
            this.setState({loading:false});
            Alert.alert('Các trường trong form không được để trống');
            return;
        }

        if (!Validator.IsEmail(account.email)) {
            this.setState({loading:false});
            Alert.alert('Email định dạng không hợp lệ!');
            return;
        }

        if (!Validator.IsPhoneNumberVN(account.phone_number)) {
            this.setState({loading:false});
            Alert.alert('Số điện thoại định dạng không hợp lệ');
            return;
        }

        // call api
        //console.log(account);
        await EditAccountAPI.EditAccountInfo(account)
        .then((res) => {
            //console.log(res[1]);
            this.setState({loading:false});
            if (res[0] == 200) {             
                //this.props.navigation.navigate('Profile');
                // const resetAction = StackActions.reset({
                //     index: 0,
                //     actions: [NavigationActions.navigate({ routeName: 'Profile' })],
                //   });
                // this.props.navigation.dispatch(resetAction);
                this.props.navigation.goBack();
            }
            else Alert.alert('Có lỗi xảy ra!');
        })
        .catch((err) => {
            this.setState({loading:false});
            Alert.alert('Lỗi: ' + err);
        })
        .done();
    }

    render() {
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <BasicHeader title='CHỈNH SỬA THÔNG TIN CÁ NHÂN' navigation={this.props.navigation}/>
                <Loader loading={this.state.loading}/>
                <KeyboardAwareScrollView>
                    <View style={{paddingHorizontal:15}}>
                        <Item inlineLabel last>                          
                            <Input placeholder="Nhập họ và tên"
                                value={this.state.name}
                                onChangeText={(text) => this.setState({name:text})}
                            />
                        </Item>                                                                                                                    
                        <Item inlineLabel last>                          
                            <Input placeholder="Nhập tên công ty"
                                value={this.state.enterpriseName}
                                onChangeText={(text) => this.setState({enterpriseName:text})}
                            />
                        </Item>          
                        <Item inlineLabel last>                          
                            <Input placeholder="Nhập email"
                                value={this.state.email}
                                onChangeText={(text) => this.setState({email:text})}
                            />
                        </Item>    
                        <Item inlineLabel last>                          
                            <Input placeholder="Nhập số điện thoại"
                                value={this.state.phoneNumber}
                                onChangeText={(text) => this.setState({phoneNumber:text})}
                            />
                        </Item>                    
                        <View>
                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                <CheckBox 
                                    checked={this.state.checked}
                                    onPress={() => this.setState({checked:!this.state.checked})}
                                />
                                <Text>Ấn vào đây nếu thay đổi mật khẩu</Text>
                            </View>
                            <Item regular style={{borderRadius:5}}>
                                <Input placeholder='' 
                                    value={this.state.newPass}
                                    onChangeText={(text) => this.setState({newPass:text})}
                                />
                            </Item>
                        </View>
                        <View style={{width:'40%',}}>
                            <Text style={{marginTop:10}}>Ảnh đại diện</Text>
                            {
                                this.state.avatarUri !== '' 
                                ? (
                                    <TouchableOpacity
                                        onPress={this.TakePhoto}>
                                        <Image source={{uri: this.state.avatarUri}} style={{marginVertical:10,width:70,height:70,resizeMode:'contain'}}/>
                                    </TouchableOpacity>
                                )                                
                                : (
                                    <TouchableOpacity
                                        onPress={this.TakePhoto}>
                                        <Image source={require('../../../assets/images/Group19.png')} style={{marginVertical:10,width:70,height:70,resizeMode:'contain'}}/>
                                    </TouchableOpacity>
                                )
                            }
                            <Button style={{backgroundColor:Colors.active,paddingHorizontal:15,marginBottom:15}}
                                onPress={() => this.UpdateInfo()}>
                                <Text style={{color:'white'}}>Cập nhập</Text>
                            </Button>
                        </View>
                    </View>
                </KeyboardAwareScrollView>     
            </View>
        );
    }
}