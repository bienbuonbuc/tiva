import React from 'react';
import {View, Item, Input, Button} from 'native-base';
import {Text,TouchableOpacity,Image, ScrollView} from 'react-native';
import BasicHeader from '../../components/BasicHeader';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const banks=[
    {
        Id:1,
        Image:require('../../../assets/images/bank1.png')
    },
    {
        Id:2,
        Image:require('../../../assets/images/bank2.png')
    },
    {
        Id:3,
        Image:require('../../../assets/images/bank3.png')
    },
    {
        Id:4,
        Image:require('../../../assets/images/bank4.png')
    },
    {
        Id:5,
        Image:require('../../../assets/images/bank5.png')
    },
    {
        Id:6,
        Image:require('../../../assets/images/bank6.png')
    },
    {
        Id:7,
        Image:require('../../../assets/images/bank7.png')
    },
    {
        Id:8,
        Image:require('../../../assets/images/bank8.png')
    },
    {
        Id:9,
        Image:require('../../../assets/images/bank9.png')
    },
    {
        Id:10,
        Image:require('../../../assets/images/bank10.png')
    },
    {
        Id:11,
        Image:require('../../../assets/images/bank11.png')
    },
    {
        Id:12,
        Image:require('../../../assets/images/bank12.png')
    },
    {
        Id:13,
        Image:require('../../../assets/images/bank13.png')
    },
    {
        Id:14,
        Image:require('../../../assets/images/bank14.png')
    },
    {
        Id:15,
        Image:require('../../../assets/images/bank15.png')
    },
    {
        Id:16,
        Image:require('../../../assets/images/bank16.png')
    },
    {
        Id:17,
        Image:require('../../../assets/images/bank17.png')
    },
    {
        Id:18,
        Image:require('../../../assets/images/bank18.png')
    },
    {
        Id:19,
        Image:require('../../../assets/images/bank19.png')
    },
    {
        Id:20,
        Image:require('../../../assets/images/bank20.png')
    },
    {
        Id:21,
        Image:require('../../../assets/images/bank21.png')
    },
    {
        Id:22,
        Image:require('../../../assets/images/bank22.png')
    },
    {
        Id:23,
        Image:require('../../../assets/images/bank23.png')
    },
    {
        Id:24,
        Image:require('../../../assets/images/bank24.png')
    },
    {
        Id:25,
        Image:require('../../../assets/images/bank25.png')
    },
    {
        Id:26,
        Image:require('../../../assets/images/bank26.png')
    },
    {
        Id:27,
        Image:require('../../../assets/images/bank27.png')
    },   
]
export default class OnlinePayment extends React.Component {

    renderBankItem = (bankData) => {
        return bankData.map((el,i) => {
            return (
                <TouchableOpacity key={i}>
                    <Image source={el.Image} style={{width:90,height:70,resizeMode:'contain',marginHorizontal:3}}></Image>
                </TouchableOpacity>
            )
        });
    }   

    render() {
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <BasicHeader title='THANH TOÁN TRỰC TUYẾN' navigation={this.props.navigation}/>
                <KeyboardAwareScrollView>
                    <View style={{padding:15}}>
                        <Text style={{fontWeight:'bold'}}>Chọn ngân hàng thanh toán</Text>
                        <View style={{ flexDirection: 'row',flexWrap: 'wrap',marginVertical:10,alignItems:'center',justifyContent:'center'}}>
                            {this.renderBankItem(banks)}
                        </View>
                        <Text style={{fontWeight:'bold'}}>Nhập số tiền cần thanh toán</Text>
                        <Item regular style={{borderRadius:5,marginTop:10}}>
                            <Input placeholder='Nhập tối thiểu 100.000 VNĐ'/>
                        </Item>
                        <Button style={{paddingHorizontal:15,backgroundColor:'#196303',marginTop:10}}>
                            <Text style={{color:'white'}}>Thanh toán</Text>
                        </Button>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}