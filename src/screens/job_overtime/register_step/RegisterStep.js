import React from 'react';
import {View, Text, Input, Button, Textarea} from 'native-base';
import {ImageBackground, Image, TouchableOpacity, ScrollView, ActivityIndicator, Alert, AsyncStorage} from 'react-native';
import style from '../../register/style';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../../styles/Color';
import {iconSize} from '../../../styles/Dimension';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RBSheet from "react-native-raw-bottom-sheet";
import Themes from '../../../styles/Theme';
import AccountAPI from '../../../api/AccountAPI';
import Validator from '../../../utils/Validator';
import { StackActions, NavigationActions } from 'react-navigation';

const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'BTab' })],
    key:null
  });
export default class RegisterStep extends React.Component {

    constructor(props) {
        super(props);
        this.state= {
            bussiness: [],
            bussinessTypeIdSelected: 0,
            loading: true,
            career_name: 'Chọn ngành nghề',
            phone: '',
            address: '',
            enterprise_name: '',
            token: ''

        }
    }

    async componentDidMount() {
        const token = await AsyncStorage.getItem('token');
        const token1 =await JSON.parse(token)
        this.setState({token:token1})
        AccountAPI.GetBussiness()
        .then(async(res) => {
            this.setState({loading:false,bussiness: res[1].business});
        })
        .catch((err) => {
            this.setState({loading:false});
            Alert.alert('Lỗi: ' + err);
        })        
        .done();
    }

    renderSubject = (subjects) => {
        return subjects.map((el,i) => {
            return (               
                <TouchableOpacity key={i} onPress={() => {
                        this.setState({bussinessTypeIdSelected: el.business_type_id,career_name:el.business_type_name});
                        this.RBSheet.close();
                    }}>
                    <View style={Themes.rbSheetItemContainer}>                                                  
                        <View style={{marginBottom:15}}></View>                        
                        <Text>{el.business_type_name}</Text>
                        <View style={[Themes.divider,{marginTop:15}]}></View>
                    </View>
                </TouchableOpacity>               
            );
        });
    }

    Register = () => {
        this.setState({loading:true});

        if (!Validator.Required([this.state.enterprise_name])) {
            this.setState({loading:false});
            Alert.alert('Tên Công Ty không được để trống!');
            return;
        }
        if (!Validator.Required([this.state.phone])) {
            this.setState({loading:false});
            Alert.alert('Số điện thoại không được để trống!');
            return;
        }
        if (!Validator.Required([this.state.address])) {
            this.setState({loading:false});
            Alert.alert('Địa chỉ không được để trống!');
            return;
        }

        if (this.state.bussinessTypeIdSelected == 0) {
            this.setState({loading:false});
            Alert.alert('Bạn phải chọn ngành nghề!');
            return;
        }

        const account = {};
        let business = [];
        business.push(this.state.bussinessTypeIdSelected);
        
        account.business = business;
        account.token = this.state.token;
        account.phone = this.state.phone;
        account.enterprise_name = this.state.enterprise_name;
        account.address = this.state.address;

        //console.log(account);            
        //return;
            AccountAPI.RegisterNTD(account)
            .then(async (res) => {         
                console.log('kkkkkkkkkk',res[0])
                this.setState({loading:false});
                if (res[0] == 200) {
                    alert(res[1].message)
                    this.props.navigation.navigate('AddJobOvertime');
                }
                else Alert.alert(res[1].message + '');                                  
            })
            .catch((err) => {
                this.setState({loading:false});
                Alert.alert("Lỗi rồi: " + err);
            })
            .done();
    }

    render() {
        return (           
            <ImageBackground source={require('../../../../assets/images/bg.jpg')} style={style.ImageBG}>
                <View style={[Themes.activityIndicatorView,{zIndex:100000,position:'absolute'}]}>        
                    <ActivityIndicator animating={this.state.loading} size={'large'} color={Colors.active}/>        
                </View>  
                <KeyboardAwareScrollView>
                    <View style={style.root}>
                        <Image source={require('../../../../assets/images/logo.png')} style={[style.Logo,{marginTop:40}]}/>
                        <Text style={style.wellcome}>Chào mừng bạn đến với TIVA !</Text>
                        <View style={[style.formGroup,{marginTop:20}]}>
                            <Text style={style.titleStep3}>Chọn ngành nghề của bạn</Text>
                            <TouchableOpacity onPress={() => {this.RBSheet.open();}}>
                                <View style={style.comboBox}>
                                    <Text>{this.state.career_name}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={style.formGroup,{marginHorizontal:10, marginTop:5}}>
                            <Text style={style.titleStep3}>Tên Công Ty:</Text>
                            <Textarea style={style.textArea}
                                onChangeText={(text) => this.setState({enterprise_name:text})}>                                
                            </Textarea>
                        </View>      
                        <View style={style.formGroup,{marginHorizontal:10, marginTop:5}}>
                            <Text style={style.titleStep3}>Địa chỉ:</Text>
                            <Textarea style={style.textArea}
                                onChangeText={(text) => this.setState({address:text})}>                                
                            </Textarea>
                        </View> 
                        <View style={style.formGroup,{marginHorizontal:10, marginTop:5}}>
                            <Text style={style.titleStep3}>Số điện thoại:</Text>
                            <Textarea style={style.textArea}
                                onChangeText={(text) => this.setState({phone:text})}>                                
                            </Textarea>
                        </View>               
                        <Button block style={style.btBlock}
                            onPress={() => this.Register()}>
                            <Text>HOÀN THÀNH ĐĂNG KÝ</Text> 
                        </Button>
                        <RBSheet
                            ref={ref => {
                                this.RBSheet = ref;
                            }}
                            height={300}
                            duration={250}                            
                            customStyles={{ 
                                container: Themes.rbSheetContainer,                               
                                }}>
                            <View style={{width:'100%'}}>
                                <View style={Themes.headerBottomSheet}>
                                    <TouchableOpacity onPress={() => {this.RBSheet.close()}}>
                                        <Ionicons name='ios-close' size={40} />
                                    </TouchableOpacity>
                                    <Text style={Themes.rbSheetTitleHeader}>Chọn ngành nghề</Text>
                                    <View></View>
                                </View>
                                <ScrollView style={{height:250}}>
                                    <View>
                                        <View style={Themes.divider}></View>                                   
                                        {this.renderSubject(this.state.bussiness)}
                                    </View>
                                </ScrollView>
                            </View>
                        </RBSheet>
                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>           
        )
    }
} 