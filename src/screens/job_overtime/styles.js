import {StyleSheet, Dimensions} from 'react-native';
const fullWidth = Dimensions.get('window').width;
const fullHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        marginTop:10,
        marginBottom: 10
    },
    name: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50
    },
    titleHeader: {
        color:'white',
        fontWeight:'bold'
    },
    image: {
        height: 40,
        width: 40,
        borderRadius:100,
        margin: 10,
        marginLeft: 15
    },
    button: {
        backgroundColor: '#802390',
        marginTop: 10,
        marginBottom: 10,
        marginHorizontal: 20,
        padding: 10,
        alignItems: 'center',
        borderRadius: 5
    },
    textButton: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold'
    },
    postNews: {
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingBottom:10,
        paddingTop: 10,
    },
    buttonNews: {
        borderColor: '#AAAAAA',
        borderWidth: 1,
        backgroundColor: 'white',
        borderRadius: 15,
        padding: 5,
        alignItems: 'center',
        flexDirection: 'row'

    },
    // icon: {
    //     height: 30,
    //     width: 30,
    //     marginHorizontal: 10
    // },
    modal: {
        height: fullHeight*0.35,
        borderRadius: 10,
        padding: 10,
        width: fullWidth*0.85
    },
})
export default styles;