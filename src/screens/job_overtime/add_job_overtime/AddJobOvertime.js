import React, { Component } from 'react';
import Themes from '../../../styles/Theme';
import styles from './styles';
import { View, Text, TextInput, TouchableOpacity, ScrollView, ActivityIndicator, Picker, AsyncStorage, Alert } from 'react-native';
import HomeAPI from '../../../api/HomeAPI';
import RBSheet from 'react-native-raw-bottom-sheet';
import Colors from '../../../styles/Color';
import {Ionicons} from '@expo/vector-icons';
import { CheckBox } from 'react-native-elements'
import {
    Container, Header, Body, Title, Item, Input, Tab, Tabs,
    Button, Content, Left, Right, Form, TabHeading
} from 'native-base';
import { NavigationEvents } from 'react-navigation';

const namePlace = [
    {
    name: "Tiêu đề công việc",
},
{
    name: "Vị trí ứng tuyển",
},
];
const dataSalary = [
    {
        province_id: 7,
        province_name: "Chọn mức lương"
    },
    {
        province_id: 7,
        province_name: "1,000,000 VND - 3,000,000VND"
    },
    {
        province_id: 8,
        province_name: "3,000,000 VND - 5,000,000VND"
    },
    {
        province_id: 9,
        province_name: "5,000,000 VND - 7,000,000VND"
    },
    {
        province_id: 10,
        province_name: "7,000,000 VND - 10,000,000VND"
    },
    {
        province_id: 11,
        province_name: "10,000,000 VND - 15,000,000VND"
    },
    {
        province_id: 12,
        province_name: "15,000,000 VND - 20,000,000VND"
    },
];
let a = '';
let p = '';
let n = '';
let m = '';
export default class AddJobOvertime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: '',
            job_title: '',
            careers: [],
            salary_id: '',
            // position: '',
            // description: '',
            addressChecked: [],
            message: '',
            token: '',
            content1: '',
            // description1: ''
        }
    }
    async componentDidMount() {
        const token = await AsyncStorage.getItem('token');
        const token1 =await JSON.parse(token)
        this.setState({token:token1})
        await HomeAPI.GetProvinces()
        .then(async(res) => {
            await res[1].provinces.map((el,i) => {
                el.checked = false;
            });
            this.setState({addressChecked:res[1].provinces});
        })
        .catch(err => {
            //this.setState({loading:false});
            Alert.alert('Lỗi: ' + err);
        })
        .done();         
    }

    postJob = () => {
        return fetch('https://tiva.vn/api/tao-viec-lam-them', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: this.state.token,
                job_group: 7,
                job_title: this.state.job_title,
                careers: [this.state.careers],
                salary_id: this.state.salary_id,
                content: this.state.content,
                // position: this.state.position,
                // description: this.state.description
            }),
            })
            .then(response => response.json())  // promise
            .then(json => {
                // console.log(json.data)
                // console.log('mmmmmmmm:',json.data==true)
                if(json.message == 'Thêm mới tin thành công') {
                    console.log(json.message)
                    a = '';
                    n = '';
                    this.setState({
                        content1: '',
                        job_title: '',
                        // position: '',
                        // description1: '',
                    })
                    Alert.alert(json.message)
                }
                if(json.message == 'Bạn không có quyền để thực hiện thao tác này.') {
                    alert('Bạn phải đăng kí để trở thành nhà tuyển dụng');
                    this.props.navigation.navigate('RegisterStep')
                }
                // console.log(
                //     'job_title:'+ this.state.job_title,
                //     'careers:'+ this.state.careers,
                //     'salary_id:'+ this.state.salary_id,
                //     'content:'+ this.state.content,
                //     'position:'+ this.state.position,
                //     'description:'+ this.state.description)
            })
    }
    // renderTextInput = (dataPlaceholder) => {
    //     return dataPlaceholder.map( (Element, index) =>{
    //             return(
    //                 <View key={index}>
    //                     <Text style={{marginBottom:5}}>{Element.name}</Text>
    //                     <TextInput style={styles.textInput} placeholder={Element.name}></TextInput>
    //                 </View>
    //         );}
    //     )
    // }    
    renderItemDropdownList = (itemDataSource) => {
        return itemDataSource.map((element, index) => {
            //hàm renderItemDropdownList cần return vì phải trả lại giá trị để import vào Picker bên dưới
            return (
                <Picker.Item key={index} label={element.province_name} value={element.province_id}/>
            );
        });
    }
    dropdownContent = async()=> {
        if(this.state.content1.search(a) == -1) {
            console.log(a)
            await this.setState({content1: a})
            alert('Bạn không được chỉnh sửa khi đã xuống dòng vui lòng reset lại để nhập lại')
            return;
        }
        let b = await this.state.content1;
        let c = await b.replace(a,'');
        if(c) {
        let d = await '<p>' + c + '</p>';
        // for( let i=1; i<= length.b; i++ ) {
        //     if(b.substring(0,i) =! a.substring(0,i)) {
                
        //     }
        // }
        // console.log(d)
        await this.setState({content: this.state.content + d})
        }
        a = await this.state.content1;
    } 
    // dropdownDescription = async () => {
    //     if(this.state.description1.search(n) == -1) {
    //         this.setState({description1: n})
    //         alert('Bạn không được chỉnh sửa khi đã xuống dòng vui lòng reset lại để nhập lại')
    //         return;
    //     }
    //     let b = await this.state.description1;
    //     let c = await b.replace(n,'')
    //     if(c) {
    //     let d = await '<p>' + c + '</p>';
    //     await this.setState({description: this.state.description + d})
    //     }
    //     n = await this.state.description1;
    // }
    render() {
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <NavigationEvents
                        onDidFocus={async() => {
                            a = '';
                            n = '';
                            }}
                    />
                <View style={Themes.header}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Ionicons name={'ios-arrow-back'} size={30} color={'white'}/>
                        </TouchableOpacity>
                        <Text style={styles.titleHeader}> ĐĂNG VIỆC LÀM THÊM</Text>
                        <View></View>
                </View>
                <ScrollView style={{flex:1}}>
                <View style={styles.content2}>
                    <Tab heading={"Địa điểm"}
                            activeTextStyle={styles.tabActiveTextStyle}
                            tabStyle={styles.tabStyle}
                            activeTabStyle={styles.tabActiveStyle}
                            style={styles.tabContent}>
                        <View>
                            <Form>
                                <Picker
                                        style={styles.picker}
                                        mode="dropdown"
                                    //headerBackButtonTextStyle={{ color: Colors.active }}
                                        selectedValue={this.state.careers}
                                        onValueChange={async (itemValue) => {
                                            await this.setState({careers: itemValue})
                                        }}>
                                    {this.renderItemDropdownList(this.state.addressChecked)}
                                </Picker>
                            </Form>
                        </View>
                    </Tab>
                    <Tab heading={"Mức lương"}
                            activeTextStyle={styles.tabActiveTextStyle}
                            tabStyle={styles.tabStyle}
                            activeTabStyle={styles.tabActiveStyle}
                            style={styles.tabContent}>
                        <View>
                            <Form>
                                <Picker
                                        style={styles.picker}
                                        mode="dropdown"
                                    //headerBackButtonTextStyle={{ color: Colors.active }}
                                    selectedValue={this.state.salary_id}
                                        onValueChange={async (itemValue) => {
                                            await this.setState({salary_id: itemValue})
                                        }}>
                                    {this.renderItemDropdownList(dataSalary)}
                                </Picker>
                            </Form>
                        </View>
                    </Tab>
                    <View>
                        <Text style={{marginBottom:5}}>Tiêu đề công việc</Text>
                        <TextInput value={this.state.job_title} onChangeText={(text)=> this.setState({job_title:text})} style={styles.textInput} placeholder={"Tiêu đề công việc"}></TextInput>
                    </View>
                    {/* <View>
                        <Text style={{marginBottom:5}}>Vị trí ứng tuyển</Text>
                        <TextInput value={this.state.position} onChangeText={(text)=> this.setState({position:text})} style={styles.textInput} placeholder={'Vị trí ứng tuyển'}></TextInput>
                    </View> */}
                    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <Text>Nội dung:</Text>
                        <TouchableOpacity onPress={()=>{
                            a = '';
                            this.setState({content1:''})
                            }}
                            style={{padding:5,backgroundColor:'#802390',borderRadius:5}}><Text style={{color:'white'}}>reset</Text></TouchableOpacity>
                    </View>
                    <TextInput style={styles.textarea} 
                        underlineColorAndroid="transparent"
                        placeholderTextColor="grey"
                        numberOfLines={4}
                        multiline={true}
                        value = {this.state.content1}
                        onChangeText={async(text)=> {
                            // console.log(text,'+',p)
                            // if(text.search(p) == -1 && text.length > p.length) {
                            //     this.setState({content1:p})
                            //     Alert.alert('Bạn không được quyền chỉnh sửa khi đã xuống dòng')
                            //     return;
                            // }
                            await this.setState({content1:text})
                            if(this.state.content1.search(a) == -1) {
                                this.setState({content1: a})
                                alert('Bạn không được chỉnh sửa khi đã xuống dòng hoặc kết thúc nhập, vui lòng reset lại để có thể chỉnh sửa')
                            }
                            // p = await text
                            }}
                        onEndEditing = {() =>{
                            if(this.state.content1.search(a) == -1) {
                            this.setState({content1: a})
                            alert('Bạn không được chỉnh sửa khi đã xuống dòng hoặc kết thúc nhập, vui lòng reset lại để có thể chỉnh sửa')
                            return;
                        }}}
                        onSubmitEditing={this.dropdownContent}
                    />
                    {/* <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <Text>Mô tả:</Text>
                        <TouchableOpacity onPress={()=>{
                            n='';
                            this.setState({description1:''})
                            }}
                            style={{padding:5,backgroundColor:'#802390',borderRadius:5}}><Text style={{color:'white'}}>reset</Text></TouchableOpacity>
                    </View> */}
                    {/* <TextInput style={styles.textarea} 
                        underlineColorAndroid="transparent"
                        placeholderTextColor="grey"
                        numberOfLines={4}
                        multiline={true}
                        value = {this.state.description1}
                        onChangeText={async(text)=> {
                            // if(text.search(m) == -1 && text.length > m.length) {
                            //     this.setState({description1:m})
                            //     Alert.alert('Bạn không được quyền chỉnh sửa khi đã xuống dòng')
                            //     return;
                            // }
                            await this.setState({description1:text})
                            if(this.state.description1.search(n) == -1) {
                                this.setState({description1: n})
                                alert('Bạn không được chỉnh sửa khi đã xuống dòng hoặc kết thúc nhập, vui lòng reset lại để có thể chỉnh sửa')
                            }
                            // m = await text
                            }}
                        onEndEditing = {() =>{
                            if(this.state.description1.search(n) == -1) {
                            this.setState({description1: n})
                            alert('Bạn không được chỉnh sửa khi đã xuống dòng hoặc kết thúc nhập, vui lòng reset lại để có thể chỉnh sửa')
                            return;
                        }}}
                        onSubmitEditing={this.dropdownDescription}
                    /> */}
                    <TouchableOpacity onPress={async()=> {
                        await this.dropdownContent();
                        // await this.dropdownDescription();
                        await this.postJob();
                    }} style={styles.button}>
                            <Text style={styles.textButton}>ĐĂNG BÀI</Text>
                    </TouchableOpacity>
                </View>
                </ScrollView>
            </View>
        );
    }
}