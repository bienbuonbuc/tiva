import { StyleSheet } from 'react-native';
import {Platform} from 'react-native';

const isIos = () => {
  return Platform.OS === 'ios' ? true : false;
}

const styles = StyleSheet.create({
    titleHeader: {
        color:'white',
        fontWeight:'bold'
    },
    textInput: {
        height: 35,
        paddingLeft: 15,
        borderRadius: 5,
        borderWidth: 0.7,
        borderColor: 'rgba(178, 188, 178, 1)',
        //borderColor: 'lightgrey',
        fontSize: 14,
        color: '#545353',
        backgroundColor: 'white',
        marginBottom: 10
        },
    content2: {
        paddingLeft : 15,
        paddingRight : 15,
        marginTop: 10
    },
    textarea: {
        backgroundColor : "white",
        textAlignVertical: "top",
        marginBottom: 10,
        marginTop: 10,
        padding :10,
        borderRadius : 3,
        borderWidth : 1,
        borderColor : "#a6a6a6",
    },
    button: {
        backgroundColor: '#802390',
        marginTop: 10,
        marginBottom: 10,
        padding: 10,
        alignItems: 'center',
        borderRadius: 5
    },
    textButton: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold'
    },
    tabActiveTextStyle: {
        color: !isIos() ? '#802390' : 'white',
      },
      tabActiveStyle: {
        backgroundColor: !isIos() ? '#802390' : '#9121a5',
      },
      tabStyle: {
        backgroundColor: !isIos() ? '#802390' : '#9121a5',
      },
      tabContent: {
        backgroundColor: 'white',
      },
      picker: {
        backgroundColor: '#F4F4F4',
        marginTop: 10,
        marginBottom:10,
        height : 50
      },
});
export default styles;