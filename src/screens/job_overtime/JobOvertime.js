import React, { Component } from 'react';
import { View, Text, FlatList, Platform, ActivityIndicator, Dimensions, Alert, AsyncStorage, Image, StatusBar } from 'react-native';
import styles from './styles';
import JobOvertimeItem from './JobOvertimeItem';
import BasicHeader from '../../components/BasicHeader';
import Themes from '../../styles/Theme';
import { TouchableOpacity } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationEvents } from 'react-navigation';
import Modal from 'react-native-modalbox';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

export default class JobOvertime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            next: '',
            isModalVisible: false,
            loading:true,
            refreshing: false,
            token: '',
            listIdRecruitment: [],
            listRecruitment: [],
            hidden: true,
            listCheck: [],
            check1: [],
            mac_ip: '',
            notification: '',
            pre:'',
            to: '',
            title: '',
            body: '',
            modalHiddenNotifi: false,
            employee: '',
            order_id: ''
        },
        console.disableYellowBox = true;
    }

    registerForPushNotificationsAsync = async() => {
        // if (Constants.isDevice) {
          // nếu chạy thiết bị ảo thì Constants.isDevice có giá trị false, máy thật thì true
          // console.log(!Constants.isDevice)
          
          const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        
        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (status !== 'granted') {
          // Android remote notification permissions are granted during the app
          // install, so this will only ask on iOS
          alert('No notification permissions!');
          return;
        }
      
        // // Stop here if the user did not grant permissions
        // if (finalStatus !== 'granted') {
        //   return;
        // }
      
        // Get the token that uniquely identifies this device
        let mac_ip = await Notifications.getExpoPushTokenAsync();
        await this.setState({mac_ip})
        // } else {
        //   alert('phai su dung may that de chay');
        // }
        if (Platform.OS === 'android') {
            Notifications.createChannelAndroidAsync('default', {
              name: 'default',
              sound: true,
              priority: 'max',
              vibrate: [0, 250, 250, 250],
            });
          }
      }
    handleNotification = (notification) => {
        this.setState({notification: notification});
        console.log(this.state.notification.data)
        if(this.state.notification.origin == 'selected') {
            this.props.navigation.navigate('JobOvertime')
            let employee =  this.state.notification.data.employee;
            let order_id = this.state.notification.data.order_id;
            this.setState({notification:'', modalHiddenNotifi: !this.state.modalHiddenNotifi, employee, order_id})
        }
    };
    getMacIp = () => {
        // console.log(job_id,this.state.token)
        return fetch('http://tiva.vn/api/check-mac-ip?mac_ip=' + this.state.mac_ip + '&token='+this.state.token)
        .then((response) => response.json())
        .then((responseJson) => {
                return responseJson
        })
        .catch((e) => {
            console.log('loi lấy công việc')
        })
    }
    postNotification = async() =>{
        let res = await fetch('https://exp.host/--/api/v2/push/send',{
          method:'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body:JSON.stringify({
            to: this.state.to ,
            sound: 'default',
            title: this.state.title,
            body: this.state.body,
            data: { employee: this.state.employee, order_id: this.state.order_id },
            _displayInForeground: true,
          })
        })
        let pre = await res.json();
        this.setState({pre:pre.data})
        return pre;
    }
    stateTransitions = async() =>{
        let res = await fetch('https://tiva.vn/api/cap-nhat-trang-thai-uv',{
          method:'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body:JSON.stringify({
            token: this.state.token,
            status: 1,
            order_id: this.state.order_id
          })
        })
        let pre = await res.json();
        if(pre.message == 'Cập nhật trạng thái ứng viên sang phỏng vấn thành công') {
            this.props.navigation.navigate('DetailCandidates',{info:{employee_id:this.state.employee}})
        }
        if(pre.message == 'Tài khoản của bạn hiện không đủ. Vui lòng nạp thêm tiền') {
            alert('Tài khoản của bạn hiện không đủ. Vui lòng nạp thêm tiền')
        }
        if(pre.message == 'Lỗi xảy ra trong quá trình cập nhật.') {
            alert('Ứng viên hoặc công việc này đã bị xóa')
        }
        return pre;
    }

    getJobOvertime() {
        // alert(this.state.token)
        return fetch('https://tiva.vn/api/lay-cong-viec-theo-group?token=' + this.state.token)
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({data:responseJson.data.data,next:responseJson.data,refreshing:false, loading:false})
            return responseJson
        })
        .catch((e) => {
            console.log('loi lấy công việc')
        })
    }
    nextPage() {
        return fetch(this.state.next.next_page_url + '&token=' + this.state.token)
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson)
            {
                let result = responseJson.data.data;
                let preArr = this.state.data;
                let endresult = preArr.concat(result);
                this.setState({data:endresult,next:responseJson.data, loading:false})
                return responseJson
            }
        })
        .catch((e) => {
            console.log('lỗi chuyển trang')
        })
    }
    Refresh = async () => {
        await this.setState({refreshing: true})
        this.getJobOvertime();
        
        // kéo xuống cập nhật dữ liệu mới
    }
    async Recruitment(job_id) {
        // console.log(job_id,this.state.token)
        return fetch('https://tiva.vn/api/ung-tuyen-nhanh?job_id=' + job_id + '&token='+this.state.token)
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.message == 'Bạn đã ứng tuyển công việc thành công.'){
                console.log('biennnnnnnnnnnnnn', responseJson.to)
                this.setState({isModalVisible: !this.state.isModalVisible, to:responseJson.to, order_id: responseJson.order_id,
                    title: responseJson.title, body: responseJson.body, employee:responseJson.employee });
                this.postNotification();
            }
                return responseJson
        })
        .catch((e) => {
            console.log('loi lấy công việc')
        })
    }
    // postToken = () => {
    //     return fetch('https://tiva.vn/api/viec-lam-them-da-ung-tuyen', {
    //         method: 'POST',
    //         headers: {
    //             Accept: 'application/json',
    //             'Content-Type': 'application/json',
    //         },
    //         body: JSON.stringify({
    //             token: this.state.token
    //         }),
    //         })
    //         .then(response => response.json())  // promise
    //         .then(json => {
    //             this.setState({listRecruitment: json.jobs.data})
    //         })
    // }
    // pustArray1 = async () => {
    //     let listRecruitment =await this.state.listRecruitment;
    //     await listRecruitment.map((element,index) => {
    //         this.state.listIdRecruitment.push(element.job_id)
    //     })
    // }
    // pustArray2 = async () => {
    //     let array = await this.state.listCheck;
    //     await this.state.data.map((element,index) => {
    //         array.push(element.job_id)
    //     })
    //     await this.setState({listCheck: array})
    // }
    // check = () =>{
    //     let listIdRecruitment = this.state.listIdRecruitment;
    //     let listCheck = this.state.listCheck;
    //     console.log('hhhhhhh',listCheck)
    //     console.log('gggggggggggg',listIdRecruitment)
    //     listCheck.map((element)=> {
    //         for(let i=0;i<listIdRecruitment.length; i++) {
    //             // console.log(element + ',' + listIdRecruitment[i])
    //             if(element==listIdRecruitment[i]) {
    //                 this.state.check1.push(true)
    //                 return;
    //             }
    //     }
    //     this.state.check1.push(false);
    // }
    //     )
    //     console.log(this.state.check1)
    // }
    checkToken = async () => {
        const token = JSON.parse(await AsyncStorage.getItem('token'));
        return fetch('https://tiva.vn/api/check-user?token=' + token)
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.message == 'Token đã hết hạn sử dụng.') {
                this.props.navigation.navigate('Login')
            }
            return responseJson
        })
        .catch((e) => {
            console.log('loi lấy công việc')
        })
    }
    async componentDidMount() {
        const token = JSON.parse(await AsyncStorage.getItem('token'));
        await this.setState({token:token});
        // await alert('tai sao' + token1)
        await this.getJobOvertime();
        await this.registerForPushNotificationsAsync();
        this.notificationSubscription = Notifications.addListener(this.handleNotification);
        await this.getMacIp();
        this.checkToken();
    }

    // async componentWillMount() {
    //     await this.getJobOvertime()
    //     const token = await AsyncStorage.getItem('token');
    //     const token1 =await JSON.parse(token)
    //     this.setState({token:token1,loading:false})
    //     await this.postToken();
    //     await this.pustArray1();
    //     await this.pustArray2();
    //     await this.check();
    //     // await this.replaceConten();
    // }
    render() {
        return(  
                <View style={{backgroundColor:'#B9BABF', flex:1}}>
                <StatusBar></StatusBar>
                {/* tạo ra thanh trạng thái của app tất cả màn hình trong app sẽ cùng hiểu khi đã chạy vào màn hình gặp StatusBar này rồi */}
                    <NavigationEvents
                            onDidFocus={async() => {
                                this.Refresh();
                                }}
                        />
                    <View style={Themes.header}>
                        {/* <TouchableOpacity onPress={()=> this.props.navigation.navigate('AddJobOvertime')} style={{borderColor:'white', borderWidth:1, borderRadius:5, padding:5}}>
                            <Text style={{color:'white', fontWeight:'bold'}}>ĐĂNG TIN</Text>
                        </TouchableOpacity> */}
                        <View style={{width:50}}></View>
                        <Text style={styles.titleHeader}>VIỆC LÀM THÊM</Text>
                        <View style={{width:50}}></View>
                    </View>
                    <View style={styles.postNews}>
                        {/* <Image resizeMode={'contain'} style={styles.icon} source={require('../../../assets/icon.png')} /> */}
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('AddJobOvertime')} style={styles.buttonNews}>
                            <MaterialCommunityIcons style={{marginLeft:5, marginRight: 30}} name={"newspaper"} size={25} color={'#AAAAAA'} />
                            <Text style={{color:'#333333'}}>Đăng tin tuyển dụng</Text>
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        data={this.state.data}
                        renderItem={({item, index }) => {
                            if(index<13) {
                            return(
                            <JobOvertimeItem
                            hidden={item.check}
                            
                            image={item.image}
                            enterprise_name={item.enterprise_name}
                            content={item.content}
                            address={item.address}
                            title={item.title}
                            bien = {this.state.bien}
                            Recruitment={() => {
                                this.Recruitment(item.job_id)
                                }}
                            />
                            );
                        }}
                        }
                        onEndReached = {async() => {
                            await this.setState({loading: true}) // phải thêm loading: true ở đây, vì khi kéo xuống loading sẽ luôn false, không nhảy thành true được nữa
                            await this.nextPage()
                            }}
                        onEndReachedThreshold={0.01}
                        refreshing = {this.state.refreshing}
                        onRefresh ={this.Refresh}
                        keyExtractor = {(item,index) => index.toString()}
                    />
                    <Modal style={styles.modal}
                        isOpen={this.state.isModalVisible}
                        position={'center'}
                        backdropOpacity={0.5}
                    >
                        <Text style={{fontWeight:'bold', fontSize:16}}>Bạn Đã Ứng Tuyển Công Việc Thành Công</Text>
                        <View style={{marginTop:5}}>
                            <Text>Một thông báo đã được gửi đến nhà tuyển dụng</Text>
                            <Text>Xin vui lòng chờ đợi phản hồi.</Text>
                            <Text>Tiva rất vui khi được bạn tin dùng. Chúc bạn có một ngày mới vui vẻ</Text>
                            <Text>Chân thành cảm ơn bạn!</Text>
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'space-around'}}>
                            <TouchableOpacity onPress={() => this.setState({isModalVisible:!this.state.isModalVisible})} 
                                style={{backgroundColor: 'red', marginTop:20, padding:5, paddingHorizontal:20 ,borderRadius:10}}>
                                <Text style={{color:'white'}}> Đóng Tab </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({isModalVisible:!this.state.isModalVisible})} 
                                style={{backgroundColor: 'blue', marginTop:20, padding:5, paddingHorizontal:20 ,borderRadius:10}}>
                                <Text style={{color:'white'}}>   Tiếp tục   </Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                    <Modal style={styles.modal}
                        isOpen={this.state.modalHiddenNotifi}
                        position={'center'}
                        backdropOpacity={0.5}
                    >
                        <Text style={{fontWeight:'bold', fontSize:16}}>Ứng Viên Muốn Ứng Tuyển Công Việc</Text>
                        <View style={{marginTop:5}}>
                            <Text>Bạn có muốn xem chi tiết ứng viên và hẹn lịch phỏng vấn. Tài khoản sẽ bị trừ 10k</Text>
                            <Text>Xin vui lòng phản hồi thông báo</Text>
                            <Text>Tiva rất vui khi được bạn tin dùng. Chúc bạn có một ngày mới vui vẻ</Text>
                            <Text>Chân thành cảm ơn bạn!</Text>
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'space-around'}}>
                            <TouchableOpacity onPress={() => this.setState({modalHiddenNotifi:!this.state.modalHiddenNotifi})} 
                                style={{backgroundColor: 'red', marginTop:20, padding:5, paddingHorizontal:20 ,borderRadius:10}}>
                                <Text style={{color:'white'}}> Để sau </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress = {() => {
                                this.stateTransitions();
                                this.setState({modalHiddenNotifi:false})
                                }}
                                style={{backgroundColor: 'blue', marginTop:20, padding:5, paddingHorizontal:20 ,borderRadius:10}}>
                                <Text style={{color:'white'}}>   Xác nhận   </Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                    {this.state.loading && 
                        <View style={Themes.activityIndicatorView}>
                            <ActivityIndicator 
                                size = {Platform.OS === 'ios'?0:'large'}
                                color = "blue"
                            />
                        </View>
                    }
                </View>
        );
    }
}