import {StyleSheet, Platform} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';

const style = StyleSheet.create({
    datePicker: {
        borderBottomWidth:1,
        paddingVertical:5,
        justifyContent:'center',
        borderColor:Colors.note,
    },
    picker:{
        borderBottomWidth:1,
        paddingVertical:3,
        justifyContent:'center',
        borderColor:Colors.note,
    }
});
export default style;