import React from 'react';
import {View,Item,Input,DatePicker,Picker,Textarea, Button} from 'native-base';
import {Text,Image, TouchableOpacity, AsyncStorage, Alert} from 'react-native';
import BasicHeader from '../../components/BasicHeader';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import style from './style';
import {Ionicons} from '@expo/vector-icons';
import Colors from '../../styles/Color';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as FileSystem from 'expo-file-system';
import Validator from '../../utils/Validator';
import Formater from '../../utils/Formater';
import PersonalInfoAPI from '../../api/PersonalInfoAPI';
import Loader from '../../components/Loader';
import { StackActions, NavigationActions } from 'react-navigation';

export default class PersonalInfo extends React.Component{

    constructor(props) {
        super(props);
        const personal = this.props.navigation.getParam('personal');
        this.state = { 
            chosenDate: new Date(), 
            selected: undefined,
            name: personal.name,
            phoneNumber: personal.phone,
            address: '',
            experience: '',
            avatarUri: personal.image ? personal.image : '',
            isPickImage: false,
            isPickDate: false,
            loading: false,
        };
        this.setDate = this.setDate.bind(this);
    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate, isPickDate:true });
    }

    onValueChange(value) {
        this.setState({
            selected: value
        });
    }

    TakePhoto = async () => {                 
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
        });

        if (!result.cancelled) {
            this.setState({avatarUri:result.uri,isPickImage:true});           
        }
    }

    AddPersonalInfo = async () =>  {       
        this.setState({loading:true});

        const token = await AsyncStorage.getItem('token');
        
        let gender = "";
        if (this.state.selected == "male") gender = "1" // nam
        else if (this.state.selected == "famale") gender == "2" // nu
        else gender = "0" // undefined
        
        const personalInfo = {
            token: JSON.parse(token),
            employee_name:this.state.name,
            phone:this.state.phoneNumber,
            address:this.state.address,
            experience: this.state.experience,
            gender:gender,
            birthday:Formater.DateUTCFormat(this.state.chosenDate.toUTCString(),'yyyy-mm-dd','-'),
            image:''
        }

        if (this.state.isPickImage == true && this.state.avatarUri != '' ) {
            personalInfo.image = await FileSystem.readAsStringAsync(this.state.avatarUri,{encoding:'base64'});
            personalInfo.image = 'data:image/png;base64,' + personalInfo.image;
            this.setState({isPickImage:false});
        }

        if (!Validator.Required([personalInfo.employee_name,personalInfo.phone,personalInfo.address,personalInfo.experience])) {           
            Alert.alert('Các trường trong form không được để trống!');
            return;
        }

        if (!Validator.IsPhoneNumberVN(personalInfo.phone)) {
            Alert.alert('Số điện thoại sai định dạng!');
            return;
        }

        if (this.state.selected == undefined) {
            Alert.alert('Bạn phải chọn giới tính!');
            return;
        }

        if (!this.state.isPickDate) {
            Alert.alert('Bạn phải chọn ngày sinh!');
            return;
        }

        console.log(personalInfo);
        await PersonalInfoAPI.EditPersonalInfo(personalInfo)
        .then((res) => {
            this.setState({loading:false});
            if (res[0] == 200) {
                // const resetAction = StackActions.reset({
                //     index: 0,
                //     actions: [NavigationActions.navigate({ routeName: 'Profile' })],
                // });
                // this.props.navigation.dispatch(resetAction);
                this.props.navigation.goBack();
            }
            else Alert.alert('Có lỗi xảy ra!');
        })
        .catch((er) => {
            this.setState({loading:false});
            Alert.alert('Lỗi: ' + er);
        })
        .done();
    }

    render() {
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <BasicHeader title='THÔNG TIN CÁ NHÂN' navigation={this.props.navigation}/>
                <Loader loading={this.state.loading}/>
                <KeyboardAwareScrollView>
                    <View style={{paddingHorizontal:15}}>
                        <Item inlineLabel last>                          
                            <Input placeholder="Họ và tên"
                                value={this.state.name}
                                onChangeText={(text) => this.setState({name:text})}
                            />
                        </Item>                                                                                                
                        <View style={style.datePicker}>
                            <DatePicker                       
                                borderRadius={10}                                    
                                locale={"vn"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Chọn ngày tháng năm"
                                textStyle={{ color: "green" }}
                                placeHolderTextStyle={{ color: "#d3d3d3",paddingLeft:5 }}
                                onDateChange={this.setDate}
                                disabled={false}
                            /> 
                        </View>                                                                                                                              
                        <View style={style.picker}>
                            <Picker                                
                                placeholderStyle={{paddingLeft:5}}                                        
                                mode="dropdown"
                                iosHeader="Chọn giới tính"
                                placeholder="Chọn giới tính"                                                                        
                                iosIcon={<Ionicons name='ios-arrow-down' size={20} color={Colors.note}/>}
                                style={{ width: '100%' }}                                                                        
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}
                                >
                                <Picker.Item label="Nam" value="male"/>
                                <Picker.Item label="Nữ" value="famale" />                                  
                            </Picker>
                        </View>       
                        <Item inlineLabel last>                          
                            <Input placeholder="Số điện thoại"
                                value={this.state.phoneNumber}
                                onChangeText={(text) => this.setState({phoneNumber:text})}
                            />
                        </Item>          
                        <Item inlineLabel last>                          
                            <Input placeholder="Địa chỉ"
                                value={this.state.address}
                                onChangeText={(text) => this.setState({address:text})}
                            />
                        </Item>                       
                        <Textarea placeholder="Quá trình làm việc hoặc trình độ" 
                            style={{height:70, borderBottomWidth:1, borderColor: Colors.note, fontSize:17, paddingLeft:5, marginTop:10 }}
                            value={this.state.experience}
                            onChangeText={(text) => this.setState({experience:text})}
                        />  
                        <View style={{width:'40%',}}>
                            <Text style={{marginTop:10}}>Ảnh đại diện</Text>
                            {
                                this.state.avatarUri === ''
                                ? (
                                    <TouchableOpacity onPress={this.TakePhoto}>
                                        <Image source={require('../../../assets/images/Group19.png')} style={{marginVertical:10,width:70,height:70,resizeMode:'contain'}}/>
                                    </TouchableOpacity>
                                )
                                : (
                                    <TouchableOpacity onPress={this.TakePhoto}>
                                        <Image source={{uri: this.state.avatarUri}} style={{marginVertical:10,width:70,height:70,resizeMode:'contain'}}/>
                                    </TouchableOpacity>
                                )
                            }
                            <Button 
                                onPress={() => this.AddPersonalInfo()}
                                style={{backgroundColor:Colors.active,paddingHorizontal:15,marginBottom:15}}>
                                <Text style={{color:'white'}}>Cập nhập</Text>
                            </Button>
                        </View>
                    </View>
                </KeyboardAwareScrollView>     
            </View>
        );
    }
}