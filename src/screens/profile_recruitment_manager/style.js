import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
const style = StyleSheet.create({
    profileIconContainer: {
        width:'100%',
        paddingHorizontal:15
    },
    subContainer: {
        width:'100%',
        flexDirection:'row'
    },   
});
export default style;