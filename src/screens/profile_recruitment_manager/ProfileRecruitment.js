import React from "react";
import { View, Tabs, Tab, Button } from "native-base";
import {
  Text,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Alert,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import BasicHeader from "../../components/BasicHeader";
import Themes from "../../styles/Theme";
import Colors from "../../styles/Color";
import RBSheet from "react-native-raw-bottom-sheet";
import { Ionicons } from "@expo/vector-icons";
import { CheckBox } from "react-native-elements";
import style from "./style";
import Loader from "../../components/Loader";
import ProfileRecruitmentAPI from "../../api/ProfileRecruitmentAPI";
import NotifyEmptyData from "../../components/NotifyEmptyData";

const searchs = [
  // {
  //     Id: 1,
  //     Name: 'Được yêu thích'
  // },
  {
    Id: 2,
    Name: "Trạng thái"
  }
  // {
  //     Id: 3,
  //     Name: 'Công việc'
  // },
];
const status = [
  {
    status_id: 0,
    Name: "Chưa xác định"
  },
  {
    status_id: 1,
    Name: "Đã gửi CV"
  },
  {
    status_id: 2,
    Name: "Thất bại"
  },
  {
    status_id: 3,
    Name: "Đã phỏng vấn"
  },
  {
    status_id: 4,
    Name: "Thành công"
  },
  {
    status_id: 5,
    Name: "Đã đi làm"
  }
];
const posted = [
  {
    Id: 1,
    Name: "Tuyển dụng thực tập sinh thực hành thành thạo với Bootstrap",
    Dateline: "10 ngày",
    CountApplication: 40
  },
  {
    Id: 2,
    Name: "Tuyển dụng thực tập sinh thực hành thành thạo với Bootstrap",
    Dateline: "10 ngày",
    CountApplication: 40
  }
];
const expiredPost = [
  {
    Id: 1,
    Name: "Tuyển dụng thực tập sinh thực hành thành thạo với Bootstrap",
    Dateline: "10 ngày",
    CountApplication: 40
  },
  {
    Id: 1,
    Name: "Tuyển dụng thực tập sinh thực hành thành thạo với Bootstrap",
    Dateline: "10 ngày",
    CountApplication: 40
  },
  {
    Id: 1,
    Name: "Tuyển dụng thực tập sinh thực hành thành thạo với Bootstrap",
    Dateline: "10 ngày",
    CountApplication: 40
  }
];
export default class ProfileRecruitment extends React.Component {
  constructor(props) {
    super(props);
    const jobId = this.props.navigation.getParam("jobId");
    this.state = {
      searchType: 1,
      statusChecked: [],
      //jobsChecked: [],
      recruitmentApplied: [],
      loading: false,
      posted: [],
      expiredPost: [],
      filterCondition: {
        token: "",
        job_id: jobId + "",
        status_id: []
      }
    };
  }

  async componentDidMount() {
    this.setState({ loading: true });

    await this.GetAllRecruitmentApplied(this.state.filterCondition);

    await this.LoadTab2();

    // TODO: init subject checked add property check to object (customize object)
    status.map((el, i) => {
      el.checked = false;
    });
    this.setState({ statusChecked: status });

    //TODO: init jobs checked
    // jobs.map((el,i) => {
    //     el.checked = false;
    // });
    // this.setState({jobsChecked: jobs});
  }

  LoadTab2 = async () => {
    const token = await AsyncStorage.getItem("token");

    await ProfileRecruitmentAPI.GetAllPosted({ token: JSON.parse(token) })
      .then(res => {
        //console.log(res);
        this.setState({ loading: false });
        if (res[0] == 200) {
          this.setState({ posted: res[1].data.data });
        } else this.setState({ posted: res[1].data });
      })
      .catch(er => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + er);
      })
      .done();

    await ProfileRecruitmentAPI.GetAllExpiredPost(JSON.parse(token))
      .then(res => {
        //console.log(res);
        this.setState({ loading: false });
        if (res[0] == 200) {
          this.setState({ expiredPost: res[1].data.data });
        } else {
          this.setState({ expiredPost: res[1].data });
        }
      })
      .catch(er => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + er);
      })
      .done();
  };

  GetStatusName(id) {
    return status.map((el, i) => {
      if (el.status_id == id) return el.Name;
    });
  }

  async GetAllRecruitmentApplied(filter) {
    const token = await AsyncStorage.getItem("token");
    this.state.filterCondition.token = JSON.parse(token);
    console.log(filter);
    await ProfileRecruitmentAPI.GetAllRecruitmentApplied(filter)
      .then(res => {
        this.setState({
          loading: false,
          //last_page: res[1].data.last_page,
          recruitmentApplied: res[1].data.data
        });
      })
      .catch(err => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + err);
      })
      .done();
  }

  FilterByStatus = () => {
    //this.setState({jobs:[]});
    let statusIds = [];
    status.map((el, i) => {
      if (el.checked === true) {
        statusIds.push(el.status_id);
      }
    });
    const filterConditionNew = this.state.filterCondition;
    filterConditionNew.status_id = statusIds;
    this.setState({ loading: true, filterCondition: filterConditionNew });
    this.RBSheet.close();
    //console.log('FilterByCareer',this.state.filterCondition);
    this.GetAllRecruitmentApplied(this.state.filterCondition);
  };

  searchPress = item => {
    if (item.Id != 1) {
      this.setState({ searchType: item.Id });
      this.RBSheet.open();
    }
  };

  renderItemSearch = searchData => {
    return searchData.map((el, i) => {
      return (
        <TouchableOpacity
          key={i}
          onPress={() => {
            this.searchPress(el);
          }}
        >
          <View style={Themes.searchItem}>
            <Text>{el.Name}</Text>
          </View>
        </TouchableOpacity>
      );
    });
  };

  //#region status
  checkBoxCheckStatus = index => {
    let tempObjectChecked = this.state.statusChecked;
    tempObjectChecked.map((el, i) => {
      if (i == index) {
        el.checked = !el.checked;
      }
    });
    this.setState({ statusChecked: tempObjectChecked });
  };

  renderStatus = statusData => {
    // TODO: use customize object alternative origin object to use property checked for checkbox
    return statusData.map((el, i) => {
      return (
        <View key={i}>
          <View style={Themes.rbSheetItemContainer}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }}
            >
              <Text>{el.Name}</Text>
              <CheckBox
                checked={el.checked}
                onPress={() => this.checkBoxCheckStatus(i)}
              />
            </View>
            <View style={Themes.divider}></View>
          </View>
        </View>
      );
    });
  };

  reCheckAllStatus = () => {
    let tempObjectChecked = this.state.statusChecked;
    tempObjectChecked.map((el, i) => {
      el.checked = false;
    });
    this.setState({ statusChecked: tempObjectChecked });
  };
  //#endregion

  //#region job
  // checkBoxCheckJob = (index) => {
  //     let tempObjectChecked = this.state.jobsChecked;
  //     tempObjectChecked.map((el,i) => {
  //         if (i == index) {
  //             el.checked = !el.checked;
  //         }
  //     });
  //     this.setState({jobsChecked:tempObjectChecked});
  // }

  // renderJob = (jobData) => {
  //     // TODO: use customize object alternative origin object to use property checked for checkbox
  //     return jobData.map((el,i) => {
  //         return (
  //             <View key={i}>
  //                 <View style={Themes.rbSheetItemContainer}>
  //                     <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center'}}>
  //                         <View style={{width:'85%'}}>
  //                             <Text>{el.Name}</Text>
  //                             <View style={{flexDirection:'row'}}>
  //                                 <Text>{el.Time} - </Text>
  //                                 <Text>{el.Salary}</Text>
  //                             </View>
  //                             <Text>{el.Address}</Text>
  //                         </View>
  //                         <CheckBox
  //                             style={{width:'15%'}}
  //                             checked={el.checked}
  //                             onPress={() => this.checkBoxCheckJob(i)}
  //                         />
  //                     </View>
  //                     <View style={[Themes.divider,{marginVertical:5}]}></View>
  //                 </View>
  //             </View>
  //         );
  //     });
  // }

  // reCheckAllJob = () => {
  //     let tempObjectChecked = this.state.jobsChecked;
  //     tempObjectChecked.map((el,i) => {
  //         el.checked = false;
  //     });
  //     this.setState({jobsChecked:tempObjectChecked});
  // }
  //#endregion

  renderByType = type => {
    if (type === 1) {
      return <View style={{ width: "100%" }}></View>;
    } else if (type === 2) {
      return (
        <View style={{ width: "100%" }}>
          <View style={Themes.headerBottomSheet}>
            <TouchableOpacity
              onPress={() => {
                this.RBSheet.close();
              }}
            >
              <Ionicons name="ios-close" size={40} />
            </TouchableOpacity>
            <Text style={Themes.rbSheetTitleHeader}>Ngành nghề</Text>
            <TouchableOpacity onPress={() => this.reCheckAllStatus()}>
              <Text style={Themes.rbSheetBtReCheck}>Đặt lại</Text>
            </TouchableOpacity>
          </View>
          <ScrollView style={{ height: 300 }}>
            <View>
              <View style={Themes.divider}></View>
              {this.renderStatus(this.state.statusChecked)}
            </View>
          </ScrollView>
          <View style={{ flexDirection: "row", width: "100%", padding: 10 }}>
            <Button
              style={{
                flex: 1,
                backgroundColor: "#E4E5EA",
                marginBottom: 5,
                marginRight: 5
              }}
              block
              onPress={() => this.FilterByStatus()}
            >
              <Text style={{ color: "black" }}>Áp dụng</Text>
            </Button>
            <Button
              style={{
                flex: 1,
                backgroundColor: "#E4E5EA",
                marginBottom: 5,
                marginLeft: 5
              }}
              block
              onPress={() => this.RBSheet.close()}
            >
              <Text style={{ color: "black" }}>Hủy</Text>
            </Button>
          </View>
        </View>
      );
    } else {
      // return (
      //     <View style={{width:'100%'}}>
      //         <View style={Themes.headerBottomSheet}>
      //             <TouchableOpacity onPress={() => {this.RBSheet.close()}}>
      //                 <Ionicons name='ios-close' size={40} />
      //             </TouchableOpacity>
      //             <Text style={Themes.rbSheetTitleHeader}>Loại công việc</Text>
      //             <TouchableOpacity onPress={() => this.reCheckAllJob() }>
      //                 <Text style={Themes.rbSheetBtReCheck}>Đặt lại</Text>
      //             </TouchableOpacity>
      //         </View>
      //         <ScrollView style={{height:300}}>
      //             <View>
      //                 <View style={Themes.divider}></View>
      //                 {this.renderJob(this.state.jobsChecked)}
      //             </View>
      //         </ScrollView>
      //         <View style={{flexDirection:'row', width:'100%', padding:10}}>
      //             <Button style={{flex:1, backgroundColor: '#E4E5EA', marginBottom:5, marginRight:5}} block>
      //                 <Text style={{color:'black'}}>Áp dụng</Text>
      //             </Button>
      //             <Button style={{flex:1, backgroundColor: '#E4E5EA', marginBottom:5, marginLeft:5}} block
      //                 onPress={() => this.RBSheet.close()}>
      //                 <Text style={{color:'black'}}>Hủy</Text>
      //             </Button>
      //         </View>
      //     </View>
      // )
    }
  };

  renderProfileItem = item => {
    return (
      <View style={style.profileIconContainer}>
        <View style={style.subContainer}>
          <TouchableOpacity
            style={{ width: "90%" }}
            onPress={() =>
              this.props.navigation.navigate("Application", {
                info: { employee_id: item.employee_id, job_id: item.job_id }
              })
            }
          >
            <View>
              <Text style={{ textTransform: "uppercase" }}>{item.title}</Text>
              <Text style={{ fontWeight: "bold", textTransform: "uppercase" }}>
                {item.employee_name}
              </Text>
              {this.state.birthday ? (
                <Text>
                  {item.birthday} - {item.address}
                </Text>
              ) : (
                <Text>{item.address}</Text>
              )}
              <Text>{item.experience}</Text>
              {item.literacy ? <Text>{item.literacy}</Text> : <View></View>}
              <View style={Themes.statusContainer}>
                <Text style={{ color: "white" }}>
                  {this.GetStatusName(item.status)}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={{ width: "10%" }}>
            <TouchableOpacity>
              <Ionicons name="ios-star-outline" size={30} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={[Themes.divider, { marginVertical: 10 }]}></View>
      </View>
    );
  };

  IncrementDateline = async item => {
    this.setState({ loading: true });
    const token = await AsyncStorage.getItem("token");
    const condition = {
      token: JSON.parse(token),
      job_id: item.job_id
    };
    await ProfileRecruitmentAPI.IncrementDateline(condition)
      .then(res => {
        this.setState({ loading: false });
        if (res[0] == 200) {
          Alert.alert("Thời hạn công việc đã được tăng");
          this.LoadTab2();
        } else Alert.alert("Có lỗi xảy ra!");
      })
      .catch(er => {
        this.setState({ loading: false });
        Alert.alert("Lỗi: " + er);
      })
      .done();
  };

  renderPosted = item => {
    return (
      <View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View style={{ width: "70%" }}>
            <View>
              <Text style={{ fontWeight: "bold" }}>{item.title}</Text>
              <Text style={{ color: Colors.money }}>
                Hết hạn sau: {item.deadline}
              </Text>
              <Text style={{ color: "#008700" }}>
                {item.number_recruited} hồ sơ ứng tuyển
              </Text>
            </View>
          </View>
          <View style={{ width: "30%", alignItems: "center" }}>
            <Button
              onPress={() => this.IncrementDateline(item)}
              style={{
                backgroundColor: "#008700",
                paddingHorizontal: 10,
                alignSelf: "center"
              }}
              small
            >
              <Text style={{ color: "white" }}>Tiếp tục</Text>
            </Button>
          </View>
        </View>
        <View style={[Themes.divider, { marginVertical: 5 }]}></View>
      </View>
    );
  };

  renderExpired = item => {
    return (
      <View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View style={{ width: "70%" }}>
            <View>
              <Text style={{ fontWeight: "bold" }}>{item.Name}</Text>
              <Text style={{ fontStyle: "italic" }}>Đã hủy đăng ký</Text>
              <Text style={{ color: Colors.money }}>
                Hết hạn sau: {item.Dateline}
              </Text>
              <Text style={{ color: "#008700" }}>
                {item.CountApplication} hồ sơ ứng tuyển
              </Text>
            </View>
          </View>
          <View style={{ width: "30%", alignItems: "center" }}>
            <Button
              onPress={() => this.IncrementDateline(item)}
              style={{
                backgroundColor: "#008700",
                paddingHorizontal: 10,
                alignSelf: "center"
              }}
              small
            >
              <Text style={{ color: "white" }}>Tiếp tục</Text>
            </Button>
          </View>
        </View>
        <View style={[Themes.divider, { marginVertical: 5 }]}></View>
      </View>
    );
  };

  render() {
    let { searchType } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <BasicHeader
          title="QUẢN LÝ TUYỂN DỤNG"
          navigation={this.props.navigation}
        />
        <View
          style={[
            Themes.activityIndicatorView,
            { zIndex: 200000, position: "absolute" }
          ]}
        >
          <ActivityIndicator
            animating={this.state.loading}
            size={"large"}
            color={Colors.active}
          />
        </View>
        <Tabs tabBarUnderlineStyle={Themes.tabsUnderlineColor}>
          <Tab
            heading={"HỒ SƠ ỨNG TUYỂN"}
            activeTextStyle={Themes.tabActiveTextStyle}
            tabStyle={Themes.tabStyle}
            textStyle={{ color: Colors.active }}
            activeTabStyle={Themes.tabActiveStyle}
            style={Themes.tabContent}
          >
            <ScrollView>
              {/* <Loader loading={this.state.loading}/> */}
              <View>
                <View style={Themes.dividerLarge}></View>
                <View>
                  <View style={Themes.searchContainer}>
                    {this.renderItemSearch(searchs)}
                  </View>
                </View>
                <View>
                  {this.state.recruitmentApplied.length == 0 &&
                  this.state.loading == false ? (
                    <View
                      style={{
                        height: "100%",
                        width: "100%",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text
                        style={{
                          color: Colors.active,
                          fontWeight: "bold",
                          fontSize: 17
                        }}
                      >
                        Không có dữ liệu
                      </Text>
                    </View>
                  ) : (
                    <FlatList
                      data={this.state.recruitmentApplied}
                      renderItem={({ item }) => this.renderProfileItem(item)}
                      keyExtractor={item => item.employee_id}
                    />
                  )}
                </View>
                <RBSheet
                  ref={ref => {
                    this.RBSheet = ref;
                  }}
                  height={420}
                  duration={250}
                  customStyles={{
                    container: Themes.rbSheetContainer
                  }}
                >
                  {this.renderByType(searchType)}
                </RBSheet>
              </View>
            </ScrollView>
          </Tab>
          <Tab
            heading={"TIN TUYỂN DỤNG"}
            activeTextStyle={Themes.tabActiveTextStyle}
            tabStyle={Themes.tabStyle}
            textStyle={{ color: Colors.active }}
            activeTabStyle={Themes.tabActiveStyle}
            style={Themes.tabContent}
          >
            <View>
              <View style={Themes.dividerLarge}></View>
              <ScrollView>
                <View>
                  <View style={{ paddingHorizontal: 15, paddingTop: 5 }}>
                    <Text style={{ fontWeight: "bold" }}>Việc làm đã đăng</Text>
                    <View
                      style={[Themes.divider, { marginVertical: 5 }]}
                    ></View>
                    {this.state.posted.length == 0 &&
                    this.state.loading == false ? (
                      <Text
                        style={{
                          alignSelf: "center",
                          marginVertical: 30,
                          color: Colors.active,
                          fontWeight: "bold",
                          fontSize: 17
                        }}
                      >
                        Không có dữ liệu
                      </Text>
                    ) : (
                      <FlatList
                        data={this.state.posted}
                        renderItem={({ item }) => this.renderPosted(item)}
                        keyExtractor={item => item.Id}
                      />
                    )}
                  </View>
                  <View style={{ paddingHorizontal: 15, paddingTop: 5 }}>
                    <Text style={{ fontWeight: "bold" }}>
                      Việc làm đã hết hạn
                    </Text>
                    <View
                      style={[Themes.divider, { marginVertical: 5 }]}
                    ></View>
                    {this.state.expiredPost.length == 0 &&
                    this.state.loading == false ? (
                      <Text
                        style={{
                          alignSelf: "center",
                          marginVertical: 30,
                          color: Colors.active,
                          fontWeight: "bold",
                          fontSize: 17
                        }}
                      >
                        Không có dữ liệu
                      </Text>
                    ) : (
                      <FlatList
                        data={this.state.expiredPost}
                        renderItem={({ item }) => this.renderExpired(item)}
                        keyExtractor={item => item.Id}
                      />
                    )}
                    <FlatList
                      data={this.state.expiredPost}
                      renderItem={({ item }) => this.renderExpired(item)}
                      keyExtractor={item => item.Id}
                    />
                  </View>
                </View>
              </ScrollView>
            </View>
          </Tab>
        </Tabs>
      </View>
    );
  }
}
