import React from "react";
import { View, Tabs, Tab, Item, Input, DatePicker, Picker, Textarea, Button, ScrollableTab } from "native-base";
import {
    Image,
    ScrollView,
    TouchableOpacity,
    FlatList,
    Text,
    Alert,
    ActivityIndicator,
    AsyncStorage,
    Dimensions
} from "react-native";
import style from "./style";
import BasicHeader from "../../components/BasicHeader";
import Colors from "../../styles/Color";
import { Ionicons } from "@expo/vector-icons";
import Themes from "../../styles/Theme";
import Modal from "react-native-modalbox";
import { dimensions } from "../../styles/Dimension";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Loader from "../../components/Loader";
import JobDetailAPI from "../../api/JobDetailAPI";
// import { WebView } from "react-native-webview";
// import HTML from 'react-native-render-html';
import HTMLView from "react-native-htmlview";

export default class JobDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            jobId: this.props.navigation.getParam("jobId"),
            chosenDate: new Date(),
            selected: undefined,
            loading: false,
            job: {},
            workPlace: [],
            employerDetail: {},
            jobGroupWithEmployer: []
        };
        this.setDate = this.setDate.bind(this);
    }

    async componentDidMount() {
        this.setState({ loading: true });

        await JobDetailAPI.GetJobDetail({ job_id: this.state.jobId })
            .then(res => {
                this.setState({ job: res[1].data.detail_job, workPlace: res[1].data.work_place });
            })
            .catch(err => {
                this.setState({ loading: false });
                Alert.alert("Lỗi: " + err);
            })
            .done();

        await JobDetailAPI.GetEmployerDetail({ job_id: this.state.jobId })
            .then(res => {
                this.setState({ employerDetail: res[1].data });
            })
            .catch(err => {
                this.setState({ loading: false });
                Alert.alert("Lỗi: " + err);
            })
            .done();

        await JobDetailAPI.GetJobGroupWithEmployer({ job_id: this.state.jobId })
            .then(res => {
                this.setState({ loading: false, jobGroupWithEmployer: res[1].data.data });
            })
            .catch(err => {
                this.setState({ loading: false });
                Alert.alert("Lỗi: " + err);
            })
            .done();
    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }

    onValueChange(value) {
        this.setState({
            selected: value
        });
    }

    renderJobItem = item => {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate("JobDetail")}>
                <View style={style.jobContainer}>
                    <Text style={style.jobTitle}>{item.title}</Text>
                    <View style={style.timeContainer}>
                        {/* <Text>{item.Time} - </Text> */}
                        <Text style={style.txtSalary}>{item.salary}</Text>
                    </View>
                    <Text>{item.enterprise_name}</Text>
                    <Text>{item.job_group_name}</Text>
                    <View style={[Themes.divider, { marginVertical: 5 }]}></View>
                </View>
            </TouchableOpacity>
        );
    };

    ApplyNow = async () => {
        //this.refs.formApply.open()
        this.setState({ loading: true });
        const token = await AsyncStorage.getItem("token");
        const condition = {
            token: JSON.parse(token),
            jobId: this.state.jobId
        };
        await JobDetailAPI.ApplyNow(condition)
            .then(res => {
                this.setState({ loading: false });
                if (res[0] == 200) {
                    Alert.alert("Ứng tuyển thành công");
                } else Alert.alert("Có lỗi xảy ra");
            })
            .catch(err => {
                this.setState({ loading: false });
                Alert.alert("Lỗi: " + err);
            })
            .done();
    };

    renderNode(node, index, siblings, parent, defaultRenderer) {
        if (node.name == "p") {
            const specialSyle = node.attribs.style;
            return (
                <Text key={index} style={[specialSyle,{fontSize:15}]}>
                    {defaultRenderer(node.children, parent)}
                </Text>
            );
        }       
    }

    render() {
        const { job, employerDetail } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: "white" }}>
                <View style={[Themes.activityIndicatorView, { zIndex: 100000, position: "absolute" }]}>
                    <ActivityIndicator animating={this.state.loading} size={"large"} color={Colors.active} />
                </View>
                <BasicHeader title="Chi tiết công việc" navigation={this.props.navigation} />
                <View style={style.detail}>
                    {job.image ? (
                        <Image source={{ uri: job.image }} style={style.companyLogo} />
                    ) : (
                        <Image source={require("../../../assets/images/employer.png")} style={style.companyLogo} />
                    )}
                    <Text style={style.title} numberOfLines={3} ellipsizeMode="tail">
                        {job.title}
                    </Text>
                </View>
                <Text style={style.companyTitle}>{job.enterprise_name}</Text>
                <Tabs tabBarUnderlineStyle={Themes.tabsUnderlineColor} renderTabBar={() => <ScrollableTab />}>
                    <Tab
                        heading={"THÔNG TIN"}
                        activeTextStyle={Themes.tabActiveTextStyle}
                        tabStyle={Themes.tabStyle}
                        textStyle={{ color: Colors.active }}
                        activeTabStyle={Themes.tabActiveStyle}
                        style={Themes.tabContent}
                    >
                        <ScrollView>
                            <View style={style.infoContainer}>
                                <View>
                                    <View style={style.infoDetailItem}>
                                        <View style={{ width: 30 }}>
                                            <Ionicons name="ios-cash" size={20} color={Colors.active} />
                                        </View>
                                        <Text style={style.txtSalary}>Mức lương: {job.salary}</Text>
                                    </View>
                                    <View style={style.infoDetailItem}>
                                        <View style={{ width: 30 }}>
                                            <Ionicons name="ios-medal" size={20} color={Colors.active} />
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Text style={{ fontWeight: "bold" }}>Hình thức làm việc: </Text>
                                            <Text>Việc làm Parttime</Text>
                                        </View>
                                    </View>
                                    <View style={style.infoDetailItem}>
                                        <View style={{ width: 30 }}>
                                            <Ionicons name="ios-transgender" size={20} color={Colors.active} />
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Text style={{ fontWeight: "bold" }}>Giới tính: </Text>
                                            <Text>Không yêu cầu</Text>
                                        </View>
                                    </View>
                                    <View style={style.infoDetailItem}>
                                        <View style={{ width: 30 }}>
                                            <Ionicons name="ios-school" size={20} color={Colors.active} />
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Text style={{ fontWeight: "bold" }}>Chức vụ: </Text>
                                            <Text>{job.position}</Text>
                                        </View>
                                    </View>
                                    <View style={style.infoDetailItem}>
                                        <View style={{ width: 30 }}>
                                            <Ionicons name="ios-hourglass" size={20} color={Colors.active} />
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Text style={{ fontWeight: "bold" }}>Kinh nghiệm yêu cầu: </Text>
                                            <Text>{job.experience}</Text>
                                        </View>
                                    </View>
                                    <View style={style.infoDetailItem}>
                                        <View style={{ width: 30 }}>
                                            <Ionicons name="ios-pin" size={20} color={Colors.active} />
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Text style={{ fontWeight: "bold" }}>Địa điểm làm việc: </Text>
                                        </View>
                                    </View>
                                    {this.state.workPlace.map((el, i) => {
                                        return (
                                            <Text
                                                key={i}
                                                style={style.addressDetail}
                                                numberOfLines={1}
                                                ellipsizeMode="tail"
                                            >
                                                - {el.address}
                                            </Text>
                                        );
                                    })}
                                    {/* <Text style={style.addressDetail} numberOfLines={1} ellipsizeMode='tail'>- {jobData.work_place.address}</Text> */}
                                </View>
                                <View style={Themes.divider}></View>
                                <View style={{ marginTop: 5 }}>
                                    <Text style={style.desText}>MÔ TẢ CÔNG VIỆC</Text>
                                    {/* <HTML html={job.content} tagsStyles={{p:{fontFamily: 'Roboto'},h3:{fontFamily: 'Roboto'}}} imagesMaxWidth={Dimensions.get('window').width} /> */}
                                    <HTMLView value={job.content} renderNode={this.renderNode} />
                                    {/* <WebView source={{ html: job.content }} style={{ width: "100%", height: 150 }} /> */}
                                    {/* <Text>
                                        {`${job.content}`}
                                    </Text> */}
                                </View>
                            </View>
                        </ScrollView>
                    </Tab>
                    <Tab
                        heading={"CÔNG TY"}
                        activeTextStyle={Themes.tabActiveTextStyle}
                        tabStyle={Themes.tabStyle}
                        textStyle={{ color: Colors.active }}
                        activeTabStyle={Themes.tabActiveStyle}
                        style={Themes.tabContent}
                    >
                        <ScrollView>
                            <View style={style.infoContainer}>
                                <View>
                                    <Text style={style.companyDetailTitle}>{job.enterprise_name}</Text>
                                    {job.address ? (
                                        <View style={[style.infoDetailItem, { marginTop: 5 }]}>
                                            <View style={{ width: 30 }}>
                                                <Ionicons name="ios-pin" size={20} color={Colors.active} />
                                            </View>
                                            <Text style={style.companyAddress}>{job.address}</Text>
                                        </View>
                                    ) : (
                                        <View></View>
                                    )}
                                    <View style={[style.infoDetailItem, { marginTop: 5 }]}>
                                        <View style={{ width: 30 }}>
                                            <Ionicons name="ios-planet" size={20} color={Colors.active} />
                                        </View>
                                        <Text style={style.companyAddress}>{job.email}</Text>
                                    </View>
                                    <View style={Themes.divider}></View>
                                    {job.introduction ? (
                                        <View style={{ marginTop: 5 }}>
                                            <Text style={style.desText}>GIỚI THIỆU CÔNG TY</Text>
                                            {/* <Text>
                                                    {job.introduction}
                                                </Text> */}
                                            {/* <WebView
                                                source={{ html: job.introduction }}
                                                style={{ width: "100%", height: 150 }}
                                            /> */}
                                            {/* <HTML html={job.introduction} imagesMaxWidth={Dimensions.get('window').width} /> */}
                                            <HTMLView value={job.introduction} renderNode={this.renderNode} />
                                        </View>
                                    ) : (
                                        <View></View>
                                    )}
                                </View>
                            </View>
                        </ScrollView>
                    </Tab>
                    <Tab
                        heading={"VIỆC CÙNG CÔNG TY"}
                        activeTextStyle={Themes.tabActiveTextStyle}
                        tabStyle={Themes.tabStyle}
                        textStyle={{ color: Colors.active }}
                        activeTabStyle={Themes.tabActiveStyle}
                        style={Themes.tabContent}
                    >
                        <View style={{ marginVertical: 15 }}>
                            <FlatList
                                data={this.state.jobGroupWithEmployer}
                                renderItem={({ item }) => this.renderJobItem(item)}
                                keyExtractor={item => item.job_id.toString()}
                            />
                        </View>
                    </Tab>
                </Tabs>
                <TouchableOpacity style={style.btApply} onPress={() => this.ApplyNow()}>
                    <Text style={{ color: "white", fontWeight: "bold", fontSize: 15 }}>ỨNG TUYỂN NGAY</Text>
                </TouchableOpacity>
                <Modal
                    ref={"formApply"}
                    swipeToClose={false}
                    style={{ borderRadius: 10, width: dimensions.fullWidth - 30, height: dimensions.fullHeight - 100 }}
                >
                    <KeyboardAwareScrollView>
                        <View>
                            <View style={style.modalHeader}>
                                <Text style={{ color: Colors.active }}>THÔNG TIN ỨNG TUYỂN NGAY</Text>
                                <TouchableOpacity onPress={() => this.refs.formApply.close()}>
                                    <Ionicons name="ios-close" size={30} />
                                </TouchableOpacity>
                            </View>
                            <View style={Themes.divider}></View>
                            <View style={style.formGroup}>
                                <Text style={{ color: Colors.active }}>Họ và tên</Text>
                                <Item regular style={style.formInput}>
                                    <Input placeholder="Nhập họ và tên" />
                                </Item>
                            </View>
                            <View style={style.formGroup}>
                                <Text style={{ color: Colors.active }}>Ngày tháng năm sinh</Text>
                                <View style={style.datePicker}>
                                    <DatePicker
                                        borderRadius={10}
                                        locale={"vn"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText="Chọn ngày tháng năm"
                                        textStyle={{ color: "green" }}
                                        placeHolderTextStyle={{ color: "#d3d3d3" }}
                                        onDateChange={this.setDate}
                                        disabled={false}
                                    />
                                </View>
                            </View>
                            <View style={style.formGroup}>
                                <Text style={{ color: Colors.active }}>Giới tính</Text>
                                <View style={[style.datePicker, { paddingRight: 10 }]}>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader="Chọn giới tính"
                                        placeholder="Chọn giới tính"
                                        iosIcon={<Ionicons name="ios-arrow-down" size={20} color={Colors.note} />}
                                        style={{ width: "100%" }}
                                        selectedValue={this.state.selected}
                                        onValueChange={this.onValueChange.bind(this)}
                                    >
                                        <Picker.Item label="Nam" value="male" />
                                        <Picker.Item label="Nữ" value="famale" />
                                    </Picker>
                                </View>
                            </View>
                            <View style={style.formGroup}>
                                <Text style={{ color: Colors.active }}>Số điện thoại</Text>
                                <Item regular style={style.formInput}>
                                    <Input placeholder="Nhập số điện thoại" />
                                </Item>
                            </View>
                            <View style={style.formGroup}>
                                <Text style={{ color: Colors.active }}>Địa chỉ</Text>
                                <Item regular style={style.formInput}>
                                    <Input placeholder="Địa chỉ" />
                                </Item>
                            </View>
                            <View style={style.formGroup}>
                                <Text style={{ color: Colors.active }}>Địa chỉ</Text>
                                <Textarea style={style.textArea}></Textarea>
                            </View>
                            <Button
                                block
                                rounded
                                style={style.btConfirm}
                                onPress={async () => {
                                    await this.refs.formApply.close();
                                    await this.refs.modalNotify.open();
                                }}
                            >
                                <Text style={{ color: "white" }}>ỨNG TUYỂN NGAY</Text>
                            </Button>
                        </View>
                    </KeyboardAwareScrollView>
                </Modal>
                <Modal
                    ref={"modalNotify"}
                    swipeToClose={false}
                    style={{ borderRadius: 10, width: dimensions.fullWidth - 30, height: dimensions.fullHeight - 350 }}
                >
                    <ScrollView>
                        <View style={{ padding: 15 }}>
                            <Text>{`Chúc mừng bạn đã ứng tuyển thành công công việc Nhân viên Digital marketing (8-15tr) của công ty Cổ phần Công nghệ MOMA Việt Nam.\nĐể xác minh thông tin ứng tuyển, chúng tôi khuyên bạn hãy chia sẻ thông tin ứng tuyển cho bạn bè xác nhận. Chúng tôi sẽ liên lạc với bạn qua Email, bạn hãy để ý để nhận được thông báo của chúng tôi.\nXin chân thành cảm ơn!`}</Text>
                            <View
                                style={{
                                    marginTop: 40,
                                    flexDirection: "row",
                                    justifyContent: "space-around",
                                    alignItems: "center"
                                }}
                            >
                                <Button
                                    rounded
                                    style={{ width: 100, justifyContent: "center", backgroundColor: "#FF0000" }}
                                    onPress={() => this.refs.modalNotify.close()}
                                >
                                    <Text style={{ color: "white" }}>Đóng</Text>
                                </Button>
                                <Button
                                    rounded
                                    style={{ width: 100, justifyContent: "center", backgroundColor: "#003CFF" }}
                                >
                                    <Text style={{ color: "white" }}>Xác minh</Text>
                                </Button>
                            </View>
                        </View>
                    </ScrollView>
                </Modal>
            </View>
        );
    }
}
