import {StyleSheet} from 'react-native';
import {dimensions} from '../../styles/Dimension';
import Colors from '../../styles/Color';
import {Platform} from 'react-native';

const style = StyleSheet.create({
    tabsUnderlineColor: {
        backgroundColor: Colors.active,
    },
    tabActiveTextStyle: {
        color: Colors.active,
    },
    tabActiveStyle: {
        backgroundColor: 'white'
    },
    tabStyle: {
        backgroundColor: 'white',        
    },
    tabContent: {
        //backgroundColor: Colors.background,
        //height: 100
    },
    detail:{
        height:70,
        width:"100%",
        paddingHorizontal:15,             
        flexDirection:'row',
        alignItems:'center',
        marginVertical:10,
    },
    companyLogo: {
        height:70,
        width:70,
        resizeMode:'contain'
    },
    title: {
        color:Colors.active,
        fontWeight:'bold',
        marginLeft:15,
        width:'70%'
    },
    companyTitle: {
        fontStyle:'italic',
        marginHorizontal:15,
        marginBottom:15
    },
    infoDetailItem: {
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        marginBottom:5
    },
    infoContainer: {
        padding:15,
        width:'100%'
    },
    txtSalary: {
        color: Colors.money,       
        fontWeight:'bold'
    },
    addressDetail: {
        marginLeft:30,
        marginBottom:5
    },
    desText: {
        fontWeight:'bold',
        color:Colors.active,
        fontSize:15
    },
    companyDetailTitle: {
        fontWeight:'bold',
        color:Colors.active,
        fontSize:13,
        textTransform:'uppercase'
    },
    companyAddress: {
        fontSize:13,
        fontStyle:'italic',      
    },
    jobContainer: {
        width:'100%',
        paddingHorizontal:15
    },
    jobTitle: {
        color: Colors.active,
        fontWeight:'bold'
    },
    timeContainer: {
        flexDirection:'row',      
    },
    btApply: {
        justifyContent:'center',
        alignItems:'center',
        width:'100%',
        height:40,
        backgroundColor:Colors.active
    },
    modalHeader: {
        paddingVertical:5,
        paddingHorizontal:15,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        height:40
    },
    formGroup: {
        paddingHorizontal:15,
        paddingTop:10
    },
    formInput: {
        height:30,
        borderRadius:10,
        marginTop:5,
        borderColor:Colors.note
    },
    datePicker: {
        borderRadius:10,
        borderWidth:1, 
        height:30,
        marginTop:5,
        justifyContent:'center',
        borderColor:Colors.note
    },
    textArea: {
        backgroundColor: 'white',
        borderRadius: 10,
        borderColor: Colors.comboBox,
        borderWidth: 1,
        marginTop:10,
        width: dimensions.fullWidth-60,
        alignSelf:'center',
        height:70
    }, 
    btConfirm: {
        backgroundColor:Colors.active,
        marginHorizontal:15,
        marginVertical:10
    }
});

export default style;